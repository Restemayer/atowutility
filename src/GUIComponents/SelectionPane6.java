/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUIComponents;

import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;

/**
 *
 * @author Chris
 */
public class SelectionPane6 extends SelectionPaneObject {

    @Override
    public String PropertyString() {
        if ("".equals((String) jComboBox1.getSelectedItem())
                || (jTextField1.isEnabled() && "".equals(jLabel2.getText()))) {
            return null;
        }

        String returnValue;
        if (jTextField1.isEnabled()) {
            returnValue = (Type + "@" + jLabel2.getText() + jTextField1.getText() + "@" + XP[0]);
        } else {
            returnValue = (Type + "@" + jComboBox1.getSelectedItem() + "@" + XP[0]);
        }

        if (jComboBox2.getSelectedIndex() != 0) {
            if (jTextField2.isEnabled()) {
                returnValue += "&" + (Type + "@" + jLabel3.getText() + jTextField2.getText() + "@" + XP[1]);
            } else {
                returnValue += "&" + (Type + "@" + jComboBox2.getSelectedItem() + "@" + XP[1]);
            }
        }

        if (jComboBox3.getSelectedIndex() != 0) {
            if (jTextField3.isEnabled()) {
                returnValue += "&" + (Type + "@" + jLabel4.getText() + jTextField3.getText() + "@" + XP[2]);
            } else {
                returnValue += "&" + (Type + "@" + jComboBox3.getSelectedItem() + "@" + XP[2]);
            }
        }

        if (jComboBox4.getSelectedIndex() != 0) {
            if (jTextField4.isEnabled()) {
                returnValue += "&" + (Type + "@" + jLabel5.getText() + jTextField4.getText() + "@" + XP[3]);
            } else {
                returnValue += "&" + (Type + "@" + jComboBox4.getSelectedItem() + "@" + XP[3]);
            }
        }
        if (jComboBox5.getSelectedIndex() != 0) {
            if (jTextField5.isEnabled()) {
                returnValue += "&" + (Type + "@" + jLabel6.getText() + jTextField5.getText() + "@" + XP[3]);
            } else {
                returnValue += "&" + (Type + "@" + jComboBox5.getSelectedItem() + "@" + XP[3]);
            }
        }
        return returnValue;
    }

    /**
     * Creates new form SelectionPane
     *
     * @param o
     * @param type
     * @param xp
     * @param panel
     */
    public SelectionPane6(String[] o, String type, int[] xp, ModulePanel panel, java.awt.Frame frame, Boolean repeatable) {
        super(o, type, xp, panel, frame, repeatable);
        Selections = 5;
        initComponents();
        jComboBox2.setEnabled(false);
        jComboBox3.setEnabled(false);
        jComboBox4.setEnabled(false);
        jComboBox5.setEnabled(false);
        jComboBox6.setEnabled(false);
        jTextField1.setVisible(false);
        jTextField2.setVisible(false);
        jTextField3.setVisible(false);
        jTextField4.setVisible(false);
        jTextField5.setVisible(false);
        jTextField6.setVisible(false);
        jLabel2.setVisible(false);
        jLabel3.setVisible(false);
        jLabel4.setVisible(false);
        jLabel5.setVisible(false);
        jLabel6.setVisible(false);
        jLabel7.setVisible(false);
        BoxOneLabel.setText("Choose one: " + xp[0] + " XP");
        BoxTwoLabel.setText("Choose one: " + xp[1] + " XP");
        BoxThreeLabel.setText("Choose one: " + xp[2] + " XP");
        BoxFourLabel.setText("Choose one: " + xp[3] + " XP");
        BoxFiveLabel.setText("Choose one: " + xp[4] + " XP");
        BoxSixLabel.setText("Choose one: " + xp[5] + " XP");
    }

    private void SetAllocatedXP() {
        XPAllocated = 0;

        if (jComboBox1.getSelectedIndex() != 0 && (!jTextField1.isVisible() || !jTextField1.getText().equals(""))) {
            XPAllocated += XP[0];
        }
        if (jComboBox2.getSelectedIndex() != 0 && (!jTextField2.isVisible() || !jTextField2.getText().equals(""))) {
            XPAllocated += XP[1];
        }
        if (jComboBox3.getSelectedIndex() != 0 && (!jTextField3.isVisible() || !jTextField3.getText().equals(""))) {
            XPAllocated += XP[2];
        }
        if (jComboBox4.getSelectedIndex() != 0 && (!jTextField4.isVisible() || !jTextField4.getText().equals(""))) {
            XPAllocated += XP[3];
        }
        if (jComboBox5.getSelectedIndex() != 0 && (!jTextField5.isVisible() || !jTextField5.getText().equals(""))) {
            XPAllocated += XP[4];
        }
        if (jComboBox6.getSelectedIndex() != 0 && (!jTextField6.isVisible() || !jTextField6.getText().equals(""))) {
            XPAllocated += XP[5];
        }
    }

    private ComboBoxModel ComboOptions1() {
        return new DefaultComboBoxModel(options);
    }

    private ComboBoxModel ComboOptions2() {
        String newoptions = "";
        for (String input : options) {
            Boolean additem = false;
            if (Repeatable || (!input.equals(jComboBox1.getSelectedItem().toString()) && !"".equals(input))) {
                additem = true;
            }

            if (additem) {
                newoptions += "," + input;
            }

        }

        return new DefaultComboBoxModel(newoptions.split(","));
    }

    private ComboBoxModel ComboOptions3() {
        String newoptions = "";
        for (String input : options) {
            Boolean additem = false;
            if (Repeatable || ((!input.equals(jComboBox1.getSelectedItem().toString()) && !"".equals(input)) && (!input.equals(jComboBox2.getSelectedItem().toString()) && !"".equals(input)))) {
                additem = true;
            }

            if (additem) {
                newoptions += "," + input;
            }
        }

        return new DefaultComboBoxModel(newoptions.split(","));
    }

    private ComboBoxModel ComboOptions4() {
        String newoptions = "";
        for (String input : options) {
            Boolean additem = false;
            if (Repeatable || ((!input.equals(jComboBox1.getSelectedItem().toString()) && !"".equals(input)) && (!input.equals(jComboBox2.getSelectedItem().toString()) && !"".equals(input)) && (!input.equals(jComboBox3.getSelectedItem().toString()) && !"".equals(input)))) {
                additem = true;
            }

            if (additem) {
                newoptions += "," + input;
            }
        }

        return new DefaultComboBoxModel(newoptions.split(","));
    }

    private ComboBoxModel ComboOptions5() {
        String newoptions = "";
        for (String input : options) {
            Boolean additem = false;
            if (Repeatable || ((!input.equals(jComboBox1.getSelectedItem().toString()) && !"".equals(input)) && (!input.equals(jComboBox2.getSelectedItem().toString()) && !"".equals(input)) && (!input.equals(jComboBox3.getSelectedItem().toString()) && !"".equals(input)) && (!input.equals(jComboBox4.getSelectedItem().toString()) && !"".equals(input)))) {
                additem = true;
            }

            if (additem) {
                newoptions += "," + input;
            }
        }

        return new DefaultComboBoxModel(newoptions.split(","));
    }

    private ComboBoxModel ComboOptions6() {
        String newoptions = "";
        for (String input : options) {
            Boolean additem = false;
            if (Repeatable || ((!input.equals(jComboBox1.getSelectedItem().toString()) && !"".equals(input)) && (!input.equals(jComboBox2.getSelectedItem().toString()) && !"".equals(input)) && (!input.equals(jComboBox3.getSelectedItem().toString()) && !"".equals(input)) && (!input.equals(jComboBox4.getSelectedItem().toString()) && !"".equals(input)) && (!input.equals(jComboBox5.getSelectedItem().toString()) && !"".equals(input)))) {
                additem = true;
            }

            if (additem) {
                newoptions += "," + input;
            }
        }

        return new DefaultComboBoxModel(newoptions.split(","));
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        BoxOneLabel = new javax.swing.JLabel();
        jComboBox1 = new javax.swing.JComboBox<>();
        jLabel2 = new javax.swing.JLabel();
        jTextField1 = new javax.swing.JTextField();
        jComboBox2 = new javax.swing.JComboBox<>();
        jLabel3 = new javax.swing.JLabel();
        jTextField2 = new javax.swing.JTextField();
        jComboBox3 = new javax.swing.JComboBox<>();
        jLabel4 = new javax.swing.JLabel();
        jTextField3 = new javax.swing.JTextField();
        jComboBox4 = new javax.swing.JComboBox<>();
        jTextField4 = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        BoxTwoLabel = new javax.swing.JLabel();
        BoxThreeLabel = new javax.swing.JLabel();
        BoxFourLabel = new javax.swing.JLabel();
        BoxFiveLabel = new javax.swing.JLabel();
        jComboBox5 = new javax.swing.JComboBox<>();
        jLabel6 = new javax.swing.JLabel();
        jTextField5 = new javax.swing.JTextField();
        BoxSixLabel = new javax.swing.JLabel();
        jComboBox6 = new javax.swing.JComboBox<>();
        jLabel7 = new javax.swing.JLabel();
        jTextField6 = new javax.swing.JTextField();

        setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        setPreferredSize(new java.awt.Dimension(430, 200));

        BoxOneLabel.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        BoxOneLabel.setText("jLabel1");

        jComboBox1.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jComboBox1.setModel(ComboOptions1());
        jComboBox1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBox1ActionPerformed(evt);
            }
        });

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel2.setText("Skill/");

        jTextField1.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jTextField1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField1ActionPerformed(evt);
            }
        });

        jComboBox2.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jComboBox2.setModel(ComboOptions2());
        jComboBox2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBox2ActionPerformed(evt);
            }
        });

        jLabel3.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel3.setText("Skill/");

        jTextField2.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jTextField2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField2ActionPerformed(evt);
            }
        });

        jComboBox3.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jComboBox3.setModel(ComboOptions3());
        jComboBox3.setEnabled(false);
        jComboBox3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBox3ActionPerformed(evt);
            }
        });

        jLabel4.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel4.setText("Skill/");

        jTextField3.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jTextField3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField3ActionPerformed(evt);
            }
        });

        jComboBox4.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jComboBox4.setModel(ComboOptions4());
        jComboBox4.setEnabled(false);
        jComboBox4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBox4ActionPerformed(evt);
            }
        });

        jTextField4.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jTextField4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField4ActionPerformed(evt);
            }
        });

        jLabel5.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel5.setText("Skill/");

        BoxTwoLabel.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        BoxTwoLabel.setText("jLabel1");

        BoxThreeLabel.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        BoxThreeLabel.setText("jLabel1");

        BoxFourLabel.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        BoxFourLabel.setText("jLabel1");

        BoxFiveLabel.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        BoxFiveLabel.setText("jLabel1");

        jComboBox5.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jComboBox5.setModel(ComboOptions5());
        jComboBox5.setEnabled(false);
        jComboBox5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBox5ActionPerformed(evt);
            }
        });

        jLabel6.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel6.setText("Skill/");

        jTextField5.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jTextField5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField5ActionPerformed(evt);
            }
        });

        BoxSixLabel.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        BoxSixLabel.setText("jLabel1");

        jComboBox6.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jComboBox6.setModel(ComboOptions6());
        jComboBox6.setEnabled(false);
        jComboBox6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBox6ActionPerformed(evt);
            }
        });

        jLabel7.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel7.setText("Skill/");

        jTextField6.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jTextField6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField6ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(BoxOneLabel, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 190, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jLabel4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jTextField3, javax.swing.GroupLayout.DEFAULT_SIZE, 160, Short.MAX_VALUE))
                    .addComponent(jComboBox1, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jTextField1))
                    .addComponent(jComboBox3, javax.swing.GroupLayout.Alignment.TRAILING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(BoxThreeLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jLabel6)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jTextField5, javax.swing.GroupLayout.DEFAULT_SIZE, 160, Short.MAX_VALUE))
                    .addComponent(jComboBox5, javax.swing.GroupLayout.Alignment.TRAILING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(BoxFiveLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(BoxFourLabel, javax.swing.GroupLayout.DEFAULT_SIZE, 197, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jTextField2))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jLabel5)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jTextField4, javax.swing.GroupLayout.DEFAULT_SIZE, 167, Short.MAX_VALUE))
                    .addComponent(BoxTwoLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jComboBox2, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jComboBox4, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jLabel7)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jTextField6, javax.swing.GroupLayout.DEFAULT_SIZE, 167, Short.MAX_VALUE))
                    .addComponent(BoxSixLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jComboBox6, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(BoxOneLabel)
                    .addComponent(BoxTwoLabel))
                .addGap(1, 1, 1)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jTextField2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel3))
                            .addComponent(jLabel2)))
                    .addComponent(jComboBox2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(2, 2, 2)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(BoxThreeLabel)
                    .addComponent(BoxFourLabel))
                .addGap(1, 1, 1)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(21, 21, 21)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jTextField3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel4)))
                    .addComponent(jComboBox3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jComboBox4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, 0)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jTextField4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel5))))
                .addGap(2, 2, 2)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(BoxSixLabel)
                        .addGap(1, 1, 1)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(21, 21, 21)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jTextField6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel7)))
                            .addComponent(jComboBox6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(BoxFiveLabel)
                        .addGap(1, 1, 1)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(21, 21, 21)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jTextField5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel6)))
                            .addComponent(jComboBox5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void jTextField1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField1ActionPerformed
        if (!"".equals(jTextField1.getText())) {
            jComboBox2.setEnabled(true);
        } else {
            jComboBox2.setEnabled(false);
        }
        modulepanel.UpdateSelections();
    }//GEN-LAST:event_jTextField1ActionPerformed

    private void jComboBox1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBox1ActionPerformed
        jComboBox2.removeAllItems();
        jComboBox2.setModel(ComboOptions2());
        jComboBox2.setSelectedIndex(0);
        jTextField2.setVisible(false);
        jTextField2.setText("");
        jComboBox3.setSelectedIndex(0);
        jTextField3.setVisible(false);
        jTextField3.setText("");
        jComboBox3.revalidate();
        jComboBox3.repaint();
        jComboBox3.revalidate();
        jComboBox2.repaint();

        if ("[NEW]".equals(((String) jComboBox1.getSelectedItem()).split("/")[((String) jComboBox1.getSelectedItem()).split("/").length - 1])) {
            jTextField1.setVisible(true);
            jLabel2.setVisible(true);
            jTextField1.setEnabled(true);
            jLabel2.setEnabled(true);

            String text = "";
            for (int x = 0; x < ((String) jComboBox1.getSelectedItem()).split("/").length - 1; x++) {
                text += (((String) jComboBox1.getSelectedItem()).split("/")[x] + "/");
            }
            jLabel2.setText(text);
            jTextField1.setText("");
        } else {
            jTextField1.setText("");
            jTextField1.setEnabled(false);
            jLabel2.setEnabled(false);
            jTextField1.setVisible(false);
            jLabel2.setVisible(false);
        }
        if (jComboBox1.getSelectedIndex() == 0 || (jTextField1.isVisible() && "".equals(jTextField1.getText()))) {
            jComboBox2.setEnabled(false);
            jComboBox3.setEnabled(false);
        } else {
            jComboBox2.setEnabled(true);
        }
        if (!"".equals((String) jComboBox1.getSelectedItem()) && !jTextField1.isEnabled()) {
            modulepanel.UpdateSelections();
        }
    }//GEN-LAST:event_jComboBox1ActionPerformed

    private void jComboBox2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBox2ActionPerformed
        if (jComboBox2.getItemAt(1) == null) {
            return;
        }

        jComboBox3.removeAllItems();
        jComboBox3.setModel(ComboOptions3());
        jComboBox3.setSelectedIndex(0);
        jTextField3.setVisible(false);
        jTextField3.setText("");
        revalidate();
        repaint();

        if ("[NEW]".equals(((String) jComboBox2.getSelectedItem()).split("/")[((String) jComboBox2.getSelectedItem()).split("/").length - 1])) {
            jTextField2.setVisible(true);
            jLabel3.setVisible(true);
            jTextField2.setEnabled(true);
            jLabel3.setEnabled(true);

            String text = "";
            for (int x = 0; x < ((String) jComboBox2.getSelectedItem()).split("/").length - 1; x++) {
                text += (((String) jComboBox2.getSelectedItem()).split("/")[x] + "/");
            }
            jLabel3.setText(text);
            jTextField2.setText("");
        } else {
            jTextField2.setText("");
            jTextField2.setEnabled(false);
            jLabel3.setEnabled(false);
            jTextField2.setVisible(false);
            jLabel3.setVisible(false);
        }
        if (jComboBox2.getSelectedIndex() == 0 || (jTextField2.isVisible() && "".equals(jTextField2.getText()))) {
            jComboBox3.setEnabled(false);
        } else {
            jComboBox3.setEnabled(true);
        }

        if (!"".equals((String) jComboBox2.getSelectedItem()) && !jTextField2.isEnabled()) {
            modulepanel.UpdateSelections();
        }

    }//GEN-LAST:event_jComboBox2ActionPerformed

    private void jTextField2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField2ActionPerformed
        if (!"".equals(jTextField2.getText())) {
            jComboBox3.setEnabled(true);
        } else {
            jComboBox3.setEnabled(false);
        }
        modulepanel.UpdateSelections();
    }//GEN-LAST:event_jTextField2ActionPerformed

    private void jComboBox3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBox3ActionPerformed
        if (jComboBox3.getItemAt(1) == null) {
            return;
        }

        jComboBox4.removeAllItems();
        jComboBox4.setModel(ComboOptions4());
        jComboBox4.setSelectedIndex(0);
        jTextField4.setVisible(false);
        jTextField4.setText("");
        revalidate();
        repaint();

        if ("[NEW]".equals(((String) jComboBox3.getSelectedItem()).split("/")[((String) jComboBox3.getSelectedItem()).split("/").length - 1])) {
            jTextField3.setVisible(true);
            jLabel4.setVisible(true);
            jTextField3.setEnabled(true);
            jLabel4.setEnabled(true);

            String text = "";
            for (int x = 0; x < ((String) jComboBox3.getSelectedItem()).split("/").length - 1; x++) {
                text += (((String) jComboBox3.getSelectedItem()).split("/")[x] + "/");
            }
            jLabel4.setText(text);
            jTextField3.setText("");
        } else {
            jTextField3.setText("");
            jTextField3.setEnabled(false);
            jLabel4.setEnabled(false);
            jTextField3.setVisible(false);
            jLabel4.setVisible(false);
        }
        if (jComboBox3.getSelectedIndex() == 0 || (jTextField3.isVisible() && "".equals(jTextField3.getText()))) {
            jComboBox4.setEnabled(false);
        } else {
            jComboBox4.setEnabled(true);
        }

        if (!"".equals((String) jComboBox3.getSelectedItem()) && !jTextField3.isEnabled()) {
            modulepanel.UpdateSelections();
        }
    }//GEN-LAST:event_jComboBox3ActionPerformed

    private void jTextField3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField3ActionPerformed
        if (!"".equals(jTextField3.getText())) {
            jComboBox4.setEnabled(true);
        } else {
            jComboBox4.setEnabled(false);
        }
        modulepanel.UpdateSelections();
    }//GEN-LAST:event_jTextField3ActionPerformed

    private void jComboBox4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBox4ActionPerformed
        if (jComboBox4.getItemAt(1) == null) {
            return;
        }

        jComboBox5.removeAllItems();
        jComboBox5.setModel(ComboOptions5());
        jComboBox5.setSelectedIndex(0);
        jTextField5.setVisible(false);
        jTextField5.setText("");
        revalidate();
        repaint();

        if ("[NEW]".equals(((String) jComboBox4.getSelectedItem()).split("/")[((String) jComboBox4.getSelectedItem()).split("/").length - 1])) {
            jTextField4.setVisible(true);
            jLabel5.setVisible(true);
            jTextField4.setEnabled(true);
            jLabel5.setEnabled(true);

            String text = "";
            for (int x = 0; x < ((String) jComboBox4.getSelectedItem()).split("/").length - 1; x++) {
                text += (((String) jComboBox4.getSelectedItem()).split("/")[x] + "/");
            }
            jLabel5.setText(text);
            jTextField4.setText("");
        } else {
            jTextField4.setText("");
            jTextField4.setEnabled(false);
            jLabel5.setEnabled(false);
            jTextField4.setVisible(false);
            jLabel5.setVisible(false);
        }
        if (jComboBox4.getSelectedIndex() == 0 || (jTextField4.isVisible() && "".equals(jTextField4.getText()))) {
            jComboBox5.setEnabled(false);
        } else {
            jComboBox5.setEnabled(true);
        }

        if (!"".equals((String) jComboBox4.getSelectedItem()) && !jTextField4.isEnabled()) {
            modulepanel.UpdateSelections();
        }
    }//GEN-LAST:event_jComboBox4ActionPerformed

    private void jTextField4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField4ActionPerformed
        if (!"".equals(jTextField4.getText())) {
            jComboBox5.setEnabled(true);
        } else {
            jComboBox5.setEnabled(false);
        }
        modulepanel.UpdateSelections();
    }//GEN-LAST:event_jTextField4ActionPerformed

    private void jComboBox5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBox5ActionPerformed
        if (jComboBox5.getItemAt(1) == null) {
            return;
        }

        jComboBox6.removeAllItems();
        jComboBox6.setModel(ComboOptions5());
        jComboBox6.setSelectedIndex(0);
        jTextField6.setVisible(false);
        jTextField6.setText("");
        revalidate();
        repaint();

        if ("[NEW]".equals(((String) jComboBox5.getSelectedItem()).split("/")[((String) jComboBox5.getSelectedItem()).split("/").length - 1])) {
            jTextField5.setVisible(true);
            jLabel6.setVisible(true);
            jTextField5.setEnabled(true);
            jLabel6.setEnabled(true);

            String text = "";
            for (int x = 0; x < ((String) jComboBox5.getSelectedItem()).split("/").length - 1; x++) {
                text += (((String) jComboBox5.getSelectedItem()).split("/")[x] + "/");
            }
            jLabel6.setText(text);
            jTextField5.setText("");
        } else {
            jTextField5.setText("");
            jTextField5.setEnabled(false);
            jLabel6.setEnabled(false);
            jTextField5.setVisible(false);
            jLabel6.setVisible(false);
        }
        if (jComboBox5.getSelectedIndex() == 0 || (jTextField5.isVisible() && "".equals(jTextField5.getText()))) {
            jComboBox6.setEnabled(false);
        } else {
            jComboBox6.setEnabled(true);
        }

        if (!"".equals((String) jComboBox5.getSelectedItem()) && !jTextField5.isEnabled()) {
            modulepanel.UpdateSelections();
        }
    }//GEN-LAST:event_jComboBox5ActionPerformed

    private void jTextField5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField5ActionPerformed
        if (!"".equals(jTextField5.getText())) {
            jComboBox6.setEnabled(true);
        } else {
            jComboBox6.setEnabled(false);
        }
        modulepanel.UpdateSelections();
    }//GEN-LAST:event_jTextField5ActionPerformed

    private void jComboBox6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBox6ActionPerformed
        if (jComboBox6.getItemAt(1) == null) {
            return;
        }

        if ("[NEW]".equals(((String) jComboBox6.getSelectedItem()).split("/")[((String) jComboBox6.getSelectedItem()).split("/").length - 1])) {
            jTextField6.setVisible(true);
            jLabel7.setVisible(true);
            jTextField6.setEnabled(true);
            jLabel7.setEnabled(true);

            String text = "";
            for (int x = 0; x < ((String) jComboBox6.getSelectedItem()).split("/").length - 1; x++) {
                text += (((String) jComboBox6.getSelectedItem()).split("/")[x] + "/");
            }
            jLabel7.setText(text);
            jTextField6.setText("");
        } else {
            jTextField6.setText("");
            jTextField6.setEnabled(false);
            jLabel7.setEnabled(false);
            jTextField6.setVisible(false);
            jLabel7.setVisible(false);
        }

        if (!"".equals((String) jComboBox6.getSelectedItem()) && !jTextField6.isEnabled()) {
            SetAllocatedXP();
            modulepanel.UpdateSelections();
        }
    }//GEN-LAST:event_jComboBox6ActionPerformed

    private void jTextField6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField6ActionPerformed
        SetAllocatedXP();
        modulepanel.UpdateSelections();
    }//GEN-LAST:event_jTextField6ActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel BoxFiveLabel;
    private javax.swing.JLabel BoxFourLabel;
    private javax.swing.JLabel BoxOneLabel;
    private javax.swing.JLabel BoxSixLabel;
    private javax.swing.JLabel BoxThreeLabel;
    private javax.swing.JLabel BoxTwoLabel;
    private javax.swing.JComboBox<String> jComboBox1;
    private javax.swing.JComboBox<String> jComboBox2;
    private javax.swing.JComboBox<String> jComboBox3;
    private javax.swing.JComboBox<String> jComboBox4;
    private javax.swing.JComboBox<String> jComboBox5;
    private javax.swing.JComboBox<String> jComboBox6;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JTextField jTextField2;
    private javax.swing.JTextField jTextField3;
    private javax.swing.JTextField jTextField4;
    private javax.swing.JTextField jTextField5;
    private javax.swing.JTextField jTextField6;
    // End of variables declaration//GEN-END:variables
}
