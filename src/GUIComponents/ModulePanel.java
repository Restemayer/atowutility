/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUIComponents;

import atowutility.ProgramDataSingleton;
import atowutility.classes.LogBook;
import atowutility.classes.LogBookEntry;
import atowutility.classes.Module;
import atowutility.classes.Rank;
import atowutility.classes.SkillField;
import atowutility.classes.Stage0Module;
import atowutility.enums.AffiliationType;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JButton;

/**
 *
 * @author Chris
 */
public class ModulePanel extends javax.swing.JPanel {

    protected Module main;
    protected Module selected;
    protected int Cost;
    int StartingYear;
    int[] Attributes = {0, 0, 0, 0, 0, 0, 0, 0};
    int[] Prerequisites = {0, 0, 0, 0, 0, 0, 0, 0};
    int additionalflexxp = 0;
    protected LogBook Traits;
    protected LogBook Skills;
    protected LogBook CaughtStuff;
    protected LogBook Rules;
    protected LogBook Restrictions;
    protected LogBook ExtraPrerequisites;
    protected List<String> OptionPanelItems;
    protected List<SelectionPaneObject> selectionPanels;
    protected List<String> NewStuff;
    protected String NearestState;
    List<String> AttributeSelections;
    protected NewCharacterPopup Parent;
    protected JButton TakeModuleButton;

    java.awt.Frame ParentFrame;

    public ModulePanel(Module module, int startingyear, NewCharacterPopup parent) {
        ParentFrame = parent;

        main = module;
        StartingYear = startingyear;
        Parent = parent;
        Skills = new LogBook(main.Name + " Skills", false);
        Traits = new LogBook(main.Name + " Traits", false);
        Rules = new LogBook(main.Name + " Rules", false);
        ExtraPrerequisites = new LogBook(main.Name + " Additional Prerequisites", false);
        Restrictions = new LogBook(main.Name + " Restrictions", false);
        CaughtStuff = new LogBook(main.Name + " Caught Stuff", false);
        AttributeSelections = new ArrayList<>();
        NearestState = "";
    }

    protected void resetLists() {
        Cost = 0;

        String name = main.Name;

        Traits.EraseLogs();
        Skills.EraseLogs();
        Restrictions.EraseLogs();
        Rules.EraseLogs();
        ExtraPrerequisites.EraseLogs();
        AttributeSelections.clear();
        NewStuff.clear();
        OptionPanelItems.clear();
        Boolean repeatModule = false;
        Boolean FlexOverride = false;
        if (Parent.Stage == 4 && main.Repeatable) {
            for (Module mod : Parent.modulesTaken) {
                if (mod.Stage == 4 && mod.Name.equals(main.Name)) {
                    repeatModule = true;
                }
            }
        }
        for (int x = 0; x < Attributes.length; x++) {
            Attributes[x] = 0;
        }
        for (int x = 0; x < Prerequisites.length; x++) {
            Prerequisites[x] = 0;
        }
        if (!repeatModule) {
            for (LogBookEntry log : main.Attributes.GetLogs()) {
                switch (log.Description()) {
                    case "Strength": {
                        Attributes[0] += Integer.parseInt(log.Value());
                        break;
                    }
                    case "Body": {
                        Attributes[1] += Integer.parseInt(log.Value());
                        break;
                    }
                    case "Reflexes": {
                        Attributes[2] += Integer.parseInt(log.Value());
                        break;
                    }
                    case "Dexterity": {
                        Attributes[3] += Integer.parseInt(log.Value());
                        break;
                    }
                    case "Intelligence": {
                        Attributes[4] += Integer.parseInt(log.Value());
                        break;
                    }
                    case "Willpower": {
                        Attributes[5] += Integer.parseInt(log.Value());
                        break;
                    }
                    case "Charisma": {
                        Attributes[6] += Integer.parseInt(log.Value());
                        break;
                    }
                    case "Edge": {
                        Attributes[7] += Integer.parseInt(log.Value());
                        break;
                    }
                    default: {
                        OptionPanelItems.add("Attribute:" + log.Description() + ":" + log.Value());
                    }
                }
            }
        }
        for (LogBookEntry log : main.Prerequisites.GetLogs()) {
            switch (log.Description()) {
                case "Strength": {
                    Prerequisites[0] += Integer.parseInt(log.Value());
                    break;
                }
                case "Body": {
                    Prerequisites[1] += Integer.parseInt(log.Value());
                    break;
                }
                case "Reflexes": {
                    Prerequisites[2] += Integer.parseInt(log.Value());
                    break;
                }
                case "Dexterity": {
                    Prerequisites[3] += Integer.parseInt(log.Value());
                    break;
                }
                case "Intelligence": {
                    Prerequisites[4] += Integer.parseInt(log.Value());
                    break;
                }
                case "Willpower": {
                    Prerequisites[5] += Integer.parseInt(log.Value());
                    break;
                }
                case "Charisma": {
                    Prerequisites[6] += Integer.parseInt(log.Value());
                    break;
                }
                case "Edge": {
                    Prerequisites[7] += Integer.parseInt(log.Value());
                    break;
                }
                default: {
                    ExtraPrerequisites.Add(log.Description(), log.Value());
                }
            }
        }

        for (SkillField sf : main.TakenSkillFields) {
            if (!sf.Prerequisites.equals("")) {
                for (String p : sf.Prerequisites.split(",")) {
                    String desc = p.split(":")[0];
                    String val = p.split(":")[1];
                    switch (desc) {
                        case "Strength": {
                            Prerequisites[0] = Integer.parseInt(val);
                            break;
                        }
                        case "Body": {
                            if (Integer.parseInt(val) > Prerequisites[1]) {
                                Prerequisites[1] = Integer.parseInt(val);
                            }
                            break;
                        }
                        case "Reflexes": {
                            if (Integer.parseInt(val) > Prerequisites[2]) {
                                Prerequisites[2] = Integer.parseInt(val);
                            }
                            break;
                        }
                        case "Dexterity": {
                            if (Integer.parseInt(val) > Prerequisites[3]) {
                                Prerequisites[3] = Integer.parseInt(val);
                            }
                            break;
                        }
                        case "Intelligence": {
                            if (Integer.parseInt(val) > Prerequisites[4]) {
                                Prerequisites[4] = Integer.parseInt(val);
                            }
                            break;
                        }
                        case "Willpower": {
                            if (Integer.parseInt(val) > Prerequisites[5]) {
                                Prerequisites[5] = Integer.parseInt(val);
                            }
                            break;
                        }
                        case "Charisma": {
                            if (Integer.parseInt(val) > Prerequisites[6]) {
                                Prerequisites[6] = Integer.parseInt(val);
                            }
                            break;
                        }
                        case "Edge": {
                            if (Integer.parseInt(val) > Prerequisites[7]) {
                                Prerequisites[7] = Integer.parseInt(val);
                            }
                            break;
                        }
                        default: {
                            ExtraPrerequisites.Add(desc, val);
                        }
                    }
                }
            }
        }

        Cost += main.Cost();

        if (!repeatModule) {
            main.Traits.GetLogs().forEach((log) -> {
                Traits.Add(log.Description(), log.Value());
            });
        }

        main.Skills.GetLogs().forEach((log) -> {
            Skills.Add(log.Description(), log.Value());
        });

        for (SkillField sf : main.TakenSkillFields) {
            for (String s : sf.Skills) {
                Skills.Add(s, Integer.toString(sf.XP));

            }
        }

        main.Restrictions.GetLogs().forEach((log) -> {
            Restrictions.Add(log.Description(), log.Value());
        });

        if (main.Rules != null && !"".equals(main.Rules)) {
            for (String r : main.Rules.split(",")) {
                LogBookEntry rule;
                if (r.split("%")[0].equals("Consequence")) { // (Example) Consequence%!Stage#2@Preparatory School|!Stage#2@Military School%Attribute@Willpower:100
                    Boolean apply = false;
                    Boolean breakout = false;
                    for (String test : r.split("%")[1].split("\\|")) {
                        switch (test.split("@")[0].split("#")[0]) {
                            case "!Stage": {
                                for (Module mod : Parent.modulesTaken) {
                                    if (mod.Name.equals(test.split("@")[1]) && mod.Stage == Integer.parseInt(test.split("@")[0].split("#")[1])) {
                                        breakout = true;
                                        apply = false;
                                        break;
                                    } else {
                                        apply = true;
                                    }
                                }
                                break;
                            }
                        }
                        if (breakout) {
                            break;
                        }
                    }
                    if (apply) {
                        switch (r.split("%")[2].split("@")[0]) {
                            case "Attribute": {
                                switch (r.split("%")[2].split("@")[1].split(":")[0]) {
                                    case "Strength": {
                                        Attributes[0] += Integer.parseInt(r.split(":")[1]);
                                        break;
                                    }
                                    case "Body": {
                                        Attributes[1] += Integer.parseInt(r.split(":")[1]);
                                        break;
                                    }
                                    case "Reflexes": {
                                        Attributes[2] += Integer.parseInt(r.split(":")[1]);
                                        break;
                                    }
                                    case "Dexterity": {
                                        Attributes[3] += Integer.parseInt(r.split(":")[1]);
                                        break;
                                    }
                                    case "Intelligence": {
                                        Attributes[4] += Integer.parseInt(r.split(":")[1]);
                                        break;
                                    }
                                    case "Willpower": {
                                        Attributes[5] += Integer.parseInt(r.split(":")[1]);
                                        break;
                                    }
                                    case "Charisma": {
                                        Attributes[6] += Integer.parseInt(r.split(":")[1]);
                                        break;
                                    }
                                    case "Edge": {
                                        Attributes[7] += Integer.parseInt(r.split(":")[1]);
                                        break;
                                    }
                                }
                                break;
                            }
                            case "Trait": {
                                Traits.Add(r.split("%")[2].split("@")[1].split(":")[0], r.split(":")[1]);
                            }
                        }
                    }
                } else if (r.split("%")[0].equals("FlexRepeatOverride")) {
                    FlexOverride = true;
                } 
                else if (repeatModule && r.split("%")[0].equals("Repeated")){
                    switch (r.split("%")[1]){
                        case "Trait":{
                            Traits.Add(r.split("%")[2].split(":")[0], r.split("%")[2].split(":")[1]);
                            break;
                        }
                    }
                }else if (r.split(":").length == 2) {
                    if (!repeatModule || !r.split("%")[0].equals("Choice") || !FlexOverride) {
                        Rules.Add(r.split(":")[0], r.split(":")[1]);
                    }
                } else {
                    Rules.Add(r, "-9999");
                }
            }
        }

    }

    protected void selectModule() {

    }

    public void UpdateSelections() {
    }

    protected String[] breakAttributeString(String input) {
        String[] orArray = input.split("\\|");
        String output = "";

        for (String object : orArray) {

            if (object.equals("Strength") || object.equals("Body") || object.equals("Reflexes") || object.equals("Dexterity") || object.equals("Intelligence") || object.equals("Willpower") || object.equals("Charisma") || object.equals("Edge")) {
                output += ("," + object);
            } else if (object.equals("Any")) {
                output = ",Attribute:Strength,Attribute:Body,Attribute:Reflexes,Attribute:Dexterity,Attribute:Intelligence,Attribute:Willpower,Attribute:Charisma,Attribute:Edge";
            } else {
                System.out.println("Unknown attribute: " + object);
            }
        }
        return output.split(",");
    }

    protected String[] breakTraitString(String input) {
        String[] orArray = input.split("\\|");
        String output = "";
        for (String object : orArray) {
            Boolean unknown = true;
            for (LogBookEntry trait : Parent.TraitMaster.GetLogs()) {
                if (object.equals(trait.Description())) {
                    unknown = false;
                    output += ("," + object);
                    break;
                }
            }
            if (unknown) {
                String[] traitArry = object.split("/");
                switch (traitArry.length) {
                    case 1:
                        switch (traitArry[0]) {
                            case "Any": {
                                output = ProgramDataSingleton.TraitsMaster().stream().filter((trait) -> (!trait.NoBuy)).map((trait) -> "," + trait.Name()).reduce(output, String::concat);
                                break;
                            }
                            default: {
                                output = ProgramDataSingleton.TraitsMaster().stream().filter((trait) -> (!trait.NoBuy)).map((trait) -> "," + trait.Name()).reduce(output, String::concat);
                                break;
                            }
                        }
                        break;
                    default:
                        switch (traitArry[traitArry.length - 1]) {
                            case "Any": {
                                String test = "";
                                for (int x = 0; x < traitArry.length - 1; x++) {
                                    if (!test.equals("")) {
                                        test += "/";
                                    }
                                    test += traitArry[x];
                                }
                                for (LogBookEntry log : Parent.TraitMaster.GetLogs()) {
                                    if (log.Description().split("/").length >= traitArry.length - 1) {
                                        String[] mastertraitarry = log.Description().split("/");
                                        String mastertrait = "";
                                        for (int x = 0; x < traitArry.length - 1; x++) {
                                            if (!mastertrait.equals("")) {
                                                mastertrait += "/";
                                            }
                                            mastertrait += mastertraitarry[x];
                                        }
                                        if (mastertrait.equals(test)) {
                                            output += "," + log.Description();
                                        }
                                    }
                                }
                                break;
                            }
                            case "Affiliation": {
                                for (Rank rank : ProgramDataSingleton.RankMaster) {
                                    if ((rank.Affiliation.equals(Parent.Affiliation) || rank.Affiliation.equals(Parent.Subaffiliation))
                                            && ((rank.YearsExcluded && (StartingYear > rank.EndingYear || StartingYear < rank.BeginningYear))
                                            || (!rank.YearsExcluded && StartingYear < rank.EndingYear && StartingYear > rank.BeginningYear))) {
                                        output += ",Rank/" + rank.Name;
                                    }
                                }
                                if (Parent.BirthAffiliationType == AffiliationType.Clan || Parent.AffiliationType == AffiliationType.Clan) {
                                    for (Rank rank : ProgramDataSingleton.RankMaster) {
                                        if (rank.Affiliation.equals("Clan")
                                                && ((rank.YearsExcluded && (StartingYear > rank.EndingYear || StartingYear < rank.BeginningYear))
                                                || (!rank.YearsExcluded && StartingYear < rank.EndingYear && StartingYear > rank.BeginningYear))) {
                                            output += ",Rank/" + rank.Name;
                                        }
                                    }
                                }
                                for (Rank rank : ProgramDataSingleton.RankMaster) {
                                    if (rank.Affiliation.equals("General")
                                            && ((rank.YearsExcluded && (StartingYear > rank.EndingYear || StartingYear < rank.BeginningYear))
                                            || (!rank.YearsExcluded && StartingYear < rank.EndingYear && StartingYear > rank.BeginningYear))) {
                                        output += ",Rank/" + rank.Name;
                                    }
                                }
                                break;
                            }
                            case "General": {
                                for (Rank rank : ProgramDataSingleton.RankMaster) {
                                    if (rank.Affiliation.equals("General")
                                            && ((rank.YearsExcluded && (StartingYear > rank.EndingYear || StartingYear < rank.BeginningYear))
                                            || (!rank.YearsExcluded && StartingYear < rank.EndingYear && StartingYear > rank.BeginningYear))) {
                                        output += ",Rank/" + rank.Name;
                                    }
                                }
                                break;
                            }
                            default: {
                                System.out.println("Unknown Trait: " + object);
                                break;
                            }
                        }
                        break;
                }
            }
        }
        return output.split(",");
    }

    protected String[] breakSkillString(String input) {

        String[] orArray = input.split("\\|");

        String output = "";

        for (String object : orArray) {
            Boolean unknown = true;
            for (LogBookEntry skill : Parent.SkillMaster.GetLogs()) {
                if (object.equals(skill.Description())) {
                    unknown = false;
                    output += ("," + object);
                    break;
                }
            }
            if (unknown) {
                String[] skillArry = object.split("/");
                if (skillArry.length == 1) {
                    switch (skillArry[0]) {
                        case "Any": {
                            for (LogBookEntry skill : Parent.SkillMaster.GetLogs()) {
                                output += "," + skill.Description();
                            }
                            break;
                        }
                        default: {
                            System.out.println("Unknown Skill: " + object + " in " + input);
                            break;
                        }
                    }

                } else {
                    String in;
                    if (skillArry[skillArry.length - 1].split("!").length > 1) {
                        in = skillArry[skillArry.length - 1].split("!")[0];
                    } else {
                        in = skillArry[skillArry.length - 1];
                    }
                    Boolean loop = true;
                    while (loop) {
                        do {
                            loop = false;
                            switch (in) {
                                case "Any": {
                                    for (LogBookEntry skill : Parent.SkillMaster.GetLogs()) {
                                        if (skill.Description().split("/")[0].equals(skillArry[0])) {
                                            Boolean newSkill = true;
                                            for (String test : output.split(",")) {
                                                if (skill.Description().equals(test)) {
                                                    newSkill = false;
                                                }
                                            }
                                            if (newSkill) {
                                                output += "," + skill.Description();
                                            }
                                        }
                                    }

                                    break;
                                }
                                case "Any Homeworld Clan":
                                case "Any Clan": {
                                    output += "," + skillArry[0] + "/Blood Spirit," + skillArry[0] + "/Cloud Cobra," + skillArry[0] + "/Coyote," + skillArry[0] + "/Fire Mandrill," + skillArry[0] + "/Goliath Scorpion," + skillArry[0] + "/Ice Hellion," + skillArry[0] + "/Star Adder," + skillArry[0] + "/Steel Viper";
                                    if (in.equals("Any Clan")) {
                                        in = "Any Invading Clan";
                                        loop = true;
                                    }
                                    break;
                                }
                                case "Any Invading Clan": {
                                    output += "," + skillArry[0] + "/Diamond Shark," + skillArry[0] + "/Ghost Bear," + skillArry[0] + "/Hell's Horses," + skillArry[0] + "/Jade Falcon," + skillArry[0] + "/Nova Cat," + skillArry[0] + "/Snow Raven," + skillArry[0] + "/Wolf";
                                    break;
                                }
                                case "Any Inner Sphere": {
                                    for (Module mod : ProgramDataSingleton.ModulesMaster) {
                                        if (mod.Stage == 0) {
                                            Stage0Module zeromod = (Stage0Module) mod;
                                            if (zeromod.AffType == AffiliationType.InnerSphere) {
                                                output += "," + skillArry[0] + "/" + zeromod.Name;
                                                for (Stage0Module sub : zeromod.Subaffiliations) {
                                                    output += "," + skillArry[0] + "/" + sub.Name;
                                                }
                                            }
                                        }
                                    }
                                    break;
                                }
                                case "Affiliation": {
                                    if (skillArry[0].equals("Language")) {
                                        String text = "";
                                        for (String l : Parent.PrimaryLanguages.split(",")) {
                                            if (!"".equals(text)) {
                                                text += "|";
                                            }
                                            text += "Language/" + l;
                                        }
                                        if (!Parent.SecondaryLanguages.equals("None")) {
                                            for (String l : Parent.SecondaryLanguages.split(",")) {
                                                if (!"".equals(text)) {
                                                    text += "|";
                                                }
                                                text += "Language/" + l;
                                            }
                                        }
                                        String[] a = breakSkillString(text);
                                        if (a.length == 1 || (a.length == 2 && a[0].equals(""))) {
                                            output = a[a.length - 1];
                                        } else {
                                            for (String o : a) {
                                                output += "," + o;
                                            }
                                        }
                                        break;
                                    } else {
                                        String text = "";
                                        if (Parent.AffiliationType == AffiliationType.Clan) {
                                            text = skillArry[0] + "/Clan|" + skillArry[0] + "/" + Parent.Affiliation.split("/")[0];
                                        } else if (Parent.Subaffiliation.equals("Generic") || Parent.Subaffiliation.equals("Spacer") || Parent.Subaffiliation.equals("Pirate") || Parent.Subaffiliation.equals("Mercenary")) {
                                            String[] a = breakSkillString(skillArry[0] + "/Any");
                                            for (String o : a) {
                                                if (!"".equals(text)) {
                                                    text += "|";
                                                }
                                                text += o;
                                            }
                                        } else if (Parent.Affiliation.equals("ComStar") || Parent.Affiliation.equals("Word of Blake")) {
                                            String priaff = "";
                                            for (Module mod : Parent.modulesTaken) {
                                                if (mod.Stage == 0) {
                                                    priaff = mod.MiscText.split(",")[0];
                                                }
                                            }
                                            switch (Parent.AffiliationType) {
                                                case InnerSphere: {
                                                    String[] aff = priaff.split("/");
                                                    for (String a : aff) {
                                                        if (!text.equals("")) {
                                                            text += "|";
                                                        }
                                                        text += skillArry[0] + "/" + a;
                                                    }
                                                    break;
                                                }
                                                default: {
                                                    text += skillArry[0] + "/" + priaff.split("/")[0];
                                                    break;
                                                }
                                            }
                                            if (skillArry[0].equals("Protocol")) {
                                                text += "|Protocol/" + Parent.Affiliation;
                                            }
                                        } else {
                                            text = skillArry[0] + "/" + Parent.Affiliation;
                                            if (!"".equals(Parent.Subaffiliation) && !"None".equals(Parent.Subaffiliation) && !Parent.Affiliation.equals(Parent.Subaffiliation)) {
                                                text += "|" + skillArry[0] + "/" + Parent.Subaffiliation;
                                            }
                                        }

                                        String[] a = breakSkillString(text);
                                        if (a.length == 1 || (a.length == 2 && a[0].equals(""))) {
                                            output = a[a.length - 1];
                                        } else {
                                            for (String o : a) {
                                                output += "," + o;
                                            }
                                        }
                                    }
                                    break;
                                }
                                case "Nearest State": {
                                    if (NearestState != null && !"".equals(NearestState)) {
                                        switch (skillArry[0]) {
                                            case "Protocol": {
                                                output += ",Protocol/" + NearestState;
                                                break;
                                            }
                                        }
                                    }
                                    break;
                                }
                                case "Aff": {
                                    String affil = skillArry[skillArry.length - 1].split("\\!")[1];
                                    switch (skillArry[0]) {
                                        case "Language": {
                                            for (Module mastermod : ProgramDataSingleton.ModulesMaster) {
                                                if (affil.equals(mastermod.Name)) {
                                                    Stage0Module mod = (Stage0Module) mastermod;
                                                    for (String lang : mod.PrimaryLanguage("").split(",")) {
                                                        Boolean skip = false;
                                                        for (String checklang : output.split(",")) {
                                                            if (checklang.equals("Language/" + lang)) {
                                                                skip = true;
                                                            }
                                                        }
                                                        if (!skip) {
                                                            output += ",Language/" + lang;
                                                        }
                                                    }
                                                    for (String lang : mod.SecondaryLanguage("").split(",")) {
                                                        Boolean skip = false;
                                                        for (String checklang : output.split(",")) {
                                                            if (checklang.equals("Language/" + lang)) {
                                                                skip = true;
                                                            }
                                                        }
                                                        if (!skip) {
                                                            output += ",Language/" + lang;
                                                        }
                                                    }
                                                    break;
                                                }
                                            }
                                            break;
                                        }
                                    }
                                    break;
                                }
                                default: {
                                    System.out.println("Unknown Skill: " + object + " in " + input);
                                    break;
                                }
                            }

                        } while (loop);
                    }

                }
            }
        }

        return output.split(",");
    }

    protected String[] breakRuleString(String input) {
        String[] inputs = input.split("%");
        String output = "";
        switch (inputs[0]) {
            case "Choice": {  //Allows a mixed choice (IE skills and triats).  Mostly used for flexible XP.
                for (String x : inputs[1].split("\\+")[0].split("\\|")) {
                    switch (x.split("\\@")[0]) {
                        case "Attribute": {
                            if ("Any".equals(x.split("\\@")[1])) {
                                output += ",Attribute:Strength,Attribute:Body,Attribute:Reflexes,Attribute:Dexterity,Attribute:Intelligence,Attribute:Willpower,Attribute:Charisma,Attribute:Edge";
                            }
                            break;
                        }
                        case "Trait": {
                            String[] traitReturn = breakTraitString(x.split("@")[1]);
                            for (int i = 1; i < traitReturn.length; i++) {
                                output += ",Trait:" + traitReturn[i];
                            }
                            break;
                        }
                        case "Skill": {
                            String[] skillReturn = breakSkillString(x.split("@")[1]);
                            for (int i = 1; i < skillReturn.length; i++) {
                                output += ",Skill:" + skillReturn[i];
                            }
                            break;
                        }
                        default: {
                            output = "You haven't implemented something yet.";
                            break;
                        }
                    }
                }
                break;
            }
            case "FieldFlex": { //Takes skills from Skill Fields taken; If a FieldFlex output returns empty, the alotted XP is automatically tranferred to something else.             
                for (Module mod : Parent.modulesTaken) {
                    if (!mod.TakenSkillFields.isEmpty()) {
                        for (SkillField sf : mod.TakenSkillFields) {
                            for (String s : sf.Skills) {
                                output += "," + s;
                            }
                        }
                    }
                }
                break;
            }
            case "TakenFieldSkills": { //Takes skills from Skill Fields taken; If it returns empty, it is converted to a number of selections from any skill.
                if (inputs[1].equals("All")) {
                    for (Module mod : Parent.modulesTaken) {
                        for (SkillField sf : mod.TakenSkillFields) {
                            for (String skill : sf.Skills) {
                                output += "," + skill;
                            }
                        }
                    }
                } else {
                    for (String field : inputs[1].split("\\+")[0].split("\\|")) {
                        Boolean breakout = false;
                        for (Module mod : Parent.modulesTaken) {
                            for (SkillField sf : mod.TakenSkillFields) {
                                if (field.equals(sf.Name)) {
                                    for (String skill : sf.Skills) {
                                        output += "," + skill;
                                    }
                                    breakout = true;
                                }
                            }
                            if (breakout) {
                                break;
                            }
                        }
                    }
                }
                break;
            }
            case "TakenFieldSkillsShift": { //Take skills from the fields primary list, if the character has them.  Otherwise populates from the secondary list.
                Boolean secondary = true;
                String[] primaryfields = inputs[1].split("\\|");
                String[] secondaryfields = inputs[2].split("\\|");
                for (String field : primaryfields) {
                    for (Module mod : Parent.modulesTaken) {
                        for (SkillField sf : mod.TakenSkillFields) {
                            if (field.equals(sf.Name)) {
                                secondary = false;
                                for (String skill : sf.Skills) {
                                    output += "," + skill;
                                }
                            }
                        }
                    }
                }
                if (secondary) {
                    for (String field : secondaryfields) {
                        for (SkillField sf : ProgramDataSingleton.SkillFieldMaster) {
                            if (field.equals(sf.Name)) {
                                for (String skill : sf.Skills) {
                                    output += "," + skill;
                                }
                            }
                        }

                    }
                }
                break;
            }
            case "TakenFieldTypeSkills": {
                for (String field : inputs[1].split("\\+")[0].split("|")) {
                    for (Module mod : Parent.modulesTaken) {
                        for (SkillField sf : mod.TakenSkillFields) {
                            if (sf.Type.equals(field)) {
                                for (String skill : sf.Skills) {
                                    output += "," + skill;
                                }
                            }
                        }
                    }
                }
                break;
            }
            default: {
                output = inputs[0] + " is not a choice rule.";
                break;
            }
        }
        if (inputs[1].split("\\+").length == 2) {
            String text = "";
            for (String t : output.split(",")) {
                int len = t.split(":").length - 1;
                if (!"".equals(t)) {
                    if (!text.equals("")) {
                        text += "|";
                    }
                    text += t.split(":")[len];
                }
            }
            text += "|" + inputs[1].split("\\+")[1];
            String[] o = breakSkillString(text);
            output = "";
            for (String t : o) {
                if (!t.equals("")) {
                    output += "," + t;
                }
            }
        }
        return output.split(",");
    }
}
