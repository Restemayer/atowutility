/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUIComponents;

import atowutility.ProgramDataSingleton;

/**
 *
 * @author Chris
 */
public class SelectionPaneObject extends javax.swing.JPanel {

    public String PropertyString() {
        return null;
    }

    String[] options;
    String Type;
    int Selections;
    int[] XP;
    int XPAllocated;
    Boolean Repeatable;
    ModulePanel modulepanel;
    java.awt.Frame ParentFrame;

    public SelectionPaneObject(String[] o, String type, int[] xp, ModulePanel panel, java.awt.Frame frame, Boolean repeatable) {
        options = ProgramDataSingleton.RemoveDuplicates(o);
        Repeatable = repeatable;
        modulepanel = panel;
        ParentFrame = frame;
        for (int i = 0; i < options.length; i++){
            if ("ZZZZZ".equals(options[i].split("/")[options[i].split("/").length - 1])){                
                String newstring = "";
                for (int y = 0; y < options[i].split("/").length - 1; y++)
                    newstring += options[i].split("/")[y] + "/";
                newstring += "[NEW]";                
                options[i] = newstring;               
            }                
        }
        XP = xp;
        Type = type;
    }
    private int UseableXP(){
        int xp = 0;
        for (int x = 0; x < Selections; x++)
            xp += XP[x];
        return xp;
    }    
    public Boolean XPAllocated(){
        return XPAllocated == UseableXP();
    }
    
    
}


