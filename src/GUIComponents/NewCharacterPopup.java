/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUIComponents;

import atowutility.ProgramDataSingleton;
import atowutility.enums.AffiliationType;
import atowutility.classes.LogBook;
import atowutility.classes.LogBookEntry;
import atowutility.classes.Module;
import atowutility.classes.Rank;
import atowutility.classes.Skill;
import atowutility.classes.SkillField;
import atowutility.classes.Stage0Module;
import atowutility.classes.Trait;
import atowutility.enums.SkillFieldCatagory;
import atowutility.enums.Stage3Type;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.swing.Box;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

/**
 *
 * @author Chris
 */
public class NewCharacterPopup extends javax.swing.JFrame {

    java.awt.Frame Parent;
    int Stage;
    int StartingYear;
    int[] Attributes;
    int[] Prerequisites;
    final int[][] PhenoModifiers;
    final int[][] AttributeMaximums;
    int StartingXP;
    int CurrentXP;
    double AgeAdjustment;
    int PrerequisiteFailures;
    int PhenoIndex;
    Boolean skip3;
    String Affiliation;
    String Subaffiliation;
    String StartingClanCaste;
    String CurrentClanCaste;
    String PrimaryLanguages;
    String SecondaryLanguages;
    LogBook TraitMaster;
    LogBook Traits;
    LogBook SkillMaster;
    LogBook Skills;
    LogBook Restrictions;
    LogBook ExtraPrerequisites;
    LogBook PhenoTraits;
    String Rules;

    List<Module> modulesTaken;
    AffiliationType AffiliationType;
    AffiliationType BirthAffiliationType;

    private NewCharacterPopup() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public int CurrentXP() {
        return CurrentXP;
    }

    /**
     * Creates new form NewCharacterPopup
     *
     * @param startingdate
     */
    public NewCharacterPopup(java.awt.Frame parent, int startingdate) {
        Parent = parent;
        PhenoIndex = 0;
        PhenoTraits = new LogBook("Phenotype traits", false);
        initComponents();
        StartingYear = startingdate;
        StartingXP = 5000;
        CurrentXP = StartingXP;
        Stage = 0;
        AgeAdjustment = 0;
        BackButton.setEnabled(false);
        ForwardButton.setEnabled(false);
        modulesTaken = new ArrayList<>();
        Affiliation = "";
        Subaffiliation = "";
        PrimaryLanguages = "";
        SecondaryLanguages = "";
        Attributes = new int[8];
        Prerequisites = new int[8];
        Skills = new LogBook("New Character Skills", false);
        SkillMaster = new LogBook("Working Master Skills List", false);
        Traits = new LogBook("New Character Traits", false);
        TraitMaster = new LogBook("Working Master Traits List", false);
        Restrictions = new LogBook("New Character Restrictions", false);
        ExtraPrerequisites = new LogBook("New Character Extra Prerequisites", false);
        SetTitleText();
        AgeLabel.setText("");
        BirthYearLabel.setText("");
        PhenoModifiers = new int[4][];
        PhenoModifiers[0] = new int[]{0, 0, 0, 0, 0, 0, 0, 0}; //Human
        PhenoModifiers[1] = new int[]{-1, -1, 2, 2, 0, 0, 0, 0}; //Aerospace
        PhenoModifiers[2] = new int[]{2, 1, 0, -1, 0, 0, 0, 0}; //Elemental        
        PhenoModifiers[3] = new int[]{0, 0, 1, 1, 0, 0, 0, 0}; //Mechwarrior
        AttributeMaximums = new int[4][];
        AttributeMaximums[0] = new int[]{8, 8, 8, 8, 8, 8, 9, 9}; //Human
        AttributeMaximums[1] = new int[]{7, 7, 9, 9, 9, 8, 8, 8}; //Aerospace
        AttributeMaximums[2] = new int[]{9, 9, 8, 7, 8, 9, 8, 8}; //Elemental
        AttributeMaximums[3] = new int[]{8, 8, 9, 9, 8, 8, 9, 8}; //Mechwarrior

        DisplayStage();
    }

    private void DisplayStage() {
        if (Stage == 3) {
            skip3 = false;
        }

        if (Stage >= 3) {
            ForwardButton.setEnabled(true);
        } else {
            ForwardButton.setEnabled(false);
        }

        for (Module mod : modulesTaken) {
            if (mod.Rules != null && !mod.Rules.equals("")) {
                for (String rule : mod.Rules.split(",")) {
                    if (rule.split(":")[0].split("#")[0].equals("Skip") && Integer.parseInt(rule.split(":")[0].split("#")[1]) == Stage) {
                        skip3 = true;
                        Stage++;
                    }
                }
            }
        }
        if ((Stage == 4 && !skip3) || Stage > 4) {
            BackButton.setEnabled(true);
        } else {
            BackButton.setEnabled(false);
        }

        MainDisplay.removeAll();
        if (Stage == 0) {
            AffiliationType = AffiliationType.InnerSphere;
            BirthAffiliationType = AffiliationType.InnerSphere;
            SkillMaster.EraseLogs();
            TraitMaster.EraseLogs();
            for (Skill skill : ProgramDataSingleton.SkillsMaster()) {
                SkillMaster.Add(skill.Name, "0");
            }
            for (Trait trait : ProgramDataSingleton.TraitsMaster()) {
                TraitMaster.Add(trait.Name, "0");
            }
        }

        ProgramDataSingleton.ModulesMaster.stream().filter((mastermod) -> (mastermod.Stage == Stage)).forEachOrdered((mastermod) -> {
            Boolean restricted = false;
            for (LogBookEntry log : mastermod.Restrictions.GetLogs()) {
                String testedvalue = "Fucked";
                switch (log.Description().split("#")[0]) {
                    case "Affiliation": {
                        String[] restrict = log.Value().split("\\|");
                        for (String r : restrict){
                        if (r.equals(Affiliation) || r.equals(Affiliation + "/" + Subaffiliation)) {
                            testedvalue = Affiliation + " or " + Affiliation + "/" + Subaffiliation;
                            restricted = true;
                        }
                        }
                        break;
                    }
                    case "!Affiliation": {
                        restricted = true;
                        String[] required = log.Value().split("\\|");
                        testedvalue = Affiliation + " or " + Affiliation + "/" + Subaffiliation;
                        for (String r : required) {
                            if (r.equals(Affiliation) || r.equals(Affiliation + "/" + Subaffiliation)) {
                                restricted = false;
                            }
                        }
                        break;
                    }
                    case "AffiliationType": {
                        String[] restrict = log.Value().split("\\|");
                        for (String r : restrict){
                        if (AffiliationType.toString().equals(r)) {
                            testedvalue = AffiliationType.toString();
                            restricted = true;
                        }
                        }
                        break;
                    }
                    case "!AffiliationType": {
                        restricted = true;
                        String[] required = log.Value().split("\\|");
                        testedvalue = AffiliationType.toString();
                        for (String r : required){
                        if (AffiliationType.toString().equals(r)) {
                            restricted = false;
                        }
                        }
                        break;
                    }
                    case "!Phenotype": {
                        restricted = true;
                        String[] required = log.Value().split("\\|");
                         for (String r : required){
                        testedvalue = PhenoComboBox.getSelectedItem().toString();
                        if (PhenoComboBox.getSelectedItem().toString().equals(r)) {
                            restricted = false;
                        }
                         }
                        break;
                    }
                    case "Stage": {
                        int stagenum = Integer.parseInt(log.Description().split("#")[1]);
                          String[] restrict = log.Value().split("\\|");
                        for (String r : restrict){
                        for (Module mod : modulesTaken) {
                            if (mod.Stage == stagenum && mod.Name.equals(r)) {
                                testedvalue = mod.Name;
                                restricted = true;
                            }
                        }
                        }
                        break;
                    }
                    case "!Stage": {
                        restricted = true;
                        int stagenum = Integer.parseInt(log.Description().split("#")[1]);
                        String[] required = log.Value().split("\\|");
                        for (String r : required) {
                            for (Module mod : modulesTaken) {
                                if (mod.Stage == stagenum && mod.Name.equals(r)) {
                                    testedvalue = mod.Name;
                                    restricted = false;
                                    break;
                                }
                                if (!restricted) {
                                    break;
                                }
                            }
                            if (!restricted) {
                                break;
                            }
                        }
                        break;
                    }
                    case "Caste": {
                        String[] restrict = log.Value().split("\\|");
                        testedvalue = CurrentClanCaste;
                        for (String r : restrict) {
                            if (r.equals(testedvalue)) {
                                restricted = true;
                            }
                        }
                        break;
                    }
                    case "!Caste": {
                        restricted = true;
                        String[] required = log.Value().split("\\|");
                        testedvalue = CurrentClanCaste;
                        for (String r : required) {
                            if (r.equals(testedvalue)) {
                                restricted = false;
                            }
                        }
                        break;
                    }
                    case "!FieldType":{
                        restricted = true;
                        String[] required = log.Value().split("\\|");
                        for (String r : required) {
                            for (Module mod : modulesTaken) {
                                for (SkillField sf : mod.TakenSkillFields){
                                if (sf.Type.equals(r)) {
                                    testedvalue = mod.Name;
                                    restricted = false;
                                    break;
                                }
                                if (!restricted) {
                                    break;
                                }
                            }
                            }
                            if (!restricted) {
                                break;
                            }
                        }
                        break;
                    }
                    case "!FieldCatagory":{
                        restricted = true;
                        String[] required = log.Value().split("\\|");
                        for (String r : required) {
                            for (Module mod : modulesTaken) {
                                for (SkillField sf : mod.TakenSkillFields){
                                if (sf.Catagory.equals(SkillFieldCatagory.valueOf(r))) {
                                    testedvalue = mod.Name;
                                    restricted = false;
                                    break;
                                }
                                if (!restricted) {
                                    break;
                                }
                            }
                            }
                            if (!restricted) {
                                break;
                            }
                        }
                        break;
                    }
                }

                if (restricted) {
                    //System.out.println("Failed test " + log.Description().split("#")[0] + " for " + mastermod.Name + ": RequiredValue(" + log.Value() + "), TestedValue(" + testedvalue + ")");
                    break;
                }
            }
            if (Stage == 3) {

                Boolean[] Stage3TypeTaken = new Boolean[]{false, false, false, false}; //Civilian,Police,Military,OCS
                for (Module mod : modulesTaken) {
                    if (mod.Stage3Type == Stage3Type.Civilian) {
                        Stage3TypeTaken[0] = true;
                    }
                    if (mod.Stage3Type == Stage3Type.Police_Intelligence) {
                        Stage3TypeTaken[1] = true;
                    }
                    if (mod.Stage3Type == Stage3Type.Military) {
                        Stage3TypeTaken[2] = true;
                    }
                    if (mod.Stage3Type == Stage3Type.OCS) {
                        Stage3TypeTaken[3] = true;
                    }
                }

                if ((mastermod.Stage3Type == Stage3Type.Civilian && Stage3TypeTaken[0] == true) || (mastermod.Stage3Type == Stage3Type.Police_Intelligence && Stage3TypeTaken[1] == true) || (mastermod.Stage3Type == Stage3Type.Military && Stage3TypeTaken[2] == true) || (mastermod.Stage3Type == Stage3Type.OCS && Stage3TypeTaken[3] == true)) {
                    restricted = true;
                }

            }

            for (LogBookEntry log : Restrictions.GetLogs()) {
                if (log.Value().equals("X") && log.Description().equals("Stage#" + Stage + "%" + mastermod.Name)) {
                    restricted = true;
                }
            }

            if (Stage == 4 && !mastermod.Repeatable) {
                for (Module mod : modulesTaken) {
                    if (mod.Stage == 4 && mod.Name.equals(mastermod.Name)) {
                        restricted = true;
                    }
                }
            }

            if (!restricted) {
                switch (Stage) {
                    case 0: {
                        Stage0Module module = (Stage0Module) mastermod;
                        Stage0ModulePanel panel = new Stage0ModulePanel(module, StartingYear, this);
                        MainDisplay.add(panel);
                        break;
                    }
                    case 3: {
                        if (mastermod.Stage == Stage) {

                            Stage3ModulePanel panel = new Stage3ModulePanel(mastermod, StartingYear, this);
                            MainDisplay.add(panel);

                            break;
                        } else {
                            break;
                        }
                    }
                    default: {
                        if (mastermod.Stage == Stage) {

                            Stage124ModulePanel panel = new Stage124ModulePanel(mastermod, StartingYear, this);
                            MainDisplay.add(panel);

                            break;
                        } else {
                            break;
                        }
                    }
                }
            }
        });
        SetTitleText();
        UpdateRecord();
        revalidate();
        repaint();
    }

    private void SetTitleText() {
        switch (Stage) {
            case 0: {
                StageLabel.setText("Stage 0: Affiliation");
                break;
            }
            case 1: {
                StageLabel.setText("Stage 1: Early Childhood");
                break;
            }
            case 2: {
                StageLabel.setText("Stage 2: Late Childhood");
                break;
            }
            case 3: {
                StageLabel.setText("Stage 3: Higher Education");
                break;
            }
            case 4: {
                StageLabel.setText("Stage 4: Real Life");
                break;
            }
        }
    }

    private void UpdateRecord() {
        RecordDetail.removeAll();
        ModulesDisplay.removeAll();
        CurrentClanCaste = StartingClanCaste;
        if (Stage > 0) {
            int bYear = (int) Math.floor(BirthYear());
            int cAge = (int) Math.floor(CurrentAge());
            String agetext = CurrentAge() + " Years Old ";
            if (bYear + cAge != StartingYear) {
                agetext += "(" + (bYear + cAge) + ")";
            }
            AgeLabel.setText(agetext);
            AddYearButton.setVisible(true);
            SubtractYearButton.setVisible(true);
            if (Stage > 1) {
                BirthYearLabel.setText("Birth Year: " + bYear + " (Estimated)");
            } else {
                BirthYearLabel.setText("");
            }
            String adjText = "";
            if (AgeAdjustment != 0) {
                if (AgeAdjustment > 0) {
                    adjText = "-" + (int) Math.floor(AgeAdjustment) + " Years";
                } else {
                    int adjfix = (int) Math.floor(AgeAdjustment) * -1;
                    adjText = "+" + adjfix + " Years";
                }
            }
            AdjLabel.setText(adjText);
        } else {
            AgeLabel.setText("");
            BirthYearLabel.setText("");
            AdjLabel.setText("");
            AddYearButton.setVisible(false);
            SubtractYearButton.setVisible(false);
        }
        for (int x = 0; x < 8; x++) {
            Attributes[x] = 0;
            Prerequisites[x] = 1;
        }
        Skills.EraseLogs();
        Traits.EraseLogs();
        for (LogBookEntry log : PhenoTraits.GetLogs()) {
            Traits.Add(log.Description(), log.Value());
        }
        ExtraPrerequisites.EraseLogs();
        SkillMaster.EraseLogs();
        TraitMaster.EraseLogs();
        for (Skill skill : ProgramDataSingleton.SkillsMaster()) {
            SkillMaster.Add(skill.Name, "0");
        }
        for (Trait trait : ProgramDataSingleton.TraitsMaster()) {
            TraitMaster.Add(trait.Name, "0");
        }
        Rules = "";
        CurrentXP = StartingXP;
        PrerequisiteFailures = 0;
        Boolean fieldstaken = false;
        for (Module mod : modulesTaken) {
            if (mod.TakenSkillFields.size() > 0) {
                fieldstaken = true;
            }
            for (LogBookEntry log : mod.Attributes.GetLogs()) {
                switch (log.Description()) {
                    case "Strength": {
                        Attributes[0] += Integer.parseInt(log.Value());
                        break;
                    }
                    case "Body": {
                        Attributes[1] += Integer.parseInt(log.Value());
                        break;
                    }
                    case "Reflexes": {
                        Attributes[2] += Integer.parseInt(log.Value());
                        break;
                    }
                    case "Dexterity": {
                        Attributes[3] += Integer.parseInt(log.Value());
                        break;
                    }
                    case "Intelligence": {
                        Attributes[4] += Integer.parseInt(log.Value());
                        break;
                    }
                    case "Willpower": {
                        Attributes[5] += Integer.parseInt(log.Value());
                        break;
                    }
                    case "Charisma": {
                        Attributes[6] += Integer.parseInt(log.Value());
                        break;
                    }
                    case "Edge": {
                        Attributes[7] += Integer.parseInt(log.Value());
                        break;
                    }
                }
            }
            for (LogBookEntry log : mod.Prerequisites.GetLogs()) {
                switch (log.Description()) {
                    case "Strength": {
                        if (Integer.parseInt(log.Value()) > Prerequisites[0]) {
                            Prerequisites[0] = Integer.parseInt(log.Value());
                        }
                        break;
                    }
                    case "Body": {
                        if (Integer.parseInt(log.Value()) > Prerequisites[1]) {
                            Prerequisites[1] = Integer.parseInt(log.Value());
                        }
                        break;
                    }
                    case "Reflexes": {
                        if (Integer.parseInt(log.Value()) > Prerequisites[2]) {
                            Prerequisites[2] = Integer.parseInt(log.Value());
                        }
                        break;
                    }
                    case "Dexterity": {
                        if (Integer.parseInt(log.Value()) > Prerequisites[3]) {
                            Prerequisites[3] = Integer.parseInt(log.Value());
                        }
                        break;
                    }
                    case "Intelligence": {
                        if (Integer.parseInt(log.Value()) > Prerequisites[4]) {
                            Prerequisites[4] = Integer.parseInt(log.Value());
                        }
                        break;
                    }
                    case "Willpower": {
                        if (Integer.parseInt(log.Value()) > Prerequisites[5]) {
                            Prerequisites[5] = Integer.parseInt(log.Value());
                        }
                        break;
                    }
                    case "Charisma": {
                        if (Integer.parseInt(log.Value()) > Prerequisites[6]) {
                            Prerequisites[6] = Integer.parseInt(log.Value());
                        }
                        break;
                    }
                    case "Edge": {
                        if (Integer.parseInt(log.Value()) > Prerequisites[7]) {
                            Prerequisites[7] = Integer.parseInt(log.Value());
                        }
                        break;
                    }
                    default: {
                        ExtraPrerequisites.AddHigher(log.Description(), log.Value());
                    }
                }
            }
            mod.Skills.GetLogs().forEach((log) -> {
                Skills.Add(log.Description(), log.Value());
            });

            mod.Traits.GetLogs().forEach((log) -> {
                Traits.Add(log.Description(), log.Value());
            });
            for (LogBookEntry log : Traits.GetLogs()) {
                TraitMaster.Add(log.Description(), "0");
            }
            for (LogBookEntry log : Skills.GetLogs()) {
                Boolean newskill = SkillMaster.AddReturnNew(log.Description(), "0");
                if (newskill) {
                    TraitMaster.Add("Natural Aptitude/" + log.Description(), "0");
                }
            }
            mod.Restrictions.GetLogs().forEach((log) -> {
                Restrictions.Add(log.Description(), log.Value());
            });
            if (!"".equals(Rules)) {
                Rules += ",";
            }
            Rules += mod.Rules;

            CurrentXP -= mod.Cost();
        }
        for (String r : Rules.split(",")) {
            if (!"".equals(r) && r != null && r.split(":").length > 1) {
                String testString = r.split(":")[0];
                String resultvalue = r.split(":")[1];
                if (testString.split("%")[0].equals("Prerequisite")) { //(Examples) Prerequisite%!Stage#1@Slave%Trait@Citizen:-1; Prerequisite%Field@Pilot-Jumpship%Trait@Rank/Any:3
                    String test = testString.split("%")[1];
                    String result = testString.split("%")[2].split("@")[0] + "%" + testString.split("%")[2].split("@")[1];
                    switch (test.split("@")[0].split("#")[0]) {
                        case "Field": { //Applies prerequisite if SkillField has been taken
                            Boolean found = false;
                            for (Module mod : modulesTaken) {
                                for (SkillField sf : mod.TakenSkillFields) {
                                    if (sf.Name.equals(test.split("@")[1])) {
                                        ExtraPrerequisites.AddHigher(result, resultvalue);
                                        found = true;
                                        break;
                                    }
                                }
                                if (found) {
                                    break;
                                }
                            }
                        }
                        case "!Field": { //Applies prerequisite if SkillField has not been taken
                            Boolean notfound = true;
                            for (Module mod : modulesTaken) {
                                for (SkillField sf : mod.TakenSkillFields) {
                                    if (sf.Name.equals(test.split("@")[1])) {

                                        notfound = false;
                                    }
                                }

                            }
                            if (notfound) {
                                ExtraPrerequisites.AddHigher(result, resultvalue);
                                break;
                            }
                        }
                        case "AffiliationType": { //Applied prerequisite if character is of the Affiliation Type (Inner Sphere,Clan,etc)
                            if (test.equals(AffiliationType)) {
                                ExtraPrerequisites.AddHigher(result, resultvalue);
                            }
                            break;
                        }
                        case "!Affiliation": {
                            if (!test.equals(Affiliation)) { //Applies prerequisite if the character is not of the Affilation
                                ExtraPrerequisites.AddHigher(result, resultvalue);
                            }
                            break;
                        }
                        case "!Stage": { //Applies prerequisite if the character does not have a module at a particular stage
                            if (Integer.parseInt(test.split("@")[0].split("#")[1]) < Stage) {
                                Boolean notTaken = true;
                                for (Module mod : modulesTaken) {
                                    if (mod.Stage == Integer.parseInt(test.split("@")[0].split("#")[1]) && mod.Name.equals(test.split("@")[1])) {
                                        notTaken = false;
                                    }
                                }
                                if (notTaken) {
                                    ExtraPrerequisites.AddHigher(result, resultvalue);
                                }
                            }
                            break;
                        }
                        case "!Stage3Type": { //Applies the prerequisite if the character does not have a Stage 3 module of a particular type (Military,Police_Intelligence,etc)
                            Boolean hasModule = false;
                            String[] types = test.split("\\|");
                            for (String t : types) {
                                for (Module mod : modulesTaken) {
                                    if (mod.Stage == 3 && mod.Stage3Type == Stage3Type.valueOf(t)) {
                                        hasModule = true;
                                    }
                                }
                            }
                            if (!hasModule) {
                                ExtraPrerequisites.AddHigher(result, resultvalue);
                            }
                            break;
                        }
                        default: {
                            System.out.println("Bad prerequisite command: " + test.split("@")[0].split("#")[0]);
                            break;
                        }
                    }
                }
            }
        }

        String currentPhenoSelection = PhenoComboBox.getSelectedItem().toString();
        for (ActionListener l : PhenoComboBox.getActionListeners()) {
            PhenoComboBox.removeActionListener(l);
        }
        PhenoComboBox.removeAllItems();
        PhenoComboBox.setModel(PhenoComboBoxOptions());
        if (((DefaultComboBoxModel) PhenoComboBox.getModel()).getIndexOf(currentPhenoSelection) != -1) {
            PhenoComboBox.setSelectedItem(currentPhenoSelection);
        }
        if (!PhenoComboBox.getSelectedItem().toString().equals("Normal Human")) {
            ExtraPrerequisites.AddHigher("Trait%Trueborn", "NA");
        }
        PhenoComboBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                PhenoComboBoxActionPerformed(evt);
            }
        });
        PhenoUpdate();
        CurrentXP += (AgeAdjustment * -100);

        JLabel attributeLabel = new JLabel();
        attributeLabel.setText("-------- Attributes --------");
        attributeLabel.setFont(new Font(attributeLabel.getFont().getFontName(), Font.BOLD, attributeLabel.getFont().getSize()));
        attributeLabel.setHorizontalAlignment(SwingConstants.CENTER);
        RecordDetail.add(attributeLabel);
        for (int x = 0; x < 8; x++) {
            JLabel attrib = new JLabel();
            attrib.setHorizontalAlignment(SwingConstants.LEFT);
            String text = "";
            int modifier = 0;
            switch (x) {
                case 0: {
                    if (Math.floor(Attributes[x] / 100) + PhenoModifiers[PhenoIndex][x] > AttributeMaximums[PhenoIndex][7] + ExceptionalAttributeCheck("Strength")) {
                        modifier = AttributeMaximums[PhenoIndex][x] - (int) Math.floor(Attributes[x] / 100);
                    } else {
                        modifier = PhenoModifiers[PhenoIndex][x];
                    }
                    text = "Strength: ";
                    break;
                }
                case 1: {
                    if (Math.floor(Attributes[x] / 100) + PhenoModifiers[PhenoIndex][x] > AttributeMaximums[PhenoIndex][7] + ExceptionalAttributeCheck("Body")) {
                        modifier = AttributeMaximums[PhenoIndex][x] - (int) Math.floor(Attributes[x] / 100);
                    } else {
                        modifier = PhenoModifiers[PhenoIndex][x];
                    }
                    text = "Body: ";
                    break;
                }
                case 2: {
                    if (Math.floor(Attributes[x] / 100) + PhenoModifiers[PhenoIndex][x] > AttributeMaximums[PhenoIndex][7] + ExceptionalAttributeCheck("Reflexes")) {
                        modifier = AttributeMaximums[PhenoIndex][x] - (int) Math.floor(Attributes[x] / 100);
                    } else {
                        modifier = PhenoModifiers[PhenoIndex][x];
                    }
                    text = "Reflexes: ";
                    break;
                }
                case 3: {
                    if (Math.floor(Attributes[x] / 100) + PhenoModifiers[PhenoIndex][x] > AttributeMaximums[PhenoIndex][7] + ExceptionalAttributeCheck("Dexterity")) {
                        modifier = AttributeMaximums[PhenoIndex][x] - (int) Math.floor(Attributes[x] / 100);
                    } else {
                        modifier = PhenoModifiers[PhenoIndex][x];
                    }
                    text = "Dexterity: ";
                    break;
                }
                case 4: {
                    if (Math.floor(Attributes[x] / 100) + PhenoModifiers[PhenoIndex][x] > AttributeMaximums[PhenoIndex][7] + ExceptionalAttributeCheck("Intelligence")) {
                        modifier = AttributeMaximums[PhenoIndex][x] - (int) Math.floor(Attributes[x] / 100);
                    } else {
                        modifier = PhenoModifiers[PhenoIndex][x];
                    }
                    text = "Intelligence: ";
                    break;
                }
                case 5: {
                    if (Math.floor(Attributes[x] / 100) + PhenoModifiers[PhenoIndex][x] > AttributeMaximums[PhenoIndex][7] + ExceptionalAttributeCheck("Willpower")) {
                        modifier = AttributeMaximums[PhenoIndex][x] - (int) Math.floor(Attributes[x] / 100);
                    } else {
                        modifier = PhenoModifiers[PhenoIndex][x];
                    }
                    text = "Willpower: ";
                    break;
                }
                case 6: {
                    if (Math.floor(Attributes[x] / 100) + PhenoModifiers[PhenoIndex][x] > AttributeMaximums[PhenoIndex][7] + ExceptionalAttributeCheck("Charisma")) {
                        modifier = AttributeMaximums[PhenoIndex][x] - (int) Math.floor(Attributes[x] / 100);
                    } else {
                        modifier = PhenoModifiers[PhenoIndex][x];
                    }
                    text = "Charisma: ";
                    break;
                }
                case 7: {
                    if (Math.floor(Attributes[x] / 100) + PhenoModifiers[PhenoIndex][x] > AttributeMaximums[PhenoIndex][7] + ExceptionalAttributeCheck("Edge")) {
                        modifier = AttributeMaximums[PhenoIndex][x] - (int) Math.floor(Attributes[x] / 100);
                    } else {
                        modifier = PhenoModifiers[PhenoIndex][x];
                    }
                    text = "Edge: ";
                    break;
                }
            }
            text += Attributes[x] + " XP, Score: ";
            int prescore = (int) Math.floor(Attributes[x] / 100);
            int score = (int) Math.floor(Attributes[x] / 100) + modifier;
            if (score <= 0) {
                score = 0;
                attrib.setForeground(Color.red);
            }
            text += score + " ";
            if (modifier > 0) {
                text += "(" + prescore + " + " + modifier + ")";
            } else if (modifier < 0) {
                text += "(" + prescore + " - " + (modifier * -1) + ")";
            }

            attrib.setText(text);
            RecordDetail.add(attrib);
        }
        RecordDetail.add(Box.createVerticalStrut(5));
        JLabel traitLabel = new JLabel("-------- Traits --------");
        traitLabel.setFont(new Font(traitLabel.getFont().getFontName(), Font.BOLD, traitLabel.getFont().getSize()));
        traitLabel.setHorizontalAlignment(SwingConstants.CENTER);
        RecordDetail.add(traitLabel);
        for (LogBookEntry log : Traits.GetLogs()) {
            Boolean restricted = false;
            for (LogBookEntry rlog : Restrictions.GetLogs()) {
                if (rlog.Value().equals("X") && rlog.Description().split("%")[0].equals("Trait") && rlog.Description().split("%")[1].equals(log.Description())) {
                    restricted = true;
                }
            }
            JLabel t = new JLabel();
            String description;
            if (restricted) {
                description = "<s>" + log.Description() + "</s>";
            } else {
                description = log.Description();
            }
            int score;
            String text = description + ": " + log.Value() + " XP ";
            if (Integer.parseInt(log.Value()) < 0) {
                score = (int) Math.ceil(Integer.parseInt(log.Value()) / 100);
            } else {
                score = (int) Math.floor(Integer.parseInt(log.Value()) / 100);
            }

            for (Trait master : ProgramDataSingleton.TraitsMaster()) {
                if (master.Name.equals(log.Description())) {
                    if ((score <= master.MaximumTP() && master.MaximumTP() < 0) || (score >= master.MinimumTP() && master.MinimumTP() >= 0) || (master.MaximumTP() > 0 && master.MinimumTP() < 0)) {
                        text += "(";
                        if (score > 0) {
                            text += "+";
                        }
                        text += score + " TP)";

                        if (score > 0 && !restricted) {

                            if (log.Description().split("/")[0].equals("Rank")) {
                                String title = "";
                                Boolean officer = false;
                                for (Module mod : modulesTaken) {
                                    if (mod.Name.equals("Officer Candidate School")) {
                                        officer = true;
                                    }
                                }
                                for (Rank r : ProgramDataSingleton.RankMaster) {
                                    if (r.Name.equals(log.Description().split("/")[1])) {
                                        if (r.Affiliation.equals("Clan")) {
                                            text += " " + r.EnlistedRanks[score][0];
                                        } else if (officer) {
                                            text += " O-" + score + " " + r.OfficerRanks[score][0];
                                        } else {
                                            text += " E-" + score + " " + r.EnlistedRanks[score][0];
                                        }
                                    }
                                }

                            }
                            t.setForeground(new Color(0, 128, 0));
                        }
                        if (score < 0) {
                            t.setForeground(Color.red);
                        }
                    } else {
                        t.setForeground(Color.GRAY);
                    }
                }
            }
            t.setText("<html>" + text + "</html>");
            RecordDetail.add(t);
        }

        RecordDetail.add(Box.createVerticalStrut(5));
        JLabel skillLabel = new JLabel();
        String txt = "-------- Skills ";
        double multiplier = 1;
        int learnerxp = 0;
        for (LogBookEntry tlog : Traits.GetLogs()) {
            if (tlog.Description().equals("Fast Learner") || tlog.Description().equals("Slow Learner")) {
                learnerxp += Integer.parseInt(tlog.Value());
            }

        }
        if (learnerxp >= 300) {
            multiplier = 0.8;
            txt += "(Fast Learner) ";
        }
        if (learnerxp <= -300) {
            multiplier = 1.2;
            txt += "(Slow Learner) ";
        }

        txt += "--------";
        skillLabel.setText(txt);
        skillLabel.setFont(new Font(skillLabel.getFont().getFontName(), Font.BOLD, skillLabel.getFont().getSize()));
        skillLabel.setHorizontalAlignment(SwingConstants.CENTER);
        RecordDetail.add(skillLabel);

        int scoretable[] = {20, 30, 50, 80, 120, 170, 230, 300, 380, 470, 570};
        for (int x : scoretable) {
            x = (int) Math.floor(x * multiplier);
        }

        for (LogBookEntry log : Skills.GetLogs()) {
            JLabel sk = new JLabel();
            String text = log.Description() + ": " + log.Value() + " XP ";
            int skillxp = Integer.parseInt(log.Value());
            String addon = "";
            if (skillxp >= scoretable[0]) {
                for (int x = 0; x < scoretable.length; x++) {
                    if (skillxp >= scoretable[x]) {
                        addon = "(" + x + ")";
                    }
                }
            } else {
                sk.setForeground(Color.GRAY);
            }
            sk.setText(text + addon);
            RecordDetail.add(sk);
        }
        for (Module mod : modulesTaken) {
            if (AffiliationType == AffiliationType.Clan && !CurrentClanCaste.equals("Dark Caste (Pirate)") && !mod.ChangeCaste.equals("")) {
                CurrentClanCaste = mod.ChangeCaste;
                mod.MiscText = "Caste changed: " + mod.ChangeCaste;
            }
            TakenModulePanel panel = new TakenModulePanel(mod, this);
            ModulesDisplay.add(panel);
        }

        //Building fields sections
        if (fieldstaken) {
            RecordDetail.add(Box.createVerticalStrut(5));
            JLabel sfLabel = new JLabel("-------- Skill Fields --------");
            sfLabel.setFont(new Font(traitLabel.getFont().getFontName(), Font.BOLD, sfLabel.getFont().getSize()));
            sfLabel.setHorizontalAlignment(SwingConstants.CENTER);
            RecordDetail.add(sfLabel);
            for (Module mod : modulesTaken) {
                for (SkillField sf : mod.TakenSkillFields) {
                    CurrentXP += sf.Refund();
                    String text = "<html>" + sf.Name;
                    if (sf.Refund() > 0) {
                        text += " <font color='green'>(" + sf.Refund() + " XP Refunded)</font>";
                    }
                    text += "</html>";
                    JLabel sfl = new JLabel(text);
                    RecordDetail.add(sfl);
                }
            }
        }
        //Building prerequisites section
        RecordDetail.add(Box.createVerticalStrut(5));
        JLabel prerequisitesLabel = new JLabel();
        prerequisitesLabel.setText("-------- Unmet Prerequisites --------");
        prerequisitesLabel.setFont(new Font(prerequisitesLabel.getFont().getFontName(), Font.BOLD, prerequisitesLabel.getFont().getSize()));
        prerequisitesLabel.setHorizontalAlignment(SwingConstants.CENTER);
        RecordDetail.add(prerequisitesLabel);

        for (int x = 0; x < 8; x++) {
            if ((int) Math.floor(Attributes[x] / 100) + PhenoModifiers[PhenoIndex][x] < Prerequisites[x]) {
                PrerequisiteFailures++;
                String labeltext;
                switch (x) {
                    case 0: {
                        labeltext = "Strength: ";
                        break;
                    }
                    case 1: {
                        labeltext = "Body: ";
                        break;
                    }
                    case 2: {
                        labeltext = "Reflexes: ";
                        break;
                    }
                    case 3: {
                        labeltext = "Dexterity: ";
                        break;
                    }
                    case 4: {
                        labeltext = "Intelligence: ";
                        break;
                    }
                    case 5: {
                        labeltext = "Willpower: ";
                        break;
                    }
                    case 6: {
                        labeltext = "Charisma: ";
                        break;
                    }
                    case 7: {
                        labeltext = "Edge: ";
                        break;
                    }
                    default:
                        labeltext = "";
                }
                labeltext += Prerequisites[x];
                JLabel a = new JLabel(labeltext);
                a.setForeground(Color.red);
                RecordDetail.add(a);
            }

        }
        for (LogBookEntry log : ExtraPrerequisites.GetLogs()) {
            int value = 0;
            if (!log.Value().equals("NA")) {
                value = Integer.parseInt(log.Value());
            }

            switch (log.Description().split("%")[0].split("#")[0]) {
                case "Trait": {
                    Boolean unmet = true;
                    int length = log.Description().split("%")[1].split("/").length;
                    int test;
                    if (log.Description().split("%")[1].split("/")[log.Description().split("%")[1].split("/").length - 1].equals("Any")) {
                        test = 2;
                    } else {
                        test = 1;
                    }
                    String trait = log.Description().split("%")[1].split("/")[log.Description().split("%")[1].split("/").length - test];
                    for (LogBookEntry tlog : Traits.GetLogs()) {
                        if (tlog.Description().split("/").length - test >= 0 && tlog.Description().split("/")[tlog.Description().split("/").length - test].equals(trait) && tlog.Description().split("/")[0].equals(log.Description().split("%")[1].split("/")[0])) {
                            for (Trait mastertrait : ProgramDataSingleton.TraitsMaster()) {
                                if (tlog.Description().split("/")[0].equals(mastertrait.Name.split("/")[0])) {
                                    if ((Integer.parseInt(tlog.Value()) >= mastertrait.MinimumTP() * 100 && mastertrait.MinimumTP() >= 0) || (Integer.parseInt(tlog.Value()) <= mastertrait.MaximumTP() * 100 && mastertrait.MaximumTP() <= 0) && (Integer.parseInt(log.Value()) == -1 || Math.floor(Integer.parseInt(tlog.Value()) / 100) >= Integer.parseInt(log.Value()))) {
                                        unmet = false;
                                    }
                                }
                            }
                        }

                    }
                    if (unmet) {
                        PrerequisiteFailures++;
                        String text = "Trait: " + log.Description().split("%")[1];
                        if (!log.Value().equals("-1")) {
                            text += " at " + log.Value() + " TP";
                        }
                        JLabel a = new JLabel(text);
                        a.setForeground(Color.red);
                        RecordDetail.add(a);
                    }

                    break;
                }
                case "!Trait": {
                    Boolean unmet = false;
                    int length = log.Description().split("%")[1].split("/").length;
                    int test;
                    if (log.Description().split("%")[1].split("/")[log.Description().split("%")[1].split("/").length - 1].equals("Any")) {
                        test = 2;
                    } else {
                        test = 1;
                    }
                    String trait = log.Description().split("%")[1].split("/")[log.Description().split("%")[1].split("/").length - test];
                    for (LogBookEntry tlog : Traits.GetLogs()) {
                        if (tlog.Description().split("/")[tlog.Description().split("/").length - test].equals(trait) && tlog.Description().split("/")[0].equals(log.Description().split("%")[1].split("/")[0])) {
                            for (Trait mastertrait : ProgramDataSingleton.TraitsMaster()) {
                                if (tlog.Description().split("/")[0].equals(mastertrait.Name.split("/")[0])) {
                                    if ((Integer.parseInt(tlog.Value()) >= mastertrait.MinimumTP() * 100 && mastertrait.MinimumTP() >= 0) || (Integer.parseInt(tlog.Value()) <= mastertrait.MaximumTP() * 100 && mastertrait.MaximumTP() <= 0)) {
                                        unmet = true;
                                    }
                                }
                            }
                        }

                    }
                    if (unmet) {
                        PrerequisiteFailures++;
                        JLabel a = new JLabel("Must not have trait: " + log.Description().split("%")[1]);
                        a.setForeground(Color.red);
                        RecordDetail.add(a);
                    }

                    break;
                }
                case "TraitXP": {
                    int testvalue = value;
                    for (LogBookEntry plog : Traits.GetLogs()) {
                        if (plog.Description().equals(log.Description().split("\\%")[1])) {
                            testvalue -= Integer.parseInt(plog.Value());
                        }
                    }
                    if (value > 0) {
                        PrerequisiteFailures++;
                        JLabel a = new JLabel("Trait: " + log.Description().split("%")[1] + " requires " + testvalue + " more XP");
                        a.setForeground(Color.red);
                        RecordDetail.add(a);
                    }
                    break;
                }
                case "SkillXP": {
                    int testvalue = value;
                    for (LogBookEntry plog : Skills.GetLogs()) {
                        if (plog.Description().equals(log.Description().split("\\%")[1])) {
                            testvalue -= Integer.parseInt(plog.Value());
                        }
                    }
                    if (value > 0) {
                        PrerequisiteFailures++;
                        JLabel a = new JLabel("Skill: " + log.Description().split("%")[1] + " requires " + testvalue + " more XP");
                        a.setForeground(Color.red);
                        RecordDetail.add(a);
                    }
                    break;
                }
                case "Sum": {
                    int testvalue = value;
                    String[] tests = log.Description().split("\\%")[1].split("\\@")[1].split("\\|");
                    switch (log.Description().split("\\%")[1].split("\\@")[0]) {
                        case "Trait": {
                            for (LogBookEntry plog : Traits.GetLogs()) {
                                for (String test : tests) {
                                    if (plog.Description().equals(test)) {
                                        testvalue -= (int) Math.floor(Integer.parseInt(plog.Value()) / 100);
                                    }
                                }
                            }

                            if (testvalue > 0) {
                                PrerequisiteFailures++;
                                String labeltext = "Total of " + value + " TP in";
                                for (int x = 0; x < tests.length; x++) {
                                    if (x != 0) {
                                        labeltext += ",";
                                    }
                                    if (x == tests.length - 1) {
                                        labeltext += " or";
                                    }
                                    labeltext += " " + tests[x];
                                }
                                JLabel a = new JLabel(labeltext);
                                a.setForeground(Color.red);
                                RecordDetail.add(a);
                            }
                            break;
                        }
                    }
                    break;
                }
                case "Field": {
                    Boolean found = false;
                    for (Module mod : modulesTaken) {
                        for (SkillField sf : mod.TakenSkillFields) {
                            if (sf.Name.equals(log.Description().split("%")[1])) {
                                found = true;
                            }
                        }
                    }
                    if (!found) {
                        PrerequisiteFailures++;
                        JLabel a = new JLabel("Skill Field: " + log.Description().split("%")[1]);
                        a.setForeground(Color.red);
                        RecordDetail.add(a);
                    }
                    break;
                }
                case "!Stage": {
                    int stage = Integer.parseInt(log.Description().split("%")[0].split("#")[1]);
                    Boolean hasModule = false;
                    for (Module mod : modulesTaken) {
                        if (mod.Name.equals(log.Description().split("%")[1])) {
                            hasModule = true;
                        }
                    }
                    if (!hasModule) {
                        PrerequisiteFailures++;
                        JLabel a = new JLabel("Stage " + stage + " Module: " + log.Description().split("%")[1]);
                        a.setForeground(Color.red);
                        RecordDetail.add(a);
                    }
                    break;
                }
            }
        }
        if (PrerequisiteFailures == 0) {
            JLabel a = new JLabel("No failing prerequisites");
            a.setForeground(new Color(0, 128, 0));
            RecordDetail.add(a);
        }

        RecordDetail.add(Box.createVerticalStrut(5));
        JLabel restrictionsLabel = new JLabel();
        restrictionsLabel.setText("-------- Restrictions --------");
        restrictionsLabel.setFont(new Font(restrictionsLabel.getFont().getFontName(), Font.BOLD, restrictionsLabel.getFont().getSize()));
        restrictionsLabel.setHorizontalAlignment(SwingConstants.CENTER);
        RecordDetail.add(restrictionsLabel);
        for (LogBookEntry log : Restrictions.GetLogs()) {
            if (log.Value().equals("X")) {
                String text = "No " + log.Description().split("%")[1];
                JLabel r = new JLabel(text);
                RecordDetail.add(r);
            }
        }
        XPLabel.setText(CurrentXP + " XP");
        revalidate();
        repaint();
    }

    int defaultage;

    private double ModYears() {
        double age = 0;
        for (Module mod : modulesTaken) {
            age += mod.Years;
        }

        if (age - AgeAdjustment < 0) {
            AgeAdjustment = age;
        }

        return age;
    }

    private double CurrentAge() {
        double age = ModYears();
        if (Stage > 3) {
            age -= AgeAdjustment;
        }

        return age;
    }

    private int BirthYear() {
        ModYears();
        if (AffiliationType == AffiliationType.Clan) {
            defaultage = 18;
        } else {
            defaultage = 21;
        }
        defaultage -= +(int) Math.floor(AgeAdjustment);
        if (CurrentAge() > defaultage) {
            return StartingYear - (int) CurrentAge();
        } else {
            return StartingYear - defaultage;
        }
    }

    public void DeleteModule(int stage) {
        if (stage < 3) {
            for (Module mod : modulesTaken) {
                if (mod.Stage >= stage) {
                    mod.RemoveMe = true;
                }
            }

            Stage = stage;
            DisplayStage();
        }

        modulesTaken.removeIf((Module mod) -> mod.RemoveMe == true);
        UpdateRecord();
    }

    public void AddModule(Module module) {
        modulesTaken.add(module);
        Collections.sort(modulesTaken, (final Module obj1, final Module obj2) -> obj1.Stage - obj2.Stage);
        if (Stage < 3) {
            Stage++;
        }
        DisplayStage();
    }

    private int ExceptionalAttributeCheck(String input) {
        for (LogBookEntry log : Traits.GetLogs()) {
            if (log.Description().split("/")[0].equals("Exceptional Attribute") && log.Description().split("/")[1].equals(input) && Integer.parseInt(log.Value()) >= 200) {
                return 1;
            }
        }
        return 0;
    }

    private void PhenoUpdate() {
        PhenoTraits.EraseLogs();
        switch (PhenoComboBox.getSelectedItem().toString()) {
            case "Normal Human": {
                PhenoIndex = 0;
                break;
            }
            case "Aerospace": {
                PhenoIndex = 1;
                PhenoTraits.Add("G-Tolerance", "100");
                PhenoTraits.Add("Glass Jaw", "-300");
                PhenoTraits.Add("Field Aptitude/Clan Fighter Pilot", "0");
                break;
            }
            case "Elemental": {
                PhenoIndex = 2;
                PhenoTraits.Add("Toughness", "300");
                PhenoTraits.Add("Field Aptitude/Elemental", "0");
                break;
            }
            case "Mechwarrior": {
                PhenoIndex = 3;
                PhenoTraits.Add("Field Aptitude/Clan Mechwarrior", "0");
                break;
            }
            case "Aerospace (Ghost Bear)": {
                PhenoIndex = 3;
                PhenoTraits.Add("Field Aptitude/Clan Fighter Pilot", "0");
                break;
            }
        }
    }

    private ComboBoxModel PhenoComboBoxOptions() {
        String[] options;
        if (Stage == 0 || BirthAffiliationType != AffiliationType.Clan) {
            PhenoComboBox.setEnabled(false);
            options = new String[]{"Normal Human"};

        } else {
            Boolean Trueborn = false;

            for (Module mod : modulesTaken) {
                if (mod.Stage == 1 && mod.Name.equals("Trueborn Crèche")) {
                    Trueborn = true;
                }
            }

            for (LogBookEntry log : Traits.GetLogs()) {
                if (log.Description().equals("Trueborn") && Integer.parseInt(log.Value()) >= 200) {
                    Trueborn = true;
                }
            }
            if (Trueborn && !"Ghost Bear".equals(Affiliation)) {
                PhenoComboBox.setEnabled(true);
                options = new String[]{"Aerospace", "Elemental", "Mechwarrior"};
            } else if (Trueborn && "Ghost Bear".equals(Affiliation)) {
                PhenoComboBox.setEnabled(true);
                options = new String[]{"Aerospace (Ghost Bear)", "Elemental", "Mechwarrior"};
            } else if ("Ghost Bear".equals(Affiliation)) {
                PhenoComboBox.setEnabled(true);
                options = new String[]{"Normal Human", "Aerospace (Ghost Bear)", "Elemental", "Mechwarrior"};
            } else {
                PhenoComboBox.setEnabled(true);
                options = new String[]{"Normal Human", "Aerospace", "Elemental", "Mechwarrior"};
            }
        }
        for (LogBookEntry log : Restrictions.GetLogs()) {
            if (log.Description().equals("!Phenotype")) {
                PhenoComboBox.setEnabled(false);
                options = new String[]{log.Value()};
            }
        }

        return new DefaultComboBoxModel(options);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        RecordPanel = new javax.swing.JPanel();
        XPLabel = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        RecordDetail = new javax.swing.JPanel();
        AgeLabel = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        BirthYearLabel = new javax.swing.JLabel();
        SubtractYearButton = new javax.swing.JButton();
        AddYearButton = new javax.swing.JButton();
        PhenoComboBox = new javax.swing.JComboBox<>();
        jLabel1 = new javax.swing.JLabel();
        AdjLabel = new javax.swing.JLabel();
        StageLabel = new javax.swing.JLabel();
        MainDisplayPane = new javax.swing.JPanel();
        DisplayScrollPane = new java.awt.ScrollPane();
        MainDisplay = new javax.swing.JPanel();
        BackButton = new javax.swing.JButton();
        CancelButton = new javax.swing.JButton();
        ForwardButton = new javax.swing.JButton();
        ModulesPanel = new javax.swing.JPanel();
        ModulesDisplay = new javax.swing.JPanel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("New Character");
        setPreferredSize(new java.awt.Dimension(2210, 1300));
        setResizable(false);
        setType(java.awt.Window.Type.POPUP);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });

        RecordPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Current Record", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 16))); // NOI18N
        RecordPanel.setPreferredSize(new java.awt.Dimension(500, 1300));

        XPLabel.setFont(new java.awt.Font("Space Bd BT", 0, 48)); // NOI18N
        XPLabel.setText("5000 XP");

        RecordDetail.setLayout(new javax.swing.BoxLayout(RecordDetail, javax.swing.BoxLayout.PAGE_AXIS));
        jScrollPane1.setViewportView(RecordDetail);

        AgeLabel.setFont(new java.awt.Font("Space Bd BT", 1, 24)); // NOI18N
        AgeLabel.setText("24 Years Old");

        BirthYearLabel.setFont(new java.awt.Font("Space Bd BT", 0, 18)); // NOI18N
        BirthYearLabel.setText("Birth Year: 3050 (Estimated)");

        SubtractYearButton.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        SubtractYearButton.setText("-");
        SubtractYearButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                SubtractYearButtonActionPerformed(evt);
            }
        });

        AddYearButton.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        AddYearButton.setText("+");
        AddYearButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AddYearButtonActionPerformed(evt);
            }
        });

        PhenoComboBox.setFont(new java.awt.Font("Space Bd BT", 0, 16)); // NOI18N
        PhenoComboBox.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Normal Human", "Aerospace", "Elemental", "Mechwarrior" }));
        PhenoComboBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                PhenoComboBoxActionPerformed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Space Bd BT", 0, 16)); // NOI18N
        jLabel1.setText("Phenotype");

        AdjLabel.setFont(new java.awt.Font("Space Bd BT", 0, 16)); // NOI18N
        AdjLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        AdjLabel.setText("jLabel2");

        javax.swing.GroupLayout RecordPanelLayout = new javax.swing.GroupLayout(RecordPanel);
        RecordPanel.setLayout(RecordPanelLayout);
        RecordPanelLayout.setHorizontalGroup(
            RecordPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(RecordPanelLayout.createSequentialGroup()
                .addGroup(RecordPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(BirthYearLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 417, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(RecordPanelLayout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addGap(6, 6, 6)
                        .addComponent(PhenoComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, 280, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 417, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 434, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(RecordPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addGroup(RecordPanelLayout.createSequentialGroup()
                            .addComponent(XPLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 321, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(AdjLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGroup(RecordPanelLayout.createSequentialGroup()
                            .addComponent(AgeLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 321, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(9, 9, 9)
                            .addComponent(AddYearButton)
                            .addGap(9, 9, 9)
                            .addComponent(SubtractYearButton))))
                .addGap(1, 1, 1))
        );
        RecordPanelLayout.setVerticalGroup(
            RecordPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(RecordPanelLayout.createSequentialGroup()
                .addGroup(RecordPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(XPLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(RecordPanelLayout.createSequentialGroup()
                        .addGap(60, 60, 60)
                        .addComponent(AdjLabel)))
                .addGroup(RecordPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(AgeLabel)
                    .addGroup(RecordPanelLayout.createSequentialGroup()
                        .addGap(5, 5, 5)
                        .addGroup(RecordPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(AddYearButton)
                            .addComponent(SubtractYearButton))))
                .addComponent(BirthYearLabel)
                .addGroup(RecordPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(RecordPanelLayout.createSequentialGroup()
                        .addGap(3, 3, 3)
                        .addComponent(jLabel1))
                    .addComponent(PhenoComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(9, 9, 9)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(9, 9, 9)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 1016, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        StageLabel.setFont(new java.awt.Font("Space Bd BT", 1, 48)); // NOI18N
        StageLabel.setText("Stage");

        MainDisplayPane.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        MainDisplayPane.setPreferredSize(new java.awt.Dimension(1300, 1200));

        DisplayScrollPane.setPreferredSize(new java.awt.Dimension(1200, 0));
        DisplayScrollPane.addMouseWheelListener(new java.awt.event.MouseWheelListener() {
            public void mouseWheelMoved(java.awt.event.MouseWheelEvent evt) {
                DisplayScrollPaneMouseWheelMoved(evt);
            }
        });

        MainDisplay.addAncestorListener(new javax.swing.event.AncestorListener() {
            public void ancestorMoved(javax.swing.event.AncestorEvent evt) {
                MainDisplayAncestorMoved(evt);
            }
            public void ancestorAdded(javax.swing.event.AncestorEvent evt) {
            }
            public void ancestorRemoved(javax.swing.event.AncestorEvent evt) {
            }
        });
        MainDisplay.setLayout(new java.awt.GridLayout(0, 1));
        DisplayScrollPane.add(MainDisplay);

        javax.swing.GroupLayout MainDisplayPaneLayout = new javax.swing.GroupLayout(MainDisplayPane);
        MainDisplayPane.setLayout(MainDisplayPaneLayout);
        MainDisplayPaneLayout.setHorizontalGroup(
            MainDisplayPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(DisplayScrollPane, javax.swing.GroupLayout.DEFAULT_SIZE, 1146, Short.MAX_VALUE)
        );
        MainDisplayPaneLayout.setVerticalGroup(
            MainDisplayPaneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(DisplayScrollPane, javax.swing.GroupLayout.DEFAULT_SIZE, 1033, Short.MAX_VALUE)
        );

        BackButton.setFont(new java.awt.Font("Space Bd BT", 1, 18)); // NOI18N
        BackButton.setText("< Go Back");
        BackButton.setMaximumSize(new java.awt.Dimension(163, 31));
        BackButton.setMinimumSize(new java.awt.Dimension(163, 31));
        BackButton.setPreferredSize(new java.awt.Dimension(163, 31));
        BackButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BackButtonActionPerformed(evt);
            }
        });

        CancelButton.setFont(new java.awt.Font("Space Bd BT", 1, 18)); // NOI18N
        CancelButton.setText("Cancel");

        ForwardButton.setFont(new java.awt.Font("Space Bd BT", 1, 18)); // NOI18N
        ForwardButton.setText("Next Stage >");
        ForwardButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ForwardButtonActionPerformed(evt);
            }
        });

        ModulesPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Modules Taken", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 16))); // NOI18N

        ModulesDisplay.setLayout(new javax.swing.BoxLayout(ModulesDisplay, javax.swing.BoxLayout.PAGE_AXIS));

        javax.swing.GroupLayout ModulesPanelLayout = new javax.swing.GroupLayout(ModulesPanel);
        ModulesPanel.setLayout(ModulesPanelLayout);
        ModulesPanelLayout.setHorizontalGroup(
            ModulesPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(ModulesDisplay, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 526, Short.MAX_VALUE)
        );
        ModulesPanelLayout.setVerticalGroup(
            ModulesPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(ModulesPanelLayout.createSequentialGroup()
                .addComponent(ModulesDisplay, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(ModulesPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(BackButton, javax.swing.GroupLayout.PREFERRED_SIZE, 163, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(330, 330, 330)
                                .addComponent(CancelButton, javax.swing.GroupLayout.PREFERRED_SIZE, 163, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(ForwardButton))
                            .addComponent(MainDisplayPane, javax.swing.GroupLayout.PREFERRED_SIZE, 1150, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(StageLabel, javax.swing.GroupLayout.DEFAULT_SIZE, 1684, Short.MAX_VALUE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(RecordPanel, javax.swing.GroupLayout.DEFAULT_SIZE, 457, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(StageLabel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(0, 9, Short.MAX_VALUE)
                        .addComponent(MainDisplayPane, javax.swing.GroupLayout.PREFERRED_SIZE, 1037, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(BackButton, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(CancelButton, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(ForwardButton, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(ModulesPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
            .addComponent(RecordPanel, javax.swing.GroupLayout.DEFAULT_SIZE, 1223, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void ForwardButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ForwardButtonActionPerformed
        if (Stage < 4) {
            Stage++;
            DisplayStage();
        }
    }//GEN-LAST:event_ForwardButtonActionPerformed

    private void DisplayScrollPaneMouseWheelMoved(java.awt.event.MouseWheelEvent evt) {//GEN-FIRST:event_DisplayScrollPaneMouseWheelMoved

    }//GEN-LAST:event_DisplayScrollPaneMouseWheelMoved

    private void MainDisplayAncestorMoved(javax.swing.event.AncestorEvent evt) {//GEN-FIRST:event_MainDisplayAncestorMoved
        revalidate();
    }//GEN-LAST:event_MainDisplayAncestorMoved

    private void AddYearButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_AddYearButtonActionPerformed
        AgeAdjustment--;
        ModYears();
        UpdateRecord();
    }//GEN-LAST:event_AddYearButtonActionPerformed

    private void SubtractYearButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_SubtractYearButtonActionPerformed
        AgeAdjustment++;
        ModYears();
        UpdateRecord();
    }//GEN-LAST:event_SubtractYearButtonActionPerformed

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        Parent.setVisible(true);
    }//GEN-LAST:event_formWindowClosing

    private void PhenoComboBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_PhenoComboBoxActionPerformed
        if (PhenoComboBox.getItemAt(0) != null) {
            PhenoUpdate();
            DisplayStage();
        }
    }//GEN-LAST:event_PhenoComboBoxActionPerformed

    private void BackButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BackButtonActionPerformed
        Stage--;
        DisplayStage();
    }//GEN-LAST:event_BackButtonActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(NewCharacterPopup.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(NewCharacterPopup.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(NewCharacterPopup.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(NewCharacterPopup.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new NewCharacterPopup().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton AddYearButton;
    private javax.swing.JLabel AdjLabel;
    private javax.swing.JLabel AgeLabel;
    private javax.swing.JButton BackButton;
    private javax.swing.JLabel BirthYearLabel;
    private javax.swing.JButton CancelButton;
    private java.awt.ScrollPane DisplayScrollPane;
    private javax.swing.JButton ForwardButton;
    private javax.swing.JPanel MainDisplay;
    private javax.swing.JPanel MainDisplayPane;
    private javax.swing.JPanel ModulesDisplay;
    private javax.swing.JPanel ModulesPanel;
    private javax.swing.JComboBox<String> PhenoComboBox;
    private javax.swing.JPanel RecordDetail;
    private javax.swing.JPanel RecordPanel;
    private javax.swing.JLabel StageLabel;
    private javax.swing.JButton SubtractYearButton;
    private javax.swing.JLabel XPLabel;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator1;
    // End of variables declaration//GEN-END:variables
}
