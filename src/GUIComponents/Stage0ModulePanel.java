/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUIComponents;

import atowutility.ProgramDataSingleton;
import atowutility.enums.AffiliationType;
import atowutility.classes.LogBookEntry;
import atowutility.classes.Module;
import atowutility.classes.Stage0Module;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JLabel;

/**
 *
 * @author Chris
 */
public class Stage0ModulePanel extends ModulePanel {

    /**
     * Creates new form ModulePanel
     */
    Stage0Module main;
    Stage0Module BirthAffiliation;
    protected Stage0Module selected;

    List<String> AffiliationString;

    public Stage0ModulePanel(Stage0Module module, int startingyear, NewCharacterPopup parent) {
        super(module, startingyear, parent);
        this.main = module;
        AffiliationString = new ArrayList<>();
        AffiliationString.add("None");
        selectionPanels = new ArrayList<>();
        OptionPanelItems = new ArrayList<>();
        NewStuff = new ArrayList<>();

        for (Module mod : ProgramDataSingleton.ModulesMaster) {

            if ((mod.Stage == 0) && !mod.Name.equals(main.Name)) {
                Stage0Module zeromod = (Stage0Module) mod;
                AffiliationString.add(zeromod.Name);
                for (Stage0Module sub : zeromod.Subaffiliations) {
                    if (zeromod.Subaffiliations != null && !zeromod.Subaffiliations.isEmpty()) {
                        AffiliationString.add(zeromod.Name + "/" + sub.Name);
                    }
                }
            }
        }

        initComponents();
        if (main.AffType == AffiliationType.Clan) {
            jLabel5.setVisible(true);
            ClanCasteComboBox.setVisible(true);
            ClanCasteComboBox.removeAllItems();
            ClanCasteComboBox.setEnabled(false);
        } else {
            jLabel5.setVisible(false);
            ClanCasteComboBox.setVisible(false);
        }

        NameLabel.setText(module.Name);
        jLabel4.setVisible(false);
        NearestStateComboBox.setVisible(false);
        displayStats();
    }

    @Override
    public void UpdateSelections() {
        TakeModuleButton.setEnabled(true);

        drawText(false);
        repaint();
        revalidate();
    }

    private void displayStats() {
        selectionPanels.clear();
        OptionsPanel.removeAll();

        if (main.AffType == AffiliationType.InnerSphere || !SubAffComboBox.getSelectedItem().equals("None")) { //Forces to take a subaffiliation for everything except Inner Sphere
            if (main.PrimaryLanguage(SubAffComboBox.getSelectedItem().toString()).equals("Nearest State") || main.SecondaryLanguage(SubAffComboBox.getSelectedItem().toString()).equals("Nearest State") || ComStarBox.getSelectedIndex() != 0) {
                if (!NearestStateComboBox.isVisible()) {
                    jLabel4.setVisible(true);
                    NearestStateComboBox.setVisible(true);
                    NearestStateComboBox.setSelectedIndex(0);
                    NearestState = "";

                    if (NearestStateComboBox.getSelectedIndex() == 0) {
                        jLabel1.setEnabled(false);
                        jLabel3.setEnabled(false);
                        scrollPane1.setEnabled(false);
                        scrollPane2.setEnabled(false);
                        SubAffComboBox.setEnabled(false);
                        BirthAffComboBox.setEnabled(false);
                        InfoPanel.setVisible(false);
                        OptionsPanel.setVisible(false);
                    }
                }
            } else {
                NameLabel.setText(main.Name);
                jLabel4.setVisible(false);
                NearestStateComboBox.setVisible(false);
                NearestStateComboBox.setSelectedIndex(0);
                NearestState = "";
                jLabel1.setEnabled(true);
                jLabel3.setEnabled(true);
                scrollPane1.setEnabled(true);
                scrollPane2.setEnabled(true);
                SubAffComboBox.setEnabled(true);
                BirthAffComboBox.setEnabled(true);
                InfoPanel.setVisible(true);
                OptionsPanel.setVisible(true);
            }
            if (InfoPanel.isEnabled()) {
                drawText(true);
            }

            if ("None".equals(BirthAffComboBox.getSelectedItem().toString()) && "None".equals(ComStarBox.getSelectedItem().toString())) {
                SubAffLabel.setText("");
                SubAffLabel.setVisible(false);
            } else {
                String subafftext = "";
                if (!"None".equals(BirthAffComboBox.getSelectedItem().toString())) {
                    subafftext = BirthAffComboBox.getSelectedItem().toString() + " (Birth Affiliation)";
                }
                if (!"None".equals(ComStarBox.getSelectedItem().toString())) {
                    if (!"".equals(subafftext)) {
                        subafftext = ", " + subafftext;
                    }
                    if (!"None".equals(SubAffComboBox.getSelectedItem().toString())) {
                        subafftext = "/" + SubAffComboBox.getSelectedItem().toString() + subafftext;
                    }

                    subafftext = main.Name + subafftext;
                }
                SubAffLabel.setText(subafftext);
                SubAffLabel.setVisible(true);
            }
        } else {
            TakeModuleButton.setEnabled(false);
        }
        repaint();
        revalidate();

    }

    @Override
    protected void resetLists() {
        super.resetLists();

        //This section checks for a subaffiliation
        if (SubAffComboBox.getSelectedItem() != "None") {
            for (Module module : main.Subaffiliations) {
                if (((String) SubAffComboBox.getSelectedItem()).equals(module.Name)) {
                    for (LogBookEntry log : module.Attributes.GetLogs()) {
                        switch (log.Description()) {
                            case "Strength": {
                                Attributes[0] += Integer.parseInt(log.Value());
                                break;
                            }
                            case "Body": {
                                Attributes[1] += Integer.parseInt(log.Value());
                                break;
                            }
                            case "Reflexes": {
                                Attributes[2] += Integer.parseInt(log.Value());
                                break;
                            }
                            case "Dexterity": {
                                Attributes[3] += Integer.parseInt(log.Value());
                                break;
                            }
                            case "Intelligence": {
                                Attributes[4] += Integer.parseInt(log.Value());
                                break;
                            }
                            case "Willpower": {
                                Attributes[5] += Integer.parseInt(log.Value());
                                break;
                            }
                            case "Charisma": {
                                Attributes[6] += Integer.parseInt(log.Value());
                                break;
                            }
                            case "Edge": {
                                Attributes[7] += Integer.parseInt(log.Value());
                                break;
                            }
                            default: {
                                OptionPanelItems.add("Attribute:" + log.Description() + ":" + log.Value());
                            }
                        }
                    }
                    module.Traits.GetLogs().forEach((nlog) -> {
                        Traits.Add(nlog.Description(), nlog.Value());
                    });
                    module.Skills.GetLogs().forEach((nlog) -> {
                        Skills.Add(nlog.Description(), nlog.Value());
                    });
                    if (module.Rules != null && !"".equals(module.Rules)) {
                        for (String r : module.Rules.split(",")) {
                            if (r.split(":").length == 2) {
                                Rules.Add(r.split(":")[0], r.split(":")[1]);
                            } else {
                                Rules.Add(r, "-9999");
                            }
                        }
                    }
                }
            }

            //Checks clan castes
            if (main.AffType == AffiliationType.Clan && ClanCasteComboBox.isEnabled()) {
                switch (ClanCasteComboBox.getSelectedItem().toString()) {
                    case "Mechwarrior Caste": {
                        Attributes[2] += 75;
                        Attributes[3] += 75;
                        Attributes[5] += 75;
                        Attributes[6] += -25;
                        Attributes[7] += -50;
                        Traits.Add("Fit", "25");
                        Traits.Add("Impatient", "-50");
                        break;
                    }
                    case "Elemental Caste": {
                        Attributes[0] += 125;
                        Attributes[1] += 125;
                        Attributes[3] += -75;
                        Attributes[6] += -75;
                        Skills.Add("Martial Arts", "25");
                        break;
                    }
                    case "Elemental-Advanced Caste": {
                        Attributes[0] += 175;
                        Attributes[1] += 200;
                        Attributes[2] += -75;
                        Attributes[3] += -100;
                        Attributes[6] += -100;
                        Attributes[7] += -100;
                        Traits.Add("Patient", "25");
                        Traits.Add("Reputation", "100");
                        break;
                    }
                    case "Aerospace Caste":
                    case "Protomech Caste": {
                        Attributes[0] += -50;
                        Attributes[1] += -50;
                        Attributes[2] += 150;
                        Attributes[3] += 150;
                        Attributes[6] += -25;
                        Attributes[7] += -25;
                        Traits.Add("Fit", "25");
                        Traits.Add("Impatient", "-50");
                        break;
                    }
                    case "Aerospace-Naval Caste": {
                        Attributes[0] += -50;
                        Attributes[1] += -50;
                        Attributes[2] += 125;
                        Attributes[3] += 125;
                        Attributes[4] += 50;
                        Attributes[6] += -25;
                        Attributes[7] += -100;
                        Traits.Add("Compulsion/Arrogance", "-100");
                        Traits.Add("Patient", "75");
                        Traits.Add("Reputation", "75");
                        break;
                    }
                    case "Warrior Caste (Other)": {
                        Attributes[0] += 50;
                        Attributes[1] += 75;
                        Attributes[2] += 50;
                        Attributes[3] += 50;
                        Attributes[6] += -25;
                        Traits.Add("Reputation", "-75");
                        break;
                    }
                    case "Scientist Caste": {
                        Attributes[0] += -50;
                        Attributes[4] += 100;
                        Traits.Add("Compulsion/Arrogance", "-25");
                        Traits.Add("Patient", "100");
                        Traits.Add("Reputation", "-25");
                        Skills.Add("Interest/Any", "10");
                        Skills.Add("Science/Any", "15");
                        break;
                    }
                    case "Technician Caste": {
                        Attributes[3] += 100;
                        Attributes[4] += 20;
                        Attributes[6] += -50;
                        Traits.Add("Patient", "100");
                        Traits.Add("Reputation", "-75");
                        Skills.Add("Interest/Any", "15");
                        Skills.Add("Technician/Any", "15");
                        break;
                    }
                    case "Merchant Caste": {
                        Attributes[1] += -50;
                        Attributes[4] += 25;
                        Attributes[6] += 75;
                        Traits.Add("Gregarious", "100");
                        Traits.Add("Reputation", "-75");
                        Skills.Add("Appraisal", "10");
                        Skills.Add("Negotiation", "15");
                        Skills.Add("Protocol/Any", "10");
                        Skills.Add("Streetwise/Clan", "15");
                        break;
                    }
                    case "Laborer Caste": {
                        Attributes[0] += 125;
                        Attributes[1] += 100;
                        Attributes[2] += 50;
                        Attributes[3] += 50;
                        Attributes[4] += -50;
                        Attributes[6] += -50;
                        Traits.Add("Reputation", "-125");
                        Skills.Add("Career/Any", "15");
                        Skills.Add("Interest/Any", "10");
                        break;
                    }
                    case "Dark Caste (Pirate)": {
                        for (Module mod : ProgramDataSingleton.ModulesMaster) {
                            if (mod.Stage == 0 && mod.Name.equals("Independent")) {
                                Stage0Module zeromod = (Stage0Module) mod;
                                for (Stage0Module sub : zeromod.Subaffiliations) {
                                    if (sub.Name.equals("Pirate")) {
                                        for (LogBookEntry log : sub.Attributes.GetLogs()) {
                                            switch (log.Description()) {
                                                case "Strength": {
                                                    Attributes[0] += Integer.parseInt(log.Value());
                                                    break;
                                                }
                                                case "Body": {
                                                    Attributes[1] += Integer.parseInt(log.Value());
                                                    break;
                                                }
                                                case "Reflexes": {
                                                    Attributes[2] += Integer.parseInt(log.Value());
                                                    break;
                                                }
                                                case "Dexterity": {
                                                    Attributes[3] += Integer.parseInt(log.Value());
                                                    break;
                                                }
                                                case "Intelligence": {
                                                    Attributes[4] += Integer.parseInt(log.Value());
                                                    break;
                                                }
                                                case "Willpower": {
                                                    Attributes[5] += Integer.parseInt(log.Value());
                                                    break;
                                                }
                                                case "Charisma": {
                                                    Attributes[6] += Integer.parseInt(log.Value());
                                                    break;
                                                }
                                                case "Edge": {
                                                    Attributes[7] += Integer.parseInt(log.Value());
                                                    break;
                                                }
                                            }
                                        }
                                        sub.Traits.GetLogs().forEach((log) -> {
                                            Traits.Add(log.Description(), log.Value());
                                        });
                                        sub.Skills.GetLogs().forEach((log) -> {
                                            Skills.Add(log.Description(), log.Value());
                                        });
                                    }
                                }
                            }
                        }
                        break;
                    }
                    default: {
                        break;
                    }
                }
            }
        }

        //The next section is to check if a change affiliation rule has been applied.
        if (BirthAffComboBox.getSelectedItem() != "None") {
            for (int x = 0; x < Attributes.length; x++) {
                double y = Attributes[x] / 2;
                Attributes[x] = (int) Math.floor(y);
            }

            Traits.HalfValues();
            Skills.HalfValues();
            Rules.HalfValues();

            Cost = (int) Math.floor(Cost / 2);

            String[] birthaffname = ((String) BirthAffComboBox.getSelectedItem()).split("/");
            for (Module mastermod : ProgramDataSingleton.ModulesMaster) {
                if (mastermod.Name.equals(birthaffname[0])) {
                    Stage0Module module = (Stage0Module) mastermod;
                    Cost += (int) Math.floor(module.Cost() / 2);
                    for (LogBookEntry log : module.Attributes.GetLogs()) {
                        switch (log.Description()) {
                            case "Strength": {
                                Attributes[0] += Math.floor(Integer.parseInt(log.Value()) / 2);
                                break;
                            }
                            case "Body": {
                                Attributes[1] += Math.floor(Integer.parseInt(log.Value()) / 2);
                                break;
                            }
                            case "Reflexes": {
                                Attributes[2] += Math.floor(Integer.parseInt(log.Value()) / 2);
                                break;
                            }
                            case "Dexterity": {
                                Attributes[3] += Math.floor(Integer.parseInt(log.Value()) / 2);
                                break;
                            }
                            case "Intelligence": {
                                Attributes[4] += Math.floor(Integer.parseInt(log.Value()) / 2);
                                break;
                            }
                            case "Willpower": {
                                Attributes[5] += Math.floor(Integer.parseInt(log.Value()) / 2);
                                break;
                            }
                            case "Charisma": {
                                Attributes[6] += Math.floor(Integer.parseInt(log.Value()) / 2);
                                break;
                            }
                            case "Edge": {
                                Attributes[7] += Math.floor(Integer.parseInt(log.Value()) / 2);
                                break;
                            }
                        }
                    }
                    module.Traits.GetLogs().forEach((log) -> {
                        Traits.Add(log.Description(), Integer.toString((int) Math.floor(Integer.parseInt(log.Value()) / 2)));
                    });
                    module.Skills.GetLogs().forEach((log) -> {
                        Skills.Add(log.Description(), Integer.toString((int) Math.floor(Integer.parseInt(log.Value()) / 2)));
                    });

                    if (module.Rules != null && !"".equals(module.Rules)) {
                        for (String r : module.Rules.split(",")) {
                            if (r.split(":").length == 2) {
                                Rules.Add(r.split(":")[0], Integer.toString((int) Math.floor(Integer.parseInt(r.split(":")[1]) / 2)));
                            }
                        }
                    }

                    //Checks for a subaffiliation
                    if (birthaffname.length == 2) {
                        for (Stage0Module sub : module.Subaffiliations) {
                            if (birthaffname[1].equals(sub.Name)) {
                                for (LogBookEntry log : sub.Attributes.GetLogs()) {
                                    switch (log.Description()) {
                                        case "Strength": {
                                            Attributes[0] += Math.floor(Integer.parseInt(log.Value()) / 2);
                                            break;
                                        }
                                        case "Body": {
                                            Attributes[1] += Math.floor(Integer.parseInt(log.Value()) / 2);
                                            break;
                                        }
                                        case "Reflexes": {
                                            Attributes[2] += Math.floor(Integer.parseInt(log.Value()) / 2);
                                            break;
                                        }
                                        case "Dexterity": {
                                            Attributes[3] += Math.floor(Integer.parseInt(log.Value()) / 2);
                                            break;
                                        }
                                        case "Intelligence": {
                                            Attributes[4] += Math.floor(Integer.parseInt(log.Value()) / 2);
                                            break;
                                        }
                                        case "Willpower": {
                                            Attributes[5] += Math.floor(Integer.parseInt(log.Value()) / 2);
                                            break;
                                        }
                                        case "Charisma": {
                                            Attributes[6] += Math.floor(Integer.parseInt(log.Value()) / 2);
                                            break;
                                        }
                                        case "Edge": {
                                            Attributes[7] += Math.floor(Integer.parseInt(log.Value()) / 2);
                                            break;
                                        }
                                    }
                                }
                                sub.Traits.GetLogs().forEach((log) -> {
                                    Traits.Add(log.Description(), Integer.toString((int) Math.floor(Integer.parseInt(log.Value()) / 2)));
                                });
                                sub.Skills.GetLogs().forEach((log) -> {
                                    Skills.Add(log.Description(), Integer.toString((int) Math.floor(Integer.parseInt(log.Value()) / 2)));
                                });

                                if (sub.Rules != null && !"".equals(sub.Rules)) {
                                    for (String r : sub.Rules.split(",")) {
                                        if (r.split(":").length == 2) {
                                            Rules.Add(r.split(":")[0], Integer.toString((int) Math.floor(Integer.parseInt(r.split(":")[1]) / 2)));
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        //The next section is to find if ComStar or WOB has been selected
        if (ComStarBox.getSelectedItem() != "None") {
            Cost += 50;
            switch ((String) ComStarBox.getSelectedItem()) {
                case "ComStar": {
                    Attributes[4] += 25;
                    Attributes[5] -= 15;
                    ProgramDataSingleton.ComStar.Skills.GetLogs().forEach((log) -> {
                        Skills.Add(log.Description(), log.Value());
                    });
                    ProgramDataSingleton.ComStar.Traits.GetLogs().forEach((log) -> {
                        Traits.Add(log.Description(), log.Value());
                    });
                    break;
                }
                case "Word of Blake": {
                    Attributes[5] += 50;
                    Attributes[6] -= 50;
                    ProgramDataSingleton.WOB.Skills.GetLogs().forEach((log) -> {
                        Skills.Add(log.Description(), log.Value());
                    });
                    ProgramDataSingleton.WOB.Traits.GetLogs().forEach((log) -> {
                        Traits.Add(log.Description(), log.Value());
                    });
                    break;
                }
            }
        }

        //The next section looks at the skills and traits and checks if they need an optionpanel
        Traits.GetLogs().forEach((log) -> {
            Boolean checkTrait = true;

            for (String description : NewStuff) {
                if (description.equals(log.Description())) {
                    checkTrait = false;
                }
            }

            for (LogBookEntry trait : Parent.TraitMaster.GetLogs()) {
                if (log.Description().equals(trait.Description())) {
                    checkTrait = false;
                }
            }
            if (checkTrait) {
                String postTrait = createSelectionPanel("Trait:" + log.Description() + ":" + log.Value(), false);

                if (postTrait != null) {
                    CaughtStuff.Add("Trait:" + postTrait, log.Value());
                } else {
                    OptionPanelItems.add("Trait:" + log.Description() + ":" + log.Value());
                }

                log.RemoveMe = true;

            }
        });

        Skills.GetLogs().forEach((log) -> {
            Boolean checkSkill = true;

            for (String description : NewStuff) {
                if (description.equals(log.Description())) {
                    checkSkill = false;
                }
            }
            for (LogBookEntry skill : Parent.SkillMaster.GetLogs()) {
                if (log.Description().equals(skill.Description())) {
                    checkSkill = false;
                }
            }
            if (checkSkill) {
                String postSkill = createSelectionPanel("Skill:" + log.Description() + ":" + log.Value(), false);

                if (postSkill != null) {
                    CaughtStuff.Add("Skill:" + postSkill, log.Value());
                } else {
                    OptionPanelItems.add("Skill:" + log.Description() + ":" + log.Value());
                }

                log.RemoveMe = true;
            }
        });

        for (LogBookEntry log : Rules.GetLogs()) {
            if (!log.Value().equals("-9999") && log.Description().split("%")[0].equals("Choice")) {
                OptionPanelItems.add("Rule:" + log.Description() + ":" + log.Value());
            }
        }

        for (LogBookEntry log : CaughtStuff.GetLogs()) {
            switch (log.Description().split(":")[0]) {
                case "Skill": {
                    Skills.Add(log.Description().split(":")[1], log.Value());
                    break;
                }
                case "Trait": {
                    Traits.Add(log.Description().split(":")[1], log.Value());
                    break;
                }
            }
            log.RemoveMe = true;
        }
        CaughtStuff.Cleanup();
        Traits.Cleanup();
        Skills.Cleanup();
    }

    protected void drawText(Boolean resetOptions) {
        InfoPanel.removeAll();
        resetLists();

        if (selectionPanels.isEmpty()) {
            TakeModuleButton.setEnabled(false);
        } else {
            TakeModuleButton.setEnabled(true);
        }

        if (resetOptions) {
            OptionsPanel.removeAll();
            selectionPanels.clear();

            OptionPanelItems.forEach((input) -> {
                createSelectionPanel(input, true);
            });
        }

        //This checks if there's any options avaliable
        for (SelectionPaneObject pane : selectionPanels) {
            if (!pane.XPAllocated()) {
                TakeModuleButton.setEnabled(false);
            }
            if (pane.PropertyString() == null) {
                TakeModuleButton.setEnabled(false);
            } else {
                String[] paneInputs = pane.PropertyString().split("&");
                for (String i : paneInputs) {
                    String[] settings = i.split("\\@");
                    switch (settings[0]) {
                        case "Skill": {
                            Skills.Add(settings[1], settings[2]);
                            break;
                        }
                        case "Trait": {
                            Traits.Add(settings[1], settings[2]);
                            break;
                        }
                        case "Attribute": {
                            switch (settings[1]) {
                                case "Strength": {
                                    Attributes[0] += Integer.parseInt(settings[2]);
                                    break;
                                }
                                case "Body": {
                                    Attributes[1] += Integer.parseInt(settings[2]);
                                    break;
                                }
                                case "Reflexes": {
                                    Attributes[2] += Integer.parseInt(settings[2]);
                                    break;
                                }
                                case "Dexterity": {
                                    Attributes[3] += Integer.parseInt(settings[2]);
                                    break;
                                }
                                case "Intelligence": {
                                    Attributes[4] += Integer.parseInt(settings[2]);
                                    break;
                                }
                                case "Willpower": {
                                    Attributes[5] += Integer.parseInt(settings[2]);
                                    break;
                                }
                                case "Charisma": {
                                    Attributes[6] += Integer.parseInt(settings[2]);
                                    break;
                                }
                                case "Edge": {
                                    Attributes[7] += Integer.parseInt(settings[2]);
                                    break;
                                }
                            }
                            break;
                        }

                        case "Rule": {
                            switch (settings[1].split(":")[0]) {
                                case "Attribute": {
                                    switch (settings[1].split(":")[1]) {
                                        case "Strength": {
                                            Attributes[0] += Integer.parseInt(settings[2]);
                                            break;
                                        }
                                        case "Body": {
                                            Attributes[1] += Integer.parseInt(settings[2]);
                                            break;
                                        }
                                        case "Reflexes": {
                                            Attributes[2] += Integer.parseInt(settings[2]);
                                            break;
                                        }
                                        case "Dexterity": {
                                            Attributes[3] += Integer.parseInt(settings[2]);
                                            break;
                                        }
                                        case "Intelligence": {
                                            Attributes[4] += Integer.parseInt(settings[2]);
                                            break;
                                        }
                                        case "Willpower": {
                                            Attributes[5] += Integer.parseInt(settings[2]);
                                            break;
                                        }
                                        case "Charisma": {
                                            Attributes[6] += Integer.parseInt(settings[2]);
                                            break;
                                        }
                                        case "Edge": {
                                            Attributes[7] += Integer.parseInt(settings[2]);
                                            break;
                                        }
                                    }
                                    break;
                                }
                                case "Skill": {
                                    Skills.Add(settings[1].split(":")[1], settings[2]);
                                    break;
                                }
                                case "Trait": {
                                    Traits.Add(settings[1].split(":")[1], settings[2]);
                                    break;
                                }
                            }
                            break;
                        }

                        default: {
                            return;
                        }
                    }
                }
            }
        }

        for (int x = 0; x < Attributes.length; x++) {
            String text = "Attribute:";
            switch (x) {
                case 0: {
                    text += " Strength";
                    break;
                }
                case 1: {
                    text += " Body";
                    break;
                }
                case 2: {
                    text += " Reflexes";
                    break;
                }
                case 3: {
                    text += " Dexterity";
                    break;
                }
                case 4: {
                    text += " Intelligence";
                    break;
                }
                case 5: {
                    text += " Willpower";
                    break;
                }
                case 6: {
                    text += " Charisma";
                    break;
                }
                default: {
                    text += " Edge";
                    break;
                }
            }

            if (Attributes[x] > 0) {
                text += " +";
            } else {
                text += " ";
            }

            text += Integer.toString(Attributes[x]);
            JLabel label = new JLabel();
            label.setText(text);
            InfoPanel.add(label);
        }

        Traits.GetLogs().stream().map((log) -> {
            String value;
            if (Integer.parseInt(log.Value()) > 0) {
                value = "+" + log.Value();
            } else {
                value = log.Value();
            }
            String text = "Trait: " + log.Description() + " " + value;
            return text;
        }).map((text) -> {
            JLabel label = new JLabel();
            label.setText(text);
            return label;
        }).forEachOrdered((label) -> {
            InfoPanel.add(label);
        });

        Skills.GetLogs().stream().map((log) -> {
            String value;
            if (Integer.parseInt(log.Value()) > 0) {
                value = "+" + log.Value();
            } else {
                value = log.Value();
            }
            String text = "Skill: " + log.Description() + " " + value;
            return text;
        }).map((text) -> {
            JLabel label = new JLabel();
            label.setText(text);
            return label;
        }).forEachOrdered((label) -> {
            InfoPanel.add(label);
        });

        TakeModuleButton.setText(Cost + " XP");
        if (selectionPanels.isEmpty() && main.AffType == AffiliationType.Clan && SubAffComboBox.getSelectedIndex() != 0) {
            TakeModuleButton.setEnabled(true);
        }
        if (Cost > Parent.CurrentXP) {
            TakeModuleButton.setEnabled(false);
        }
    }

    private String PrimaryLanguages() {

        String language;

        if (ComStarBox.getSelectedIndex() != 0) {
            return "English";
        } else {
            language = main.PrimaryLanguage(SubAffComboBox.getSelectedItem().toString());
        }

        if ("Nearest State".equals(language)) {
            language = NeedNearestState();
        }

        return language;

    }

    private String SecondaryLanguages() {

        String language;

        if (ComStarBox.getSelectedIndex() != 0) {
            language = "Nearest State";
        } else {
            language = main.SecondaryLanguage(SubAffComboBox.getSelectedItem().toString());
        }

        if ("Nearest State".equals(language)) {
            language = NeedNearestState();
        }

        return language;

    }

    private String NeedNearestState() {
        if (NearestStateComboBox.getSelectedItem() == null || !NearestStateComboBox.isEnabled() || NearestStateComboBox.getSelectedIndex() == 0) {
            return null;
        } else {
            for (Module mod : ProgramDataSingleton.ModulesMaster) {
                if (mod.Stage == 0) {
                    Stage0Module zeromod = (Stage0Module) mod;
                    switch (zeromod.AffType) {
                        case InnerSphere: {
                            if (zeromod.Name.equals(NearestState)) {
                                return ProgramDataSingleton.RemoveDuplicates(zeromod.PrimaryLanguage("") + "," + zeromod.SecondaryLanguage(""));
                            }
                        }
                        case Periphery: {
                            if (!"Independent".equals(zeromod.Name)) {
                                for (Stage0Module sub : zeromod.Subaffiliations) {
                                    if (sub.Name.equals(NearestState)) {
                                        return ProgramDataSingleton.RemoveDuplicates(zeromod.PrimaryLanguage(sub.Name) + "," + zeromod.SecondaryLanguage(sub.Name));
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return null;
        }
    }

    @Override
    protected String[] breakSkillString(String input) {
        String[] orArray = input.split("\\|");

        String output = "";

        for (String object : orArray) {
            Boolean unknown = true;
            for (LogBookEntry skill : Parent.SkillMaster.GetLogs()) {
                if (object.equals(skill.Description())) {
                    unknown = false;
                    output += ("," + object);
                    break;
                }
            }
            if (unknown) {
                String[] skillArry = object.split("/");
                if (skillArry.length == 1) {
                    String[] temp = super.breakSkillString(object);
                    for (String t : temp) {
                        if (!"".equals(t)) {
                            output += "," + t;
                        }
                    }
                } else {
                    String in;
                    if (skillArry[1].split("!").length > 1) {
                        in = skillArry[1].split("!")[0];
                    } else {
                        in = skillArry[1];
                    }
                    Boolean loop;
                    do {
                        loop = false;
                        switch (in) {
                            case "Any Primary": {
                                String[] languages = PrimaryLanguages().split(",");
                                for (String language : languages) {
                                    if (!"None".equals(language)) {
                                        output += (",Language/" + language);
                                    }
                                }
                                break;
                            }
                            case "Any Secondary": {
                                String out = SecondaryLanguages();
                                if (out == null) {
                                    break;
                                }
                                String[] languages = out.split(",");
                                for (String language : languages) {
                                    if ("Any".equals(language)) {
                                        loop = true;
                                        in = "Any";
                                        break;
                                    }
                                    if (!"None".equals(language)) {
                                        output += (",Language/" + language);
                                    }
                                }
                                break;
                            }                            

                            default: {
                                String[] temp = super.breakSkillString(object);
                                for (String t : temp) {
                                    if (!"".equals(t)) {
                                        output += "," + t;
                                    }
                                }
                                break;
                            }
                        }

                    } while (loop);

                }
            }
        }
        return output.split(",");
    }

    private String createSelectionPanel(String input, Boolean makePanel) {
        String numberOfChoices = "1";
        String[] inputArray = input.split(":");
        if (inputArray[1].split("\\#").length > 1) {
            numberOfChoices = inputArray[1].split("\\#")[1];
        }

        String type = inputArray[0];
        String[] output;
        int[] xp = new int[4];
        if (inputArray[2].split("/").length == 1) {
            xp[0] = Integer.parseInt(inputArray[2]);
            xp[1] = Integer.parseInt(inputArray[2]);
            xp[2] = Integer.parseInt(inputArray[2]);
            xp[3] = Integer.parseInt(inputArray[2]);
        } else {
            for (int x = 0; x < inputArray[2].split("/").length; x++) {
                xp[x] = Integer.parseInt(inputArray[2].split("/")[x]);
            }
        }

        switch (type) {
            case "Trait": {
                output = breakTraitString(inputArray[1].split("#")[0]);
                break;
            }
            case "Skill": {
                output = breakSkillString(inputArray[1].split("#")[0]);
                break;
            }
            case "Rule": {
                output = breakRuleString(inputArray[1].split("#")[0]);
                break;
            }
            case "Attribute": {
                output = breakAttributeString(inputArray[1].split("#")[0]);
                break;
            }
            default: {
                output = new String[]{"Something's fucked."};
                break;
            }
        }

        if (output.length == 1) {
            System.out.println("Not a valid input for a " + type + " selection panel: " + output[0]);
            return null;
        }

        if (output.length > 2) {
            if (makePanel) {
                switch (numberOfChoices) {
                    case "1": {
                        SelectionPane panel = new SelectionPane(output, type, xp, this, ParentFrame);
                        selectionPanels.add(panel);
                        OptionsPanel.add(panel);
                        break;
                    }
                    case "2": {
                        SelectionPane2 panel = new SelectionPane2(output, type, xp, this, ParentFrame, false);
                        selectionPanels.add(panel);
                        OptionsPanel.add(panel);
                        break;
                    }
                    case "3": {
                        SelectionPane3 panel = new SelectionPane3(output, type, xp, this, ParentFrame, false);
                        selectionPanels.add(panel);
                        OptionsPanel.add(panel);
                        break;
                    }
                    case "4": {
                        SelectionPane4 panel = new SelectionPane4(output, type, xp, this, ParentFrame, false);
                        selectionPanels.add(panel);
                        OptionsPanel.add(panel);
                        break;
                    }
                }

            }
            return null;
        } else {
            return output[1];
        }

    }

    @Override
    protected void selectModule() {

        String attributes = "Strength:" + Attributes[0] + ",Body:" + Attributes[1] + ",Reflexes:" + Attributes[2] + ",Dexterity:" + Attributes[3] + ",Intelligence:" + Attributes[4] + ",Willpower:" + Attributes[5] + ",Charisma:" + Attributes[6] + ",Edge:" + Attributes[7];
        String prerequisites = "Strength:" + Prerequisites[0] + ",Body:" + Prerequisites[1] + ",Reflexes:" + Prerequisites[2] + ",Dexterity:" + Prerequisites[3] + ",Intelligence:" + Prerequisites[4] + ",Willpower:" + Prerequisites[5] + ",Charisma:" + Prerequisites[6] + ",Edge:" + Prerequisites[7];
        for (LogBookEntry log : ExtraPrerequisites.GetLogs()) {
            prerequisites += "," + log.Description() + ":" + log.Value();
        }
        String skills = "";
        for (LogBookEntry log : Skills.GetLogs()) {
            if (!"".equals(skills)) {
                skills += ",";
            }

            skills += log.Description() + ":" + log.Value();
        }
        String traits = "";
        for (LogBookEntry log : Traits.GetLogs()) {
            if (!"".equals(traits)) {
                traits += ",";
            }

            traits += log.Description() + ":" + log.Value();
        }
        String rules = "";
        for (LogBookEntry log : Rules.GetLogs()) {
            if (!"".equals(rules)) {
                rules += ",";
            }

            rules += log.Description() + ":" + log.Value();
        }
        String restrictions = "";
        for (LogBookEntry log : Restrictions.GetLogs()) {
            if (!"".equals(restrictions)) {
                restrictions += ",";
            }

            restrictions += log.Description() + ":" + log.Value();
        }
        String Name;
        if ((main.AffType == AffiliationType.Clan || main.AffType == AffiliationType.Periphery) && SubAffComboBox.getSelectedIndex() != 0) {
            Name = SubAffComboBox.getSelectedItem().toString();
        } else {
            Name = main.Name;            
        }

        if (ComStarBox.getSelectedIndex() != 0) {
            switch (ComStarBox.getSelectedIndex()) {
                case 1: {
                    Name = "ComStar";
                    break;
                }
                case 2: {
                    Name = "Word of Blake";
                    break;
                }
            }
        }

        String misc = "";
        if (SubAffComboBox.getSelectedIndex() != 0 || ComStarBox.getSelectedIndex() != 0) {
            switch (main.AffType) {
                case Clan: {
                    if ("ComStar".equals(Name) || "Word of Blake".equals(Name)) {
                        misc += SubAffComboBox.getSelectedItem().toString() + "/" + ClanCasteComboBox.getSelectedItem().toString();
                    } else {
                        misc += ClanCasteComboBox.getSelectedItem().toString();
                    }
                    break;
                }
                case InnerSphere: {
                    if ("ComStar".equals(Name) || "Word of Blake".equals(Name)) {
                        if (SubAffComboBox.getSelectedIndex() != 0)
                            misc += main.Name + "/" + SubAffComboBox.getSelectedItem().toString();
                        else misc += main.Name;
                    }
                    break;
                }
                case Periphery: {
                    if ("ComStar".equals(Name) || "Word of Blake".equals(Name)) {
                        misc += SubAffComboBox.getSelectedItem().toString();
                    }
                }
            }
        }
        if (BirthAffComboBox.getSelectedIndex() != 0) {
            if (!"".equals(misc)) {
                misc += ", ";
            }

            misc += BirthAffComboBox.getSelectedItem().toString() + " (Birth Affiliation)";
        }

        selected = new Stage0Module(0, Cost, 0, Name, attributes, traits, skills, prerequisites, restrictions, rules, main.HouseName, PrimaryLanguages(), SecondaryLanguages(), main.AffType.toString());
        selected.MiscText = misc;
        Parent.Affiliation = Name;
        Parent.Subaffiliation = SubAffComboBox.getSelectedItem().toString();
        Parent.AffiliationType = main.AffType;
        if (BirthAffComboBox.getSelectedIndex() == 0) {
            Parent.BirthAffiliationType = main.AffType;
        } else {
            String aff = BirthAffComboBox.getSelectedItem().toString().split("/")[0];
            for (Module mod : ProgramDataSingleton.ModulesMaster) {
                if (mod.Name.equals(aff)) {
                    Stage0Module zeromod = (Stage0Module) mod;
                    Parent.BirthAffiliationType = zeromod.AffType;
                    break;
                }
            }
        }
        Parent.PrimaryLanguages = PrimaryLanguages();
        Parent.SecondaryLanguages = SecondaryLanguages();
        if (ClanCasteComboBox.isEnabled()) {
            Parent.StartingClanCaste = ClanCasteComboBox.getSelectedItem().toString();
        } else {
            Parent.StartingClanCaste = "None";
        }
        Parent.AddModule(selected);
    }

    private ComboBoxModel AffiliationComboBox() {
        String[] affiliations = new String[AffiliationString.size()];

        for (int x = 0; x < AffiliationString.size(); x++) {
            affiliations[x] = AffiliationString.get(x);
        }

        return new DefaultComboBoxModel(affiliations);
    }

    private ComboBoxModel SubaffiliationComboBox() {
        String[] subaffiliations = new String[main.Subaffiliations.size() + 1];
        subaffiliations[0] = "None";

        for (int x = 0; x < main.Subaffiliations.size(); x++) {
            subaffiliations[x + 1] = main.Subaffiliations.get(x).Name;
        }

        return new DefaultComboBoxModel(subaffiliations);
    }

    private ComboBoxModel NearestStateComboBox() {
        List<String> affiliations = new LinkedList<>();

        for (Module mastermod : ProgramDataSingleton.ModulesMaster) {
            if (mastermod.Stage == 0) {
                Stage0Module mod = (Stage0Module) mastermod;
                if (mod.AffType != AffiliationType.Clan) {
                    switch (mod.AffType) {
                        case InnerSphere: {
                            affiliations.add(mod.Name);
                            break;
                        }
                        case Periphery: {
                            mod.Subaffiliations.forEach((sub) -> {
                                affiliations.add(sub.Name);
                            });
                            break;
                        }
                    }
                }
            }
        }

        String[] aff = new String[affiliations.size() + 1];
        for (int x = 0; x < affiliations.size(); x++) {
            aff[x + 1] = affiliations.get(x);
        }
        return new DefaultComboBoxModel(aff);
    }

    private ComboBoxModel ClanCasteComboBox() {
        String Clan = SubAffComboBox.getSelectedItem().toString();
        List<String> o = new ArrayList<>();
        o.add("Mechwarrior Caste");
        o.add("Elemental Caste");
        if (Clan.equals("Ghost Bear") || Clan.equals("Hell's Horses")) {
            o.add("Elemental-Advanced Caste");
        }
        o.add("Aerospace Caste");
        if (Clan.equals("Snow Raven")) {
            o.add("Aerospace-Naval Caste");
        }
        if (StartingYear > 3062
                && (main.Name.equals("Homeworld Clan") || (main.Name.equals("Invading Clan") && ((StartingYear < 3076 && !Clan.equals("Jade Falcon")) || (Clan.equals("Jade Falcon") && StartingYear < 3068))))
                && !Clan.equals("Diamond Shark") && !Clan.equals("Ghost Bear") && !Clan.equals("Steel Viper") && !Clan.equals("Wolf")) {
            o.add("Protomech Caste");
        }
        o.add("Warrior Caste (Other)");
        o.add("Scientist Caste");
        o.add("Technician Caste");
        o.add("Merchant Caste");
        o.add("Laborer Caste");
        o.add("Dark Caste (Pirate)");

        String[] options = new String[o.size()];
        options = o.toArray(options);
        return new DefaultComboBoxModel(options);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        scrollPane1 = new java.awt.ScrollPane();
        InfoPanel = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        scrollPane2 = new java.awt.ScrollPane();
        OptionsPanel = new javax.swing.JPanel();
        BirthAffComboBox = new javax.swing.JComboBox<>();
        ComStarBox = new javax.swing.JComboBox<>();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        SubAffComboBox = new javax.swing.JComboBox<>();
        jLabel3 = new javax.swing.JLabel();
        NearestStateComboBox = new javax.swing.JComboBox<>();
        jLabel4 = new javax.swing.JLabel();
        SubAffLabel = new javax.swing.JLabel();
        NameLabel = new javax.swing.JLabel();
        TakeModuleButton = new javax.swing.JButton();
        ClanCasteComboBox = new javax.swing.JComboBox<>();
        jLabel5 = new javax.swing.JLabel();

        setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        setMaximumSize(new java.awt.Dimension(1000, 400));
        setPreferredSize(new java.awt.Dimension(1000, 400));

        scrollPane1.setMaximumSize(new java.awt.Dimension(600, 400));

        InfoPanel.addAncestorListener(new javax.swing.event.AncestorListener() {
            public void ancestorMoved(javax.swing.event.AncestorEvent evt) {
                InfoPanelAncestorMoved(evt);
            }
            public void ancestorAdded(javax.swing.event.AncestorEvent evt) {
            }
            public void ancestorRemoved(javax.swing.event.AncestorEvent evt) {
            }
        });
        InfoPanel.setLayout(new java.awt.GridLayout(0, 1));
        scrollPane1.add(InfoPanel);

        jPanel1.setMaximumSize(new java.awt.Dimension(780, 400));
        jPanel1.setPreferredSize(new java.awt.Dimension(780, 400));

        scrollPane2.setMinimumSize(new java.awt.Dimension(0, 158));
        scrollPane2.setPreferredSize(new java.awt.Dimension(600, 300));

        OptionsPanel.addAncestorListener(new javax.swing.event.AncestorListener() {
            public void ancestorMoved(javax.swing.event.AncestorEvent evt) {
                OptionsPanelAncestorMoved(evt);
            }
            public void ancestorAdded(javax.swing.event.AncestorEvent evt) {
            }
            public void ancestorRemoved(javax.swing.event.AncestorEvent evt) {
            }
        });
        OptionsPanel.setLayout(new java.awt.GridLayout(0, 1));
        scrollPane2.add(OptionsPanel);

        BirthAffComboBox.setModel(AffiliationComboBox());
        BirthAffComboBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BirthAffComboBoxActionPerformed(evt);
            }
        });

        ComStarBox.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "None", "ComStar", "Word of Blake" }));
        ComStarBox.setPreferredSize(new java.awt.Dimension(600, 26));
        ComStarBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ComStarBoxActionPerformed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel1.setText("Birth Affiliation (optional, p.53 )");

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel2.setText("ComStar/Word of Blake");

        SubAffComboBox.setModel(SubaffiliationComboBox());
        SubAffComboBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                SubAffComboBoxActionPerformed(evt);
            }
        });

        jLabel3.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel3.setText("Subaffiliation");

        NearestStateComboBox.setModel(NearestStateComboBox());
        NearestStateComboBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                NearestStateComboBoxActionPerformed(evt);
            }
        });

        jLabel4.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel4.setText("Nearest State");

        SubAffLabel.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        SubAffLabel.setText("Subaffiliation");
        SubAffLabel.setMaximumSize(new java.awt.Dimension(212, 15));

        NameLabel.setFont(new java.awt.Font("Space Bd BT", 1, 24)); // NOI18N
        NameLabel.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        NameLabel.setText("jLabel1");

        TakeModuleButton.setText("Choose");
        TakeModuleButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TakeModuleButtonActionPerformed(evt);
            }
        });

        ClanCasteComboBox.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        ClanCasteComboBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ClanCasteComboBoxActionPerformed(evt);
            }
        });

        jLabel5.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel5.setText("Clan Caste");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(NameLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(SubAffLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(SubAffComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, 197, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 106, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(ClanCasteComboBox, 0, 173, Short.MAX_VALUE)
                            .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addComponent(scrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 750, Short.MAX_VALUE)
                    .addComponent(BirthAffComboBox, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(ComStarBox, 0, 1, Short.MAX_VALUE)
                            .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, 213, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(NearestStateComboBox, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(TakeModuleButton))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel4)
                                .addGap(0, 0, Short.MAX_VALUE)))))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(NameLabel)
                        .addGap(0, 0, 0)
                        .addComponent(SubAffLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel3)
                            .addComponent(jLabel5))
                        .addGap(26, 26, 26))
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(ClanCasteComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(SubAffComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(scrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 210, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel1)
                .addGap(0, 0, 0)
                .addComponent(BirthAffComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(jLabel4))
                .addGap(0, 0, 0)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(ComStarBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(NearestStateComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(TakeModuleButton))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(scrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 320, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
            .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.LEADING, 398, 398, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addComponent(scrollPane1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents

    private void BirthAffComboBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BirthAffComboBoxActionPerformed
        displayStats();
    }//GEN-LAST:event_BirthAffComboBoxActionPerformed

    private void ComStarBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ComStarBoxActionPerformed
        if (null == ComStarBox.getSelectedItem().toString()) {
            NameLabel.setText(main.Name);
        } else {
            switch (ComStarBox.getSelectedItem().toString()) {
                case "ComStar":{
                    NameLabel.setText("ComStar");
                    break;
                }
                case "Word of Blake":{
                    NameLabel.setText("Word of Blake");
                    break;
                }
                default:{
                    NameLabel.setText(main.Name);
                    break;
                }
            }
        }

        displayStats();
    }//GEN-LAST:event_ComStarBoxActionPerformed

    private void SubAffComboBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_SubAffComboBoxActionPerformed
        if (ClanCasteComboBox.isVisible()) {
            ClanCasteComboBox.removeAllItems();
            if (SubAffComboBox.getSelectedIndex() == 0) {
                ClanCasteComboBox.setEnabled(false);
            } else {
                ClanCasteComboBox.setModel(ClanCasteComboBox());
                ClanCasteComboBox.setEnabled(true);
            }
        }
        displayStats();
    }//GEN-LAST:event_SubAffComboBoxActionPerformed

    private void OptionsPanelAncestorMoved(javax.swing.event.AncestorEvent evt) {//GEN-FIRST:event_OptionsPanelAncestorMoved
        revalidate();
    }//GEN-LAST:event_OptionsPanelAncestorMoved

    private void InfoPanelAncestorMoved(javax.swing.event.AncestorEvent evt) {//GEN-FIRST:event_InfoPanelAncestorMoved
        revalidate();
    }//GEN-LAST:event_InfoPanelAncestorMoved

    private void NearestStateComboBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_NearestStateComboBoxActionPerformed
        if (NearestStateComboBox.isVisible()) {
            if (NearestStateComboBox.getSelectedItem() == null) {
                OptionsPanel.removeAll();
                InfoPanel.removeAll();
                jLabel4.setVisible(true);
                NearestStateComboBox.setVisible(true);
                jLabel1.setEnabled(false);
                jLabel3.setEnabled(false);
                scrollPane1.setEnabled(false);
                scrollPane2.setEnabled(false);
                SubAffComboBox.setEnabled(false);
                BirthAffComboBox.setEnabled(false);
                InfoPanel.setEnabled(false);
                NearestState = "";
            } else {
                jLabel1.setEnabled(true);
                jLabel3.setEnabled(true);
                scrollPane1.setEnabled(true);
                scrollPane2.setEnabled(true);
                SubAffComboBox.setEnabled(true);
                BirthAffComboBox.setEnabled(true);
                InfoPanel.setEnabled(true);
                NearestState = NearestStateComboBox.getSelectedItem().toString();
            }
        } else {
            NameLabel.setText(main.Name);
            jLabel4.setVisible(false);
            NearestStateComboBox.setVisible(false);
            NearestStateComboBox.setSelectedIndex(0);
            jLabel1.setEnabled(true);
            jLabel3.setEnabled(true);
            scrollPane1.setEnabled(true);
            scrollPane2.setEnabled(true);
            SubAffComboBox.setEnabled(true);
            BirthAffComboBox.setEnabled(true);
            InfoPanel.setEnabled(true);
            NearestState = "";
        }
        displayStats();
    }//GEN-LAST:event_NearestStateComboBoxActionPerformed

    private void ClanCasteComboBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ClanCasteComboBoxActionPerformed
        if (ClanCasteComboBox.getItemAt(1) != null) {
            displayStats();
        }
    }//GEN-LAST:event_ClanCasteComboBoxActionPerformed

    private void TakeModuleButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_TakeModuleButtonActionPerformed
        selectModule();
    }//GEN-LAST:event_TakeModuleButtonActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox<String> BirthAffComboBox;
    private javax.swing.JComboBox<String> ClanCasteComboBox;
    private javax.swing.JComboBox<String> ComStarBox;
    private javax.swing.JPanel InfoPanel;
    private javax.swing.JLabel NameLabel;
    private javax.swing.JComboBox<String> NearestStateComboBox;
    private javax.swing.JPanel OptionsPanel;
    private javax.swing.JComboBox<String> SubAffComboBox;
    private javax.swing.JLabel SubAffLabel;
    private javax.swing.JButton TakeModuleButton;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JPanel jPanel1;
    private java.awt.ScrollPane scrollPane1;
    private java.awt.ScrollPane scrollPane2;
    // End of variables declaration//GEN-END:variables
}
