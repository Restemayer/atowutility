/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUIComponents;

import atowutility.ProgramDataSingleton;
import atowutility.classes.Skill;
import java.awt.Color;
import java.util.ListIterator;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 *
 * @author Chris
 */
public class SkillPanel extends javax.swing.JPanel {

    /**
     * Creates new form SkillPanel
     */
    private final String ShortName;
    private AddNewSkillPopup Popup;
    
    public SkillPanel(Skill skill) {
        initComponents();
        ShortName = skill.Name;
        if (skill.Score() == 10)
            AddXPButton.setEnabled(false);
        UpdateText(skill.Name(), skill.Score(), skill.Experience(), skill.MinimumXP(), skill.NaturalAptitudeXP(), skill.NaturalAptitudeTP(), skill.TargetNumber(), skill.ComplexityRating(), skill.NaturalAptitude());               
    }
    
    public SkillPanel(Skill skill, AddNewSkillPopup popup) {
        initComponents();
        Popup = popup;
        ShortName = skill.Name;        
        UpdateText(skill.Name(), skill.Score(), skill.Experience(), skill.MinimumXP(), skill.NaturalAptitudeXP(), skill.NaturalAptitudeTP(), skill.TargetNumber(), skill.ComplexityRating(), skill.NaturalAptitude());               
    }    
    
    
    private void AddXP(){        
        int addedXP;
        Boolean newSkill = true;
        try {
            addedXP = Integer.parseInt(AddXPField.getText());
        }
        catch (NumberFormatException e){
            AddXPField.setText("");
            JFrame frame = new JFrame();
            JOptionPane.showMessageDialog(frame,
            "That is not a number!",
            "ERROR",
            JOptionPane.ERROR_MESSAGE);
            return;
        }
        
        AddXPField.setText("");
        
        if (addedXP <= 0){
            JFrame frame = new JFrame();
            JOptionPane.showMessageDialog(frame,
            "You must add XP!",
            "ERROR",
            JOptionPane.ERROR_MESSAGE);
            return;
        }
        if (addedXP > ProgramDataSingleton.CurrentCharacter.Experience()){
            JFrame frame = new JFrame();
            JOptionPane.showMessageDialog(frame,
            "You don't have that much XP!",
            "ERROR",
            JOptionPane.ERROR_MESSAGE);
            return;
        }
        else {
            for (ListIterator<Skill> iter = ProgramDataSingleton.CurrentCharacter.Skills.listIterator(); iter.hasNext();){
                Skill element = iter.next();
                if (element.Name.equals(ShortName)){
                    if (element.MaximumXP() - element.Experience() < addedXP){
                        int refund = addedXP + element.Experience() - element.MaximumXP();
                        String refundmessage = "You are at maximum XP. " + refund + " XP returned to your pool.";
                        JFrame frame = new JFrame();
                        JOptionPane.showMessageDialog(frame,
                        refundmessage,
                        "Too much XP",
                        JOptionPane.WARNING_MESSAGE);
                        addedXP = element.MaximumXP() - element.Experience(); 
                        AddXPButton.setEnabled(false);
                    }
                    newSkill = false;
                    int oldxp = element.Experience();
                    element.ChangeExperience("Added XP", addedXP);
                    if (oldxp < element.MinimumXP() && oldxp + addedXP >= element.MinimumXP()){
                        ProgramDataSingleton.RefreshSkillScreen();
                    }
                    else UpdateText(element.Name(), element.Score(), element.Experience(), element.MinimumXP(), element.NaturalAptitudeXP(), element.NaturalAptitudeTP(), element.TargetNumber(), element.ComplexityRating(), element.NaturalAptitude());
                    break;
                }              
            }
        }
        
        //Adds skill to Character's skill list if it isn't already there.
        if (newSkill){             
            Skill skill = ProgramDataSingleton.FindMasterSkill(ShortName);
            skill.ChangeExperience("New Skill Acquired", addedXP);
            ProgramDataSingleton.AddNewSkill(skill);
            Popup.Adjust(this);
        }
        ProgramDataSingleton.CurrentCharacter.EnterXPLog("Added XP to skill: " + ShortName, -addedXP);
    }
    private void AddNaturalAptitude(){
        int addedXP;
        try {
            addedXP = Integer.parseInt(AddXPField.getText());
        }
        catch (NumberFormatException e){
            AddXPField.setText("");
            JFrame frame = new JFrame();
            JOptionPane.showMessageDialog(frame,
            "That is not a number!",
            "ERROR",
            JOptionPane.ERROR_MESSAGE);
            return;
        }
        
        AddXPField.setText("");
        
        if (addedXP <= 0){
            JFrame frame = new JFrame();
            JOptionPane.showMessageDialog(frame,
            "You must add XP!",
            "ERROR",
            JOptionPane.ERROR_MESSAGE);
        }
        else if (addedXP > ProgramDataSingleton.CurrentCharacter.Experience()){
            JFrame frame = new JFrame();
            JOptionPane.showMessageDialog(frame,
            "You don't have that much XP!",
            "ERROR",
            JOptionPane.ERROR_MESSAGE);
        }
        else {
            for (ListIterator<Skill> iter = ProgramDataSingleton.CurrentCharacter.Skills.listIterator(); iter.hasNext();){
                Skill element = iter.next();
                if (element.Name.equals(ShortName)){
                    if (element.NaturalAptitudeTP() - element.NaturalAptitudeXP() < addedXP){
                        int refund = addedXP + element.NaturalAptitudeXP() - element.NaturalAptitudeTP();
                        String refundmessage = "You are at maximum XP. " + refund + " XP returned to your pool.";
                        JFrame frame = new JFrame();
                        JOptionPane.showMessageDialog(frame,
                        refundmessage,
                        "Too much XP",
                        JOptionPane.WARNING_MESSAGE);
                        addedXP = element.NaturalAptitudeTP() - element.NaturalAptitudeXP();                          
                    }
                    element.ChangeAptitudeXP("Added XP", addedXP);                   
                    UpdateText(element.Name(), element.Score(), element.Experience(), element.MinimumXP(), element.NaturalAptitudeXP(), element.NaturalAptitudeTP(), element.TargetNumber(), element.ComplexityRating(), element.NaturalAptitude());
                    break;
                }              
            }
            ProgramDataSingleton.CurrentCharacter.EnterXPLog("Added XP to Natural Aptitude: " + ShortName, -addedXP);
        }
    }
    private void UpdateText(String name, int score, int xp, int minimumxp, int aptitudexp, int aptitudetp, int targetnumber, String complexity, Boolean aptitude){        
        AddXPField.setText("");
        SkillNameLabel.setText(name); 
        SkillXPLabel.setText(xp + " XP");
        SkillTNLabel.setText(targetnumber+"/"+complexity);
        SkillRankLabel.setForeground(Color.BLACK);
        if (score > 0)
            SkillRankLabel.setText("+" + score);
        else if (xp < minimumxp && Popup == null){
            SkillRankLabel.setText("Untrained");
            SkillRankLabel.setForeground(Color.red);
            NaturalAptitudeXPLabel.setForeground(Color.GRAY);
            AptitudeButton.setEnabled(false);
            SkillNameLabel.setForeground(Color.GRAY);
            SkillTNLabel.setForeground(Color.GRAY);
        }          
        else SkillRankLabel.setText("0");
        if (aptitude) {
           NaturalAptitudeXPLabel.setText("Natural Aptitude +" + aptitudetp/100 + " TP");
           NaturalAptitudeXPLabel.setForeground(new Color(0,100,0));
           AptitudeButton.setEnabled(false);
        }
        else if (Popup != null){
            AptitudeButton.setEnabled(false);
            SkillRankLabel.setText("");
            NaturalAptitudeXPLabel.setText("");
        }
        else if (!ProgramDataSingleton.CurrentCharacter.AvaliableAptitude() && xp >= minimumxp){
            NaturalAptitudeXPLabel.setText("At maximum Natural Aptitude Traits");
            AptitudeButton.setEnabled(false);
        }
        
        else NaturalAptitudeXPLabel.setText("Natural Aptitude: " + aptitudexp + "/" + aptitudetp + " XP");
        if (!AddXPButton.isEnabled() && !AptitudeButton.isEnabled())
            AddXPField.setEnabled(false);
        validate();
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        SkillNameLabel = new javax.swing.JLabel();
        SkillTNLabel = new javax.swing.JLabel();
        SkillRankLabel = new javax.swing.JLabel();
        SkillXPLabel = new javax.swing.JLabel();
        NaturalAptitudeXPLabel = new javax.swing.JLabel();
        AddXPField = new java.awt.TextField();
        AddXPButton = new javax.swing.JButton();
        AptitudeButton = new javax.swing.JButton();

        setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        setPreferredSize(new java.awt.Dimension(400, 100));

        SkillNameLabel.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        SkillNameLabel.setText("Skill/Subskill (Basic Tier)");

        SkillTNLabel.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        SkillTNLabel.setText("7/SB");

        SkillRankLabel.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        SkillRankLabel.setText("+10");

        SkillXPLabel.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        SkillXPLabel.setText("400 XP");

        NaturalAptitudeXPLabel.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        NaturalAptitudeXPLabel.setText("Natural Aptitude: 250/500 XP");

        AddXPField.setFont(new java.awt.Font("Dialog", 0, 18)); // NOI18N
        AddXPField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AddXPFieldActionPerformed(evt);
            }
        });

        AddXPButton.setText("Add to XP");
        AddXPButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AddXPButtonActionPerformed(evt);
            }
        });

        AptitudeButton.setText("Add to Natural Aptitude");
        AptitudeButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AptitudeButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(NaturalAptitudeXPLabel)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(SkillNameLabel)
                        .addGap(10, 10, 10)
                        .addComponent(SkillTNLabel)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(SkillRankLabel)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(SkillXPLabel)))
                .addContainerGap())
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(AddXPField, javax.swing.GroupLayout.PREFERRED_SIZE, 56, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(AddXPButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(AptitudeButton)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(SkillNameLabel)
                    .addComponent(SkillTNLabel)
                    .addComponent(SkillRankLabel)
                    .addComponent(SkillXPLabel))
                .addGap(1, 1, 1)
                .addComponent(NaturalAptitudeXPLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(AddXPButton)
                        .addComponent(AptitudeButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(AddXPField, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        AddXPButton.getAccessibleContext().setAccessibleName("");
    }// </editor-fold>//GEN-END:initComponents

    private void AddXPFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_AddXPFieldActionPerformed
       
    }//GEN-LAST:event_AddXPFieldActionPerformed

    private void AddXPButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_AddXPButtonActionPerformed
         AddXP();
    }//GEN-LAST:event_AddXPButtonActionPerformed

    private void AptitudeButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_AptitudeButtonActionPerformed
        AddNaturalAptitude();
    }//GEN-LAST:event_AptitudeButtonActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton AddXPButton;
    private java.awt.TextField AddXPField;
    private javax.swing.JButton AptitudeButton;
    private javax.swing.JLabel NaturalAptitudeXPLabel;
    private javax.swing.JLabel SkillNameLabel;
    private javax.swing.JLabel SkillRankLabel;
    private javax.swing.JLabel SkillTNLabel;
    private javax.swing.JLabel SkillXPLabel;
    // End of variables declaration//GEN-END:variables
}
