/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUIComponents;

import atowutility.classes.LogBook;
import atowutility.classes.LogBookEntry;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Chris
 */
public class FlexAllocationDialog extends javax.swing.JDialog {

    int XPPool;
    int[] MaxValues;
    int[] Limits = {0,0,0};
    int[] XPByType = {0,0,0};    
    String Type;
    List<FlexItemPaneling> FlexItems;
    FlexSelectionPane Parent;
    LogBook Things;

    /**
     * Creates new form FlexAllocationDialog
     */
    public FlexAllocationDialog(java.awt.Frame parent, boolean modal) {
        super(parent, modal);

        initComponents();
    }

    public FlexAllocationDialog(java.awt.Frame parent, boolean modal, FlexSelectionPane p, LogBook things, String type, int xp, int[] maxvalues, int[] limiter) {
        super(parent, modal);
        Parent = p;
        XPPool = xp;
        Type = type;
        //Array of maximum xp that can be assigned; {Attributes,Traits,Skills}
        MaxValues = maxvalues;
        Limits = limiter;
        for (int x = 0; x < 3; x++){
            if (Limits[x] > 0 && Limits[x] < MaxValues[x])
                MaxValues[x] = Limits[x];
        }
            
        FlexItems = new ArrayList<>();        
        Things = things;
        initComponents();
        RebuildComponents();
    }

    private int XPLeft() {
        int xp = XPPool;
        for (FlexItemPaneling item : FlexItems) {
            xp -= item.XPAllocated;
        }
        return xp;
    }

    public void RebuildComponents() {
        DisplayPanel.removeAll();
        FlexItems.clear();
        for (int i = 0; i < 3; i++)
             XPByType[i] = 0;
        Things.GetLogs().stream().map((log) -> {
            if (Type.equals("Attribute") || log.Description().split(":")[0].equals("Attribute"))
                XPByType[0] += Integer.parseInt(log.Value());
            return log;
        }).map((log) -> {
            if (Type.equals("Trait") || log.Description().split(":")[0].equals("Trait"))
                XPByType[1] += Integer.parseInt(log.Value());
            return log;
        }).filter((log) -> (Type.equals("Skill") || log.Description().split(":")[0].equals("Skill"))).forEachOrdered((log) -> {
            XPByType[2] += Integer.parseInt(log.Value());
        });
        int type = 3;
        switch (Type) {
            case "Attribute": {
                type = 0;
                break;
            }
            case "Trait": {
                type = 1;
                break;
            }
            case "Skill": {
                type = 2;
                break;
            }
        }
        FlexFieldItemPanel skillfieldpanel = null;
        for (LogBookEntry log : Things.GetLogs()) {
            if (Type.equals("Rule")) {
                switch (log.Description().split(":")[0]) {
                    case "Attribute": {
                        type = 0;
                        break;
                    }
                    case "Trait": {
                        type = 1;
                        break;
                    }
                    case "Skill": {
                        type = 2;
                        break;
                    }
                }
            } else {
                switch (Type) {
                    case "Attribute": {
                        type = 0;
                        break;
                    }
                    case "Trait": {
                        type = 1;
                        break;
                    }
                    case "Skill": {
                        type = 2;
                        break;
                    }
                }
            }

            int maxxp;
            if (XPLeft() < MaxValues[type]) {
                maxxp = XPLeft();
            } else {
                maxxp = MaxValues[type];
            }            
            if (Limits[type] > 0 && maxxp > Limits[type] - XPByType[type])
                maxxp = Limits[type] - XPByType[type];
            

            if (log.Description().split(":")[0].equals("AAAAA")) {
                if (skillfieldpanel == null) {
                    skillfieldpanel = new FlexFieldItemPanel(this, maxxp, Integer.parseInt(log.Description().split(":")[1].split("#")[1].split("/")[1]));
                    DisplayPanel.add(skillfieldpanel);
                    FlexItems.add(skillfieldpanel);
                }
                skillfieldpanel.AddItem(log.Description().split(":")[1].split("#")[0], Integer.parseInt(log.Description().split(":")[1].split("#")[1].split("/")[0]), Integer.parseInt(log.Value()));
            } else if (log.Description().split("/")[log.Description().split("/").length - 1].equals("ZZZZZ")) {
                String name = "";
                for (int x = 0; x < log.Description().split("/").length - 1; x++) {
                    if (!"".equals(name)) {
                        name += "/";
                    }
                    name += log.Description().split("/")[x];
                }
                FlexNewItemPanel panel = new FlexNewItemPanel(this, name, 0, maxxp, MaxValues[type]);
                DisplayPanel.add(panel);
            } else {
                FlexItemPanel panel = new FlexItemPanel(this, log.Description(), Integer.parseInt(log.Value()), maxxp, MaxValues[type]);
                FlexItems.add(panel);
                DisplayPanel.add(panel);
            }            
        }
        XPLabel.setText((XPPool - XPLeft()) + "/" + XPPool + " XP");
    }

    public void UpdateComponents(String name, int value) {
        Boolean newItem = Things.SetValueReturnNew(name, Integer.toString(value));
        if (newItem) {
            RebuildComponents();
        } else {
            UpdateComponents();
        }
    }

    public void UpdateComponents() {
        int type = 3;
         for (int i = 0; i < 3; i++)
             XPByType[i] = 0;
        Things.GetLogs().stream().map((log) -> {
            if (Type.equals("Attribute") || log.Description().split(":")[0].equals("Attribute"))
                XPByType[0] += Integer.parseInt(log.Value());
            return log;
        }).map((log) -> {
            if (Type.equals("Trait") || log.Description().split(":")[0].equals("Trait"))
                XPByType[1] += Integer.parseInt(log.Value());
            return log;
        }).filter((log) -> (Type.equals("Skill") || log.Description().split(":")[0].equals("Skill"))).forEachOrdered((log) -> {
            XPByType[2] += Integer.parseInt(log.Value());
        });
        switch (Type) {
            case "Attribute": {
                type = 0;
                break;
            }
            case "Trait": {
                type = 1;
                break;
            }
            case "Skill": {
                type = 2;
                break;
            }
        }
        for (FlexItemPaneling item : FlexItems) {
            if (Type.equals("Rule")) {
                switch (item.Type) {
                    case "Attribute": {
                        type = 0;
                        break;
                    }
                    case "Trait": {
                        type = 1;
                        break;
                    }
                    case "Skill": {
                        type = 2;
                        break;
                    }
                }
            }

            int maxxp;
            if (XPLeft() < MaxValues[type]) {
                maxxp = XPLeft();
            } else {
                maxxp = MaxValues[type];
            }            
            if (Limits[type] > 0 && maxxp > Limits[type] - XPByType[type])
                maxxp = Limits[type] - XPByType[type];

            item.SetSpinner(maxxp);
        }
        XPLabel.setText((XPPool - XPLeft()) + "/" + XPPool + " XP");
    }

    public void UpdateFieldSkills(String name, int xp) {
        for (LogBookEntry log : Things.GetLogs()) {
            if (log.Description().split(":")[0].equals("AAAAA")) {
                if (log.Description().split(":")[1].split("#")[0].equals(name)) {
                    log.ChangeValue(xp);
                } else {
                    log.ChangeValue(0);
                }
            }
        }
    }
    

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        XPLabel = new javax.swing.JLabel();
        button1 = new java.awt.Button();
        scrollPane2 = new java.awt.ScrollPane();
        DisplayPanel = new javax.swing.JPanel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        XPLabel.setFont(new java.awt.Font("Space Bd BT", 1, 24)); // NOI18N
        XPLabel.setText("jLabel1");

        button1.setFont(new java.awt.Font("Space Bd BT", 0, 18)); // NOI18N
        button1.setLabel("Done");
        button1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                button1ActionPerformed(evt);
            }
        });

        DisplayPanel.setLayout(new java.awt.GridLayout(0, 4));
        scrollPane2.add(DisplayPanel);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(XPLabel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(button1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(2049, Short.MAX_VALUE))
            .addComponent(scrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(button1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(XPLabel))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(scrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 1095, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void button1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_button1ActionPerformed
        Parent.UpdateDisplay();
        dispose();
    }//GEN-LAST:event_button1ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FlexAllocationDialog.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FlexAllocationDialog.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FlexAllocationDialog.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FlexAllocationDialog.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                FlexAllocationDialog dialog = new FlexAllocationDialog(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel DisplayPanel;
    private javax.swing.JLabel XPLabel;
    private java.awt.Button button1;
    private java.awt.ScrollPane scrollPane2;
    // End of variables declaration//GEN-END:variables
}
