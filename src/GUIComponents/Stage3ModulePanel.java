/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUIComponents;

import atowutility.ProgramDataSingleton;
import atowutility.classes.LogBookEntry;
import atowutility.classes.Module;
import atowutility.classes.Skill;
import atowutility.classes.SkillField;
import java.awt.Color;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JLabel;

/**
 *
 * @author Chris
 */
public class Stage3ModulePanel extends ModulePanel {

    /**
     * Creates new form ModulePanel
     */
    Module main;
    protected Module selected;

    List<String> AffiliationString;

    public Stage3ModulePanel(Module module, int startingyear, NewCharacterPopup parent) {
        super(module, startingyear, parent);
        this.main = module;
        AffiliationString = new ArrayList<>();
        AffiliationString.add("None");
        selectionPanels = new ArrayList<>();
        OptionPanelItems = new ArrayList<>();
        NewStuff = new ArrayList<>();
        initComponents();
        NameLabel.setText(main.Name);
        MiscTextLabel.setVisible(false);           
        SpecialComboBox.setEnabled(false);
        UpdateSkillFields();
        displayStats();
    }

    @Override
    public void UpdateSelections() {
        TakeModuleButton.setEnabled(true);

        drawText(false);
        repaint();
        revalidate();
    }

    private void displayStats() {
        selectionPanels.clear();
        OptionsPanel.removeAll();

        if (InfoPanel.isEnabled()) {
            drawText(true);
        }

        repaint();
        revalidate();
    }

    @Override
    protected void resetLists() {
        super.resetLists();

        //The next section looks at the skills and traits and checks if they need an optionpanel
        Traits.GetLogs().forEach((log) -> {
            Boolean checkTrait = true;

            for (String description : NewStuff) {
                if (description.equals(log.Description())) {
                    checkTrait = false;
                }
            }

            for (LogBookEntry trait : Parent.TraitMaster.GetLogs()) {
                if (log.Description().equals(trait.Description())) {
                    checkTrait = false;
                }
            }
            if (checkTrait) {
                String postTrait = createSelectionPanel("Trait:" + log.Description() + ":" + log.Value(), false);

                if (postTrait != null) {
                    CaughtStuff.Add("Trait:" + postTrait, log.Value());
                } else {
                    OptionPanelItems.add("Trait:" + log.Description() + ":" + log.Value());
                }

                log.RemoveMe = true;

            }
        });

        Skills.GetLogs().forEach((log) -> {
            Boolean checkSkill = true;

            for (String description : NewStuff) {
                if (description.equals(log.Description())) {
                    checkSkill = false;
                }
            }
            for (Skill skill : ProgramDataSingleton.SkillsMaster()) {
                if (log.Description().equals(skill.Name)) {
                    checkSkill = false;
                }
            }
            if (checkSkill) {
                String postSkill = createSelectionPanel("Skill:" + log.Description() + ":" + log.Value(), false);

                if (postSkill != null) {
                    CaughtStuff.Add("Skill:" + postSkill, log.Value());
                } else {
                    OptionPanelItems.add("Skill:" + log.Description() + ":" + log.Value());
                }

                log.RemoveMe = true;
            }
        });
        for (LogBookEntry log : Rules.GetLogs()) {
            if (!log.Value().equals("-9999") && log.Description().split("%")[0].equals("Choice")) {
                OptionPanelItems.add("Rule:" + log.Description() + ":" + log.Value());
            }
        }
        for (LogBookEntry log : CaughtStuff.GetLogs()) {
            switch (log.Description().split(":")[0]) {
                case "Skill": {
                    Skills.Add(log.Description().split(":")[1], log.Value());
                    break;
                }
                case "Trait": {
                    Traits.Add(log.Description().split(":")[1], log.Value());
                    break;
                }
            }
            log.RemoveMe = true;
        }
        CaughtStuff.Cleanup();
        Traits.Cleanup();
        Skills.Cleanup();
    }

    protected void drawText(Boolean resetOptions) {
        InfoPanel.removeAll();
        resetLists();

        if (selectionPanels.isEmpty()) {
            TakeModuleButton.setEnabled(false);
        } else {
            TakeModuleButton.setEnabled(true);
        }

        if (resetOptions) {
            OptionsPanel.removeAll();
            selectionPanels.clear();

            OptionPanelItems.forEach((input) -> {
                createSelectionPanel(input, true);
            });
        }

        //This checks if there's any options avaliable
        for (SelectionPaneObject pane : selectionPanels) {
            if (!pane.XPAllocated()) {
                TakeModuleButton.setEnabled(false);
            }
            if (pane.PropertyString() == null) {
                TakeModuleButton.setEnabled(false);
            } else {
                String[] paneInputs = pane.PropertyString().split("&");
                for (String i : paneInputs) {
                    String[] settings = i.split("\\@");
                    switch (settings[0]) {
                        case "Skill": {
                            Skills.Add(settings[1], settings[2]);
                            break;
                        }
                        case "Trait": {
                            Traits.Add(settings[1], settings[2]);
                            break;
                        }
                        case "Attribute": {
                            switch (settings[1]) {
                                case "Strength": {
                                    Attributes[0] += Integer.parseInt(settings[2]);
                                    break;
                                }
                                case "Body": {
                                    Attributes[1] += Integer.parseInt(settings[2]);
                                    break;
                                }
                                case "Reflexes": {
                                    Attributes[2] += Integer.parseInt(settings[2]);
                                    break;
                                }
                                case "Dexterity": {
                                    Attributes[3] += Integer.parseInt(settings[2]);
                                    break;
                                }
                                case "Intelligence": {
                                    Attributes[4] += Integer.parseInt(settings[2]);
                                    break;
                                }
                                case "Willpower": {
                                    Attributes[5] += Integer.parseInt(settings[2]);
                                    break;
                                }
                                case "Charisma": {
                                    Attributes[6] += Integer.parseInt(settings[2]);
                                    break;
                                }
                                case "Edge": {
                                    Attributes[7] += Integer.parseInt(settings[2]);
                                    break;
                                }
                            }
                            break;
                        }

                        case "Rule": {
                            switch (settings[1].split(":")[0]) {
                                case "Attribute": {
                                    switch (settings[1].split(":")[1]) {
                                        case "Strength": {
                                            Attributes[0] += Integer.parseInt(settings[2]);
                                            break;
                                        }
                                        case "Body": {
                                            Attributes[1] += Integer.parseInt(settings[2]);
                                            break;
                                        }
                                        case "Reflexes": {
                                            Attributes[2] += Integer.parseInt(settings[2]);
                                            break;
                                        }
                                        case "Dexterity": {
                                            Attributes[3] += Integer.parseInt(settings[2]);
                                            break;
                                        }
                                        case "Intelligence": {
                                            Attributes[4] += Integer.parseInt(settings[2]);
                                            break;
                                        }
                                        case "Willpower": {
                                            Attributes[5] += Integer.parseInt(settings[2]);
                                            break;
                                        }
                                        case "Charisma": {
                                            Attributes[6] += Integer.parseInt(settings[2]);
                                            break;
                                        }
                                        case "Edge": {
                                            Attributes[7] += Integer.parseInt(settings[2]);
                                            break;
                                        }
                                    }
                                    break;
                                }
                                case "Skill": {
                                    Skills.Add(settings[1].split(":")[1], settings[2]);
                                    break;
                                }
                                case "Trait": {
                                    Traits.Add(settings[1].split(":")[1], settings[2]);
                                    break;
                                }
                                case "SkillField": {
                                    Boolean exists = false;
                                    Boolean badentries = false;
                                    for (SkillField sf : main.TakenSkillFields) {
                                        if (sf.Flex && sf.Name.equals(settings[1].split(":")[1])) {
                                            exists = true;
                                        }
                                        if (sf.Flex && !sf.Name.equals(settings[1].split(":")[1])) {
                                            badentries = true;
                                        }
                                    }
                                    if (!exists || badentries) {
                                        Predicate<SkillField> FieldPredicate = p -> p.Flex == true;
                                        main.TakenSkillFields.removeIf(FieldPredicate);

                                        for (SkillField sf : main.AvailableSkillFields) {
                                            if (sf.Name.equals(settings[1].split(":")[1])) {
                                                sf.XP = Integer.parseInt(settings[2]);
                                                main.TakenSkillFields.add(sf);
                                            }
                                        }
                                        resetLists();
                                    }

                                }
                            }
                            break;
                        }

                        default: {
                            return;
                        }
                    }
                }
            }
        }

        for (int x = 0; x < Attributes.length; x++) {
            String text = "Attribute:";
            switch (x) {
                case 0: {
                    text += " Strength";
                    break;
                }
                case 1: {
                    text += " Body";
                    break;
                }
                case 2: {
                    text += " Reflexes";
                    break;
                }
                case 3: {
                    text += " Dexterity";
                    break;
                }
                case 4: {
                    text += " Intelligence";
                    break;
                }
                case 5: {
                    text += " Willpower";
                    break;
                }
                case 6: {
                    text += " Charisma";
                    break;
                }
                default: {
                    text += " Edge";
                    break;
                }
            }

            if (Attributes[x] > 0) {
                text += " +";
            } else {
                text += " ";
            }
            if (Attributes[x] != 0) {
                text += Integer.toString(Attributes[x]);
                JLabel label = new JLabel();
                label.setText(text);
                InfoPanel.add(label);
            }
        }
        for (SkillField sf : main.TakenSkillFields) {
            JLabel sfieldlabel = new JLabel("++ " + sf.Name + " (" + sf.Refund() + " XP Refund) ++");
            sfieldlabel.setForeground(new Color(0, 128, 0));
            InfoPanel.add(sfieldlabel);
        }
        Traits.GetLogs().stream().map((log) -> {
            String value;
            if (Integer.parseInt(log.Value()) > 0) {
                value = "+" + log.Value();
            } else {
                value = log.Value();
            }
            String text = "Trait: " + log.Description() + " " + value;
            return text;
        }).map((text) -> {
            JLabel label = new JLabel();
            label.setText(text);
            return label;
        }).forEachOrdered((label) -> {
            InfoPanel.add(label);
        });

        Skills.GetLogs().stream().map((log) -> {
            String value;
            if (Integer.parseInt(log.Value()) > 0) {
                value = "+" + log.Value();
            } else {
                value = log.Value();
            }
            String text = "Skill: " + log.Description() + " " + value;
            return text;
        }).map((text) -> {
            JLabel label = new JLabel();
            label.setText(text);
            return label;
        }).forEachOrdered((label) -> {
            InfoPanel.add(label);
        });

        TakeModuleButton.setText(Cost + " XP");
        if (selectionPanels.isEmpty()) {
            TakeModuleButton.setEnabled(true);
        }
        if (Cost > Parent.CurrentXP) {
            TakeModuleButton.setEnabled(false);
        }
        Double dyears = Years();
        int years = (int)Math.floor(dyears);
        if (dyears - Math.floor(dyears) == 0) {            
            YearLabel.setText("+" + years + " Years");
        } else {
            double monthDecimal = dyears - Math.floor(dyears);
            int month = (int) Math.floor(12 * monthDecimal);
            YearLabel.setText("+" + years + " Years " + month + " Mo.");
        }
    }

    private String createSelectionPanel(String input, Boolean makePanel) {
        String numberOfChoices = "1";
        String[] inputArray = input.split(":");
        if (inputArray[1].split("\\#").length > 1) {
            numberOfChoices = inputArray[1].split("\\#")[1];
        }

        String type = inputArray[0];
        String[] output;
        int[] xp = new int[5];
        if (inputArray[2].split("/").length == 1) {
            xp[0] = Integer.parseInt(inputArray[2]);
            xp[1] = Integer.parseInt(inputArray[2]);
            xp[2] = Integer.parseInt(inputArray[2]);
            xp[3] = Integer.parseInt(inputArray[2]);
            xp[4] = Integer.parseInt(inputArray[2]);
        } else {
            for (int x = 0; x < inputArray[2].split("/").length; x++) {
                xp[x] = Integer.parseInt(inputArray[2].split("/")[x]);
            }
        }

        switch (type) {
            case "Trait": {
                output = breakTraitString(inputArray[1].split("#")[0]);
                break;
            }
            case "Skill": {
                output = breakSkillString(inputArray[1].split("#")[0]);
                break;
            }
            case "Rule": {
                output = breakRuleString(inputArray[1].split("#")[0]);
                break;
            }
            case "Attribute": {
                output = breakAttributeString(inputArray[1].split("#")[0]);
                break;
            }
            default: {
                output = new String[]{"Something's fucked."};
                break;
            }
        }

        if (output.length == 1) {
            Boolean badinput = true;
            switch (type) {
                case "Skill": {
                    for (LogBookEntry skill : Parent.SkillMaster.GetLogs()) {
                        if (skill.Description().equals(output[0])) {
                            badinput = false;
                            CaughtStuff.Add("Skill:" + output[0], inputArray[2]);
                        }
                    }
                }
            }
            if (badinput) {
                System.out.println("Not a valid input for a " + type + " selection panel: " + input + ": " + output[0]);
            }
            return null;
        }

        if (output.length > 2) {
            if (makePanel) {
                switch (numberOfChoices) {
                    case "1": {
                        SelectionPane panel = new SelectionPane(output, type, xp, this, ParentFrame);
                        selectionPanels.add(panel);
                        OptionsPanel.add(panel);
                        break;
                    }
                    case "2": {
                        SelectionPane2 panel = new SelectionPane2(output, type, xp, this, ParentFrame, false);
                        selectionPanels.add(panel);
                        OptionsPanel.add(panel);
                        break;
                    }
                    case "2R": {
                        SelectionPane2 panel = new SelectionPane2(output, type, xp, this, ParentFrame, true);
                        selectionPanels.add(panel);
                        OptionsPanel.add(panel);
                        break;
                    }
                    case "3": {
                        SelectionPane3 panel = new SelectionPane3(output, type, xp, this, ParentFrame, false);
                        selectionPanels.add(panel);
                        OptionsPanel.add(panel);
                        break;
                    }
                    case "3R": {
                        SelectionPane3 panel = new SelectionPane3(output, type, xp, this, ParentFrame, true);
                        selectionPanels.add(panel);
                        OptionsPanel.add(panel);
                        break;
                    }
                    case "4": {
                        SelectionPane4 panel = new SelectionPane4(output, type, xp, this, ParentFrame, false);
                        selectionPanels.add(panel);
                        OptionsPanel.add(panel);
                        break;
                    }
                    case "5": {
                        SelectionPane5 panel = new SelectionPane5(output, type, xp, this, ParentFrame, false);
                        selectionPanels.add(panel);
                        OptionsPanel.add(panel);
                        break;
                    }
                    case "F": {
                        List<String> temp = new ArrayList<>();
                        int[] limiter = {0, 0, 0};
                        for (String o : main.Rules.split(",")) {
                            switch (o.split(":")[0].split("%")[0]) {
                                case "FlexLimit": {
                                    int value = Integer.parseInt(o.split(":")[1]);
                                    switch (o.split(":")[0].split("%")[1]) {
                                        case "Attributes": {
                                            limiter[0] = value;
                                            break;
                                        }
                                        case "Traits": {
                                            limiter[1] = value;
                                            break;
                                        }
                                        case "Skills": {
                                            limiter[2] = value;
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                        for (String o : output) {
                            temp.add(o);
                        }
                        for (SkillField sf : main.AvailableSkillFields) {
                            if (sf.Flex) {
                                temp.add("AAAAA:" + sf.Name + "#" + Integer.toString(sf.SkillCount()) + "/" + Integer.toString(sf.XP));
                            }
                        }
                        output = new String[temp.size()];
                        temp.toArray(output);

                        FlexSelectionPane panel = new FlexSelectionPane(output, type, xp, limiter, this, Parent.Stage, ParentFrame, this);
                        selectionPanels.add(panel);
                        OptionsPanel.add(panel);
                        break;
                    }
                }

            }
            return null;
        } else {
            return output[1];
        }

    }

    @Override
    protected void selectModule() {

        String attributes = "Strength:" + Attributes[0] + ",Body:" + Attributes[1] + ",Reflexes:" + Attributes[2] + ",Dexterity:" + Attributes[3] + ",Intelligence:" + Attributes[4] + ",Willpower:" + Attributes[5] + ",Charisma:" + Attributes[6] + ",Edge:" + Attributes[7];
        String prerequisites = "Strength:" + Prerequisites[0] + ",Body:" + Prerequisites[1] + ",Reflexes:" + Prerequisites[2] + ",Dexterity:" + Prerequisites[3] + ",Intelligence:" + Prerequisites[4] + ",Willpower:" + Prerequisites[5] + ",Charisma:" + Prerequisites[6] + ",Edge:" + Prerequisites[7];
        for (LogBookEntry log : ExtraPrerequisites.GetLogs()) {
            prerequisites += "," + log.Description() + ":" + log.Value();
        }

        String skills = "";
        for (LogBookEntry log : Skills.GetLogs()) {
            if (!"".equals(skills)) {
                skills += ",";
            }

            skills += log.Description() + ":" + log.Value();
        }
        String traits = "";
        for (LogBookEntry log : Traits.GetLogs()) {
            if (!"".equals(traits)) {
                traits += ",";
            }

            traits += log.Description() + ":" + log.Value();
        }
        String rules = "";
        for (LogBookEntry log : Rules.GetLogs()) {
            if (!"".equals(rules)) {
                rules += ",";
            }

            rules += log.Description() + ":" + log.Value();
        }
        String restrictions = "";
        for (LogBookEntry log : Restrictions.GetLogs()) {
            if (!"".equals(restrictions)) {
                restrictions += ",";
            }

            restrictions += log.Description() + ":" + log.Value();
        }

        String misc = "";
        selected = new Module(main.Stage, Cost, Years(), main.Name, attributes, traits, skills, prerequisites, restrictions, rules, "");
        for (SkillField sf : main.TakenSkillFields) {
            selected.TakenSkillFields.add(sf);
        }
        selected.MiscText = misc;
        selected.Stage3Type = main.Stage3Type;
        Parent.AddModule(selected);
    }

    private double Years() {
        double additional = 0;
        for (LogBookEntry log : Rules.GetLogs()) {
            if (log.Description().split("\\%")[0].split("\\#")[0].equals("Age")) {
                switch (log.Description().split("\\%")[1].split("@")[0]) {
                    case "Field": {
                        for (SkillField sf : main.TakenSkillFields) {
                            if (log.Description().split("\\%")[1].split("@")[1].equals(sf.Name)) {
                                additional += Double.parseDouble(log.Description().split("\\%")[0].split("\\#")[1]);
                                break;
                            }
                        }
                    }
                    case "Affiliation": {
                        if (log.Description().split("\\%")[1].split("@")[1].equals(Parent.Affiliation)) {
                            additional += Double.parseDouble(log.Description().split("\\%")[0].split("\\#")[1]);
                        }
                    }
                }
            }
        }
        for (SkillField sf : main.TakenSkillFields){
            switch (sf.Type){
                case "Basic": {additional += main.Stage3Aging[0]; break;}
                case "Advanced": {additional += main.Stage3Aging[1]; break;}
                case "Special": {additional += main.Stage3Aging[2]; break;}
            }
        }
        return main.Years + additional;
    }
    private void UpdateSkillFields(){
        Predicate<SkillField> pred = p -> !p.Automatic;
        main.TakenSkillFields.removeIf(pred);
        for (SkillField sf : main.AvailableSkillFields)
            if (sf.Name.equals(BasicComboBox.getSelectedItem()) || sf.Name.equals(AdvancedComboBox.getSelectedItem()) || sf.Name.equals(SpecialComboBox.getSelectedItem()))
                main.TakenSkillFields.add(sf);
        
        if (AdvancedComboBox.getItemCount() <= 1)
            AdvancedComboBox.setEnabled(false);
        if (SpecialComboBox.getItemCount() <= 1)
            SpecialComboBox.setEnabled(false);
        
        drawText(true);
    }

    private ComboBoxModel BasicComboBoxOptions() {
        BasicComboBox.setEnabled(true);
        List<String> options = new ArrayList<>();        
        for (SkillField sf : main.AvailableSkillFields) {
            if (sf.Type.equals("Basic")) {
                Boolean restricted = false;
                if (!sf.Restrictions.equals("")) {
                    String[] restrictions = sf.Restrictions.split(",");
                    for (String r : restrictions) {
                        Boolean breakout = false;
                        for (String test : r.split("\\|")) {
                            switch (test.split(":")[0]) {
                                case "!Affiliation": {
                                    restricted = !Parent.Affiliation.equals(test.split(":")[1]);
                                    if (!restricted) {
                                        breakout = true;
                                    }
                                    break;
                                }
                                case "!AffiliationType": {
                                    restricted = !Parent.AffiliationType.toString().equals(test.split(":")[1]);
                                    if (!restricted) {
                                        breakout = true;
                                    }
                                    break;
                                }
                            }
                            if (breakout) {
                                break;
                            }
                        }
                        if (restricted) {
                            break;
                        }
                    }
                }
                if (!restricted) {
                    options.add(sf.Name);
                }
            }
        }
        String[] o;
        if (options.size() == 1) {
            BasicComboBox.setEnabled(false);
        }
            
        o = new String[options.size()];
        options.toArray(o);
        
        return new DefaultComboBoxModel(o);
    }

    private ComboBoxModel AdvancedComboBoxOptions() {
        List<String> options = new ArrayList<>();
        options.add("None");
        for (SkillField sf : main.AvailableSkillFields) {
            if (sf.Type.equals("Advanced")) {
                Boolean restricted = false;
                if (!sf.Restrictions.equals("")) {
                    String[] restrictions = sf.Restrictions.split(",");
                    for (String r : restrictions) {
                        Boolean breakout = false;
                        for (String test : r.split("\\|")) {
                            switch (test.split(":")[0]) {
                                case "!Affiliation": {
                                    restricted = !Parent.Affiliation.equals(test.split(":")[1]);
                                    if (!restricted) {
                                        breakout = true;
                                    }
                                    break;
                                }
                                case "!AffiliationType": {
                                    restricted = !Parent.AffiliationType.toString().equals(test.split(":")[1]);
                                    if (!restricted) {
                                        breakout = true;
                                    }
                                    break;
                                }
                            }
                            if (breakout) {
                                break;
                            }
                        }
                        if (restricted) {
                            break;
                        }
                    }
                }
                if (!restricted) {
                    options.add(sf.Name);
                }
            }
        }
        String[] o = new String[options.size()];
        options.toArray(o);
        return new DefaultComboBoxModel(o);
    }

    private ComboBoxModel SpecialComboBoxOptions() {
        List<String> options = new ArrayList<>();
        options.add("None");
        for (SkillField sf : main.AvailableSkillFields) {
            if (sf.Type.equals("Advanced") || sf.Type.equals("Special")) {
                Boolean restricted = false;
                if (!sf.Restrictions.equals("")) {
                    String[] restrictions = sf.Restrictions.split(",");
                    for (String r : restrictions) {
                        Boolean breakout = false;
                        for (String test : r.split("\\|")) {
                            switch (test.split(":")[0]) {
                                case "!Affiliation": {
                                    restricted = !Parent.Affiliation.equals(test.split(":")[1]);
                                    if (!restricted) {
                                        breakout = true;
                                    }
                                    break;
                                }
                                case "!AffiliationType": {
                                    restricted = !Parent.AffiliationType.toString().equals(test.split(":")[1]);
                                    if (!restricted) {
                                        breakout = true;
                                    }
                                    break;
                                }
                            }
                            if (breakout) {
                                break;
                            }
                        }
                        if (restricted) {
                            break;
                        }
                    }
                }
                for (SkillField t : main.TakenSkillFields) {
                    if (t.Name.equals(sf.Name)) {
                        restricted = true;
                    }
                }
                if (!restricted) {
                    options.add(sf.Name);
                }
            }
        }
        String[] o = new String[options.size()];
        options.toArray(o);
        return new DefaultComboBoxModel(o);
    }
    

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        scrollPane1 = new java.awt.ScrollPane();
        InfoPanel = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        scrollPane2 = new java.awt.ScrollPane();
        OptionsPanel = new javax.swing.JPanel();
        MiscTextLabel = new javax.swing.JLabel();
        NameLabel = new javax.swing.JLabel();
        TakeModuleButton = new javax.swing.JButton();
        YearLabel = new javax.swing.JLabel();
        BasicComboBox = new javax.swing.JComboBox<>();
        AdvancedComboBox = new javax.swing.JComboBox<>();
        SpecialComboBox = new javax.swing.JComboBox<>();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();

        setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        setMaximumSize(new java.awt.Dimension(1000, 400));
        setPreferredSize(new java.awt.Dimension(1000, 400));

        scrollPane1.setMaximumSize(new java.awt.Dimension(600, 400));

        InfoPanel.addAncestorListener(new javax.swing.event.AncestorListener() {
            public void ancestorMoved(javax.swing.event.AncestorEvent evt) {
                InfoPanelAncestorMoved(evt);
            }
            public void ancestorAdded(javax.swing.event.AncestorEvent evt) {
            }
            public void ancestorRemoved(javax.swing.event.AncestorEvent evt) {
            }
        });
        InfoPanel.setLayout(new java.awt.GridLayout(0, 1));
        scrollPane1.add(InfoPanel);

        jPanel1.setMaximumSize(new java.awt.Dimension(780, 400));
        jPanel1.setPreferredSize(new java.awt.Dimension(780, 400));

        scrollPane2.setMinimumSize(new java.awt.Dimension(0, 158));
        scrollPane2.setPreferredSize(new java.awt.Dimension(600, 300));

        OptionsPanel.addAncestorListener(new javax.swing.event.AncestorListener() {
            public void ancestorMoved(javax.swing.event.AncestorEvent evt) {
                OptionsPanelAncestorMoved(evt);
            }
            public void ancestorAdded(javax.swing.event.AncestorEvent evt) {
            }
            public void ancestorRemoved(javax.swing.event.AncestorEvent evt) {
            }
        });
        OptionsPanel.setLayout(new java.awt.GridLayout(0, 1));
        scrollPane2.add(OptionsPanel);

        MiscTextLabel.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        MiscTextLabel.setText("Miscellaneous");
        MiscTextLabel.setMaximumSize(new java.awt.Dimension(212, 15));

        NameLabel.setFont(new java.awt.Font("Space Bd BT", 1, 24)); // NOI18N
        NameLabel.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        NameLabel.setText("jLabel1");

        TakeModuleButton.setText("Choose");
        TakeModuleButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TakeModuleButtonActionPerformed(evt);
            }
        });

        YearLabel.setFont(new java.awt.Font("Space Bd BT", 1, 24)); // NOI18N
        YearLabel.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        YearLabel.setText("jLabel1");

        BasicComboBox.setModel(BasicComboBoxOptions());
        BasicComboBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BasicComboBoxActionPerformed(evt);
            }
        });

        AdvancedComboBox.setModel(AdvancedComboBoxOptions());
        AdvancedComboBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AdvancedComboBoxActionPerformed(evt);
            }
        });

        SpecialComboBox.setModel(SpecialComboBoxOptions());
        SpecialComboBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                SpecialComboBoxActionPerformed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel1.setText("Basic");

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel2.setText("Advanced");

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel3.setText("Advanced/Special");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(scrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(MiscTextLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addComponent(NameLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(YearLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 228, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1)
                            .addComponent(BasicComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, 171, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2)
                            .addComponent(AdvancedComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, 171, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel3)
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(SpecialComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, 171, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 124, Short.MAX_VALUE)
                                .addComponent(TakeModuleButton)))))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(NameLabel)
                    .addComponent(YearLabel))
                .addGap(0, 0, 0)
                .addComponent(MiscTextLabel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(scrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 258, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jLabel2)
                    .addComponent(jLabel3))
                .addGap(0, 0, 0)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(TakeModuleButton)
                    .addComponent(BasicComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(AdvancedComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(SpecialComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(scrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 249, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
            .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.LEADING, 398, 398, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addComponent(scrollPane1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents

    private void OptionsPanelAncestorMoved(javax.swing.event.AncestorEvent evt) {//GEN-FIRST:event_OptionsPanelAncestorMoved
        revalidate();
    }//GEN-LAST:event_OptionsPanelAncestorMoved

    private void InfoPanelAncestorMoved(javax.swing.event.AncestorEvent evt) {//GEN-FIRST:event_InfoPanelAncestorMoved
        revalidate();
    }//GEN-LAST:event_InfoPanelAncestorMoved

    private void TakeModuleButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_TakeModuleButtonActionPerformed
        selectModule();
    }//GEN-LAST:event_TakeModuleButtonActionPerformed

    private void SpecialComboBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_SpecialComboBoxActionPerformed
        UpdateSkillFields();
    }//GEN-LAST:event_SpecialComboBoxActionPerformed

    private void BasicComboBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BasicComboBoxActionPerformed
        UpdateSkillFields();
    }//GEN-LAST:event_BasicComboBoxActionPerformed

    private void AdvancedComboBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_AdvancedComboBoxActionPerformed
        if (AdvancedComboBox.getSelectedIndex() == 0){
            SpecialComboBox.setSelectedIndex(0);
            SpecialComboBox.setEnabled(false);
        }
        else SpecialComboBox.setEnabled(true);
        UpdateSkillFields();
    }//GEN-LAST:event_AdvancedComboBoxActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox<String> AdvancedComboBox;
    private javax.swing.JComboBox<String> BasicComboBox;
    private javax.swing.JPanel InfoPanel;
    private javax.swing.JLabel MiscTextLabel;
    private javax.swing.JLabel NameLabel;
    private javax.swing.JPanel OptionsPanel;
    private javax.swing.JComboBox<String> SpecialComboBox;
    private javax.swing.JButton TakeModuleButton;
    private javax.swing.JLabel YearLabel;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPanel jPanel1;
    private java.awt.ScrollPane scrollPane1;
    private java.awt.ScrollPane scrollPane2;
    // End of variables declaration//GEN-END:variables
}
