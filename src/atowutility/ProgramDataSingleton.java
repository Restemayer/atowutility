/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package atowutility;

import atowutility.classes.Module;
import atowutility.classes.Trait;
import atowutility.classes.Skill;
import atowutility.classes.CharacterClass;
import atowutility.classes.Rank;
import atowutility.classes.SkillField;
import atowutility.classes.Stage0Module;
import atowutility.enums.Phenotype;
import atowutility.enums.Stage3Type;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.ListIterator;
import java.util.Set;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author Chris
 */
public class ProgramDataSingleton {

    public static CharacterClass CurrentCharacter;
    private static List<Skill> SkillsMaster;

    public static List<Skill> SkillsMaster() {
        return SkillsMaster;
    }
    public static List<Skill> SkillsNew;
    private static List<Trait> TraitsMaster;
    private static List<Trait> SkillDependentTraits;

    public static List<Trait> TraitsMaster() {
        return TraitsMaster;
    }
    public static List<Trait> TraitsNew;
    public static List<Module> ModulesMaster;
    public static List<SkillField> SkillFieldMaster;
    public static List<Rank> RankMaster;
    public static MainJFrame MainJFrame;

    public static Module ComStar;
    public static Module WOB;

    private ProgramDataSingleton() {
        SkillsMaster = new ArrayList<>();
        SkillsNew = new ArrayList<>();
        TraitsMaster = new ArrayList<>();
        ModulesMaster = new ArrayList<>();
        RankMaster = new ArrayList<>();
        SkillFieldMaster = new ArrayList<>();
        SkillDependentTraits = new ArrayList<>();

        String[] skillslist = ReadCSV("skillsmaster.csv");

        for (String skillslist1 : skillslist) {
            Boolean expandible = false;
            String[] values = skillslist1.split(",");
            String[] id = values[0].split("/");
            if (id[id.length - 1].equals("ZZZZZ")) {
                expandible = true;
            }
            Skill newskill = new Skill(values[0], values[1], values[2], Boolean.parseBoolean(values[3]), expandible);
            SkillsMaster.add(newskill);
        }

        String[] traitslist = ReadCSV("traitmaster.csv");

        for (String traits : traitslist) {
            Boolean expandible = false;
            String[] values = traits.split(",");
            if (!"ignoreme".equals(values[0])) {
                String[] id = values[0].split("/");
                if (id[id.length - 1].equals("ZZZZZ")) {
                    expandible = true;
                }
                String name = values[0];
                int min = Integer.parseInt(values[1]);
                int max = Integer.parseInt(values[2]);
                Boolean vehicle = Boolean.parseBoolean(values[3]);
                Boolean identity = Boolean.parseBoolean(values[4]);
                String rules;
                String opposed;
                if (values.length < 6) {
                    rules = "";
                } else {
                    rules = values[5];
                }
                if (values.length < 7) {
                    opposed = "";
                } else {
                    opposed = values[6];
                }
                if (id[id.length - 1].equals("[SKILL]")) {
                    Trait skilltrait = new Trait(name, min, max, vehicle, identity, expandible, rules, opposed);
                    SkillDependentTraits.add(skilltrait);
                    String sname = "";
                    for (int s = 0 ; s < name.split("/").length-1; s++){                        
                        sname += name.split("/")[s] + "/";
                    }
                    for (Skill skill : SkillsMaster) {
                        if (!skill.Expandible()){
                            Trait newtrait = new Trait(sname + skill.Name, min, max, vehicle, identity, false, rules, opposed);                            
                            TraitsMaster.add(newtrait);
                        }
                    }
                } else {
                    Trait newtrait = new Trait(name, min, max, vehicle, identity, expandible, rules, opposed);
                    TraitsMaster.add(newtrait);
                }
            }
        }

        String[] files = ReadCSV("ModuleIndex");

        for (String file : files) {
            switch (file.split(":")[0]){
                case "Module":{
                    Module mod = ImportModule(file.split(":")[1]);
                    ModulesMaster.add(mod);
                    break;
                }
                case "Field":{
                    SkillField sf = ImportField(file.split(":")[1]);
                    SkillFieldMaster.add(sf);
                    break;
                }
                case "Rank":{
                    Rank r = ImportRank(file.split(":")[1]);
                    Trait newtrait = new Trait("Rank/" + r.Name, 1, 15, false, true, true, "", "");
                    TraitsMaster.add(newtrait);
                    RankMaster.add(r);
                }
            }                
            
            
        }

        //This is for the WOB & ComStar static modules
        String cTraits = "Enemy/Any:-100,Equipped:100,Rank/ComStar-Civilian|Rank/ComStar-Military:50,Reputation:-30,Connections:50,Enemy/Word of Blake:-100";
        String cSkills = "Communications/Conventional EM:10,Interest/Writings of Jerome Blake:10,Negotiation:10,Protocol/Nearest State:15,Protocol/ComStar:15,Technician/Any:10";
        String cAttributes = "Intelligence:25,Willpower:-15";
        ComStar = new Stage0Module(0, 0, 0, "ComStar", cAttributes, cTraits, cSkills, "", "Trait%Extra Income:X,Trait%Property:X", "", "", "English", "", "InnerSphere");
        cTraits = "Enemy/Any:-100,Equipped:130,Rank/Word of Blake-Civilian|Word of Blake-Military:50,Reputation:-50,Compulsion/Paranoia:-50,Connections:75,Enemy/ComStar:-100";
        cSkills = "Communications/Conventional EM:10,Interest/Writings of Jerome Blake:25,Negotiation:20,Interest/Writings of the Master:15,Protocol/Nearest State:5,Protocol/Word of Blake:10,Technician/Any:10";
        cAttributes = "Willpower:50,Charisma:-50";
        WOB = new Stage0Module(0, 0, 0, "Word of Blake", cAttributes, cTraits, cSkills, "", "Trait%Extra Income:X,Trait%Property:X", "", "", "English", "", "InnerSphere");

        //Probably remove this later.  For testing only... I think.   
        //Be sure to create a new initialization line above when below is deleted
        TraitsNew = TraitsMaster;
        SortLists();
        for (Module mod : ModulesMaster){
            mod.SystemCheck();
        }
    }

    public static ProgramDataSingleton getInstance() {
        return ProgramDataSingletonHolder.INSTANCE;
    }

    private static class ProgramDataSingletonHolder {

        private static final ProgramDataSingleton INSTANCE = new ProgramDataSingleton();
    }

    private static String[] ReadCSV(String filename) {

        System.out.println("Importing " + filename);
        String[] lines;
        try {
            File file = new File("./Files/CSVFiles/" + filename);

            if (file.exists()) {

                FileReader fr = new FileReader(file);
                try (LineNumberReader lnr = new LineNumberReader(fr)) {
                    int linenumber = 0;

                    while (lnr.readLine() != null) {
                        linenumber++;
                    }

                    lines = new String[linenumber];
                }

            } else {
                System.out.println("File does not exist!");
                return null;
            }

        } catch (IOException e) {
            return null;
        }

        BufferedReader br = null;
        String line;
        try {
            br = new BufferedReader(new FileReader(new File("./Files/CSVFiles/" + filename)));
            int index = 0;
            while ((line = br.readLine()) != null) {
                lines[index] = line;
                index++;
            }

        } catch (FileNotFoundException e) {
        } catch (IOException e) {
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                }
            }
        }
        return lines;
    }

    public static Module ImportModule(String filename) {
        System.out.println("Importing module" + filename);
        

        try {
            String name;
            String attributes;
            String skills;
            String fields;
            String traits;
            String prerequisites;
            String restrictions;
            String rules;
            int stage;
            int cost;
            int years;

            File fXmlFile = new File("./Files/Modules/" + filename);
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(fXmlFile);

            doc.getDocumentElement().normalize();

            NodeList nList = doc.getElementsByTagName("Module");

            Node nNode = nList.item(0);
            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                Element eElement = (Element) nNode;

                name = eElement.getElementsByTagName("Name").item(0).getTextContent();
                attributes = eElement.getElementsByTagName("Attributes").item(0).getTextContent();
                skills = eElement.getElementsByTagName("Skills").item(0).getTextContent();
                traits = eElement.getElementsByTagName("Traits").item(0).getTextContent();
                fields = eElement.getElementsByTagName("Fields").item(0).getTextContent();
                prerequisites = eElement.getElementsByTagName("Prerequisites").item(0).getTextContent();
                restrictions = eElement.getElementsByTagName("Restrictions").item(0).getTextContent();
                rules = eElement.getElementsByTagName("Rules").item(0).getTextContent();
                stage = Integer.parseInt(eElement.getElementsByTagName("Stage").item(0).getTextContent());
                cost = Integer.parseInt(eElement.getElementsByTagName("Cost").item(0).getTextContent());
                years = Integer.parseInt(eElement.getElementsByTagName("Years").item(0).getTextContent());

                switch (stage) {
                    case 0: {
                        Stage0Module module;
                        String housename = eElement.getElementsByTagName("HouseName").item(0).getTextContent();
                        String primarylanguage = eElement.getElementsByTagName("PrimaryLanguage").item(0).getTextContent();
                        String secondarylanguage = eElement.getElementsByTagName("SecondaryLanguage").item(0).getTextContent();
                        String type = eElement.getElementsByTagName("AffiliationType").item(0).getTextContent();
                        
                        module = new Stage0Module(stage, cost, years, name, attributes, traits, skills, prerequisites, restrictions, rules, housename, primarylanguage, secondarylanguage, type);

                        nList = doc.getElementsByTagName("Subaffiliation");

                        for (int i = 0; i < nList.getLength(); i++) {
                            Node node = nList.item(i);
                            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                                Element subElement = (Element) nNode;

                                name = subElement.getElementsByTagName("SubName").item(i).getTextContent();
                                attributes = subElement.getElementsByTagName("SubAttributes").item(i).getTextContent();
                                skills = subElement.getElementsByTagName("SubSkills").item(i).getTextContent();
                                traits = subElement.getElementsByTagName("SubTraits").item(i).getTextContent();
                                prerequisites = subElement.getElementsByTagName("SubPrerequisites").item(i).getTextContent();
                                restrictions = subElement.getElementsByTagName("SubRestrictions").item(i).getTextContent();
                                rules = subElement.getElementsByTagName("SubRules").item(i).getTextContent();
                                primarylanguage = subElement.getElementsByTagName("SubPrimaryLanguages").item(i).getTextContent();
                                secondarylanguage = subElement.getElementsByTagName("SubSecondaryLanguages").item(i).getTextContent();

                                Stage0Module sub = new Stage0Module(stage, 0, 0, name, attributes, traits, skills, prerequisites, restrictions, rules, "", primarylanguage, secondarylanguage, type);
                                module.Subaffiliations.add(sub);
                            }
                        }
                        return module;
                    }
                    case 3:{
                        Module mod = new Module(stage, cost, years, name, attributes, traits, skills, prerequisites, restrictions, rules, fields);
                        mod.Stage3Type = Stage3Type.valueOf(eElement.getElementsByTagName("Stage3Type").item(0).getTextContent());
                        String[] aging = eElement.getElementsByTagName("Stage3Aging").item(0).getTextContent().split("/");
                        for (int x = 0;x < 3; x++)
                            mod.Stage3Aging[x] = Double.parseDouble(aging[x]);
                        return mod;
                    }
                    default: {                        
                        Module mod = new Module(stage, cost, years, name, attributes, traits, skills, prerequisites, restrictions, rules, fields);
                        if (stage == 4) mod.Repeatable = Boolean.parseBoolean(eElement.getElementsByTagName("Repeatable").item(0).getTextContent());
                        return mod;                        
                    }
                }
            }
        } catch (IOException | ParserConfigurationException | DOMException | SAXException e) {
            return null;
        }

        return null;
    }
    public static SkillField ImportField(String filename) {
        System.out.println("Importing skill field " + filename);
        

        try {
            String catagory;
            String name;
            String skills;
            String prerequisites;
            String restrictions;
            String rules;

            File fXmlFile = new File("./Files/SkillFields/" + filename);
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(fXmlFile);

            doc.getDocumentElement().normalize();

            NodeList nList = doc.getElementsByTagName("SkillField");

            Node nNode = nList.item(0);
            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                Element eElement = (Element) nNode;

                name = eElement.getElementsByTagName("Name").item(0).getTextContent();
                catagory = eElement.getElementsByTagName("Catagory").item(0).getTextContent();
                skills = eElement.getElementsByTagName("Skills").item(0).getTextContent();
                prerequisites = eElement.getElementsByTagName("Prerequisites").item(0).getTextContent();
                restrictions = eElement.getElementsByTagName("Restrictions").item(0).getTextContent();
                rules = eElement.getElementsByTagName("Rules").item(0).getTextContent();  
                if ("".equals(name) || name == null)
                    System.out.println("SkillField file " + filename + " failed to load");
                return new SkillField(catagory, name, skills, prerequisites, restrictions, rules);
            }            
            
        } catch (IOException | ParserConfigurationException | DOMException | SAXException e) {
            return null;
        }

        return null;
    }
    public static Rank ImportRank(String filename) {
        System.out.println("Importing ranksheet " + filename);
        

        try {
            String name;
            String affiliation;
            String enlisted;
            String officer;
            String years;

            File fXmlFile = new File("./Files/" + filename);
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(fXmlFile);

            doc.getDocumentElement().normalize();

            NodeList nList = doc.getElementsByTagName("Rank");

            Node nNode = nList.item(0);
            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                Element eElement = (Element) nNode;

                name = eElement.getElementsByTagName("Name").item(0).getTextContent();
                affiliation = eElement.getElementsByTagName("Affiliation").item(0).getTextContent();
                years = eElement.getElementsByTagName("Years").item(0).getTextContent();
                enlisted = eElement.getElementsByTagName("Enlisted").item(0).getTextContent();
                officer = eElement.getElementsByTagName("Officer").item(0).getTextContent(); 
                
                return new Rank(name, years, affiliation, enlisted, officer);
            }            
            
        } catch (IOException | ParserConfigurationException | DOMException | SAXException e) {
            return null;
        }

        return null;
    }

    public static void SortLists() {
        if (CurrentCharacter != null) {
            Collections.sort(CurrentCharacter.Skills, (final Skill object1, final Skill object2) -> object1.Name.compareTo(object2.Name));
            Collections.sort(CurrentCharacter.Traits, (final Trait object1, final Trait object2) -> object1.Name.compareTo(object2.Name));
        }
        Collections.sort(SkillsNew, (final Skill object1, final Skill object2) -> object1.Name.compareTo(object2.Name));
        Collections.sort(TraitsNew, (final Trait object1, final Trait object2) -> object1.Name.compareTo(object2.Name));
    }

    public static void SetMainFrame(MainJFrame frame) {
        MainJFrame = frame;
    }

    public static void AddNewSkill(Skill skill) {
        if (CurrentCharacter != null) {
            CurrentCharacter.Skills.add(skill);
            SortLists();
            MainJFrame.RefreshSkills();
        }
    }

    public static Skill FindMasterSkill(String name) {
        Skill skill = null;
        for (ListIterator<Skill> iter = SkillsMaster.listIterator(); iter.hasNext();) {
            Skill element = iter.next();
            if (element.Name.equals(name)) {
                skill = element;
            }
        }
        return skill;
    }

    public static void RefreshSkillScreen() {
        MainJFrame.RefreshSkills();
    }

    //this is a placeholder for testing
    public static void CreateNewCharacter() {
        CurrentCharacter = new CharacterClass("Bob", Phenotype.Elemental);
        CurrentCharacter.SetMainScreen(MainJFrame);
        CurrentCharacter.EnterXPLog("This is a test", 2000);
        CurrentCharacter.Intelligence.ChangeExperience("Adding XP", 200);
        MainJFrame.DisplayScreen();
    }

    public static String RemoveDuplicates(String input) {
        List<String> l = Arrays.asList(input.split(","));
        Set<String> s = new HashSet<>(l);
        l = new ArrayList<>(s);
        Collections.sort(l, (final String object1, final String object2) -> object1.compareTo(object2));
        String output = "";
        for (String t : l) {
            if (!"".equals(output)) {
                output += ",";
            }
            output += t;
        }
        return output;
    }

    public static String[] RemoveDuplicates(String[] input) {
        List<String> l = Arrays.asList(input);
        Set<String> s = new HashSet<>(l);
        l = new ArrayList<>(s);
        Collections.sort(l, (final String object1, final String object2) -> object1.compareTo(object2));
        String[] output = new String[l.size()];
        output = l.toArray(output);

        return output;
    }
}
