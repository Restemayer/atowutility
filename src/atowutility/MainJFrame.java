/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package atowutility;
import atowutility.classes.Trait;
import atowutility.classes.CharacterClass;
import atowutility.classes.AttributeClass;
import GUIComponents.*;
import java.awt.Color;
import java.awt.GridLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;


/**
 *
 * @author Chris
 */
public class MainJFrame extends javax.swing.JFrame {   
    
    
    /**
     * Creates new form MainJFrame
     */
    public MainJFrame() {
        initComponents();
        ProgramDataSingleton.getInstance();
        ProgramDataSingleton.SetMainFrame(this);    
        GridLayout layout = new GridLayout(0,1);
        AttributePanel.setVisible(false);
        SkillsPanel.setVisible(false);
        TraitsPanel.setVisible(false);
        EquipmentPanel.setVisible(false);
        VehiclePanel.setVisible(false);
        CombatDataPanel.setVisible(false);
        PersDataPanel.setVisible(false);
        XPPanel.setVisible(false);
        ModulesPanel.setVisible(false);
        SkillDisplayPanel.setLayout(layout);
    }
    
    private void displayAttributes(){
        CharacterClass current = ProgramDataSingleton.CurrentCharacter;
        StrengthScoreLabel.setText(current.Strength.ScoreString());
        BodyScoreLabel.setText(current.Body.ScoreString());
        ReflexesScoreLabel.setText(current.Reflexes.ScoreString());
        DexterityScoreLabel.setText(current.Dexterity.ScoreString());
        IntelligenceScoreLabel.setText(current.Intelligence.ScoreString());
        WillpowerScoreLabel.setText(current.Willpower.ScoreString());
        CharismaScoreLabel.setText(current.Charisma.ScoreString());
        EdgeScoreLabel.setText(current.Edge.ScoreString());
    
        if(current.Strength.Score() == 0)
            StrengthScoreLabel.setForeground(Color.red);
        else StrengthScoreLabel.setForeground(Color.BLACK);
        if(current.Body.Score() == 0)
            BodyScoreLabel.setForeground(Color.red);
        else BodyScoreLabel.setForeground(Color.BLACK);
        if(current.Reflexes.Score() == 0)
            ReflexesScoreLabel.setForeground(Color.red);
        else ReflexesScoreLabel.setForeground(Color.BLACK);
        if(current.Dexterity.Score() == 0)
            DexterityScoreLabel.setForeground(Color.red);
        else DexterityScoreLabel.setForeground(Color.BLACK);
        if(current.Intelligence.Score() == 0)
            IntelligenceScoreLabel.setForeground(Color.red);
        else IntelligenceScoreLabel.setForeground(Color.BLACK);
        if(current.Willpower.Score() == 0)
            WillpowerScoreLabel.setForeground(Color.red);
        else WillpowerScoreLabel.setForeground(Color.BLACK);
        if(current.Charisma.Score() == 0)
            CharismaScoreLabel.setForeground(Color.red);
        else CharismaScoreLabel.setForeground(Color.BLACK);
        if(current.Edge.Score() == 0)
            EdgeScoreLabel.setForeground(Color.red);
        else EdgeScoreLabel.setForeground(Color.BLACK);
    
    
        StrengthLinkLabel.setText(current.Strength.LinkString());
        BodyLinkLabel.setText(current.Body.LinkString());
        ReflexesLinkLabel.setText(current.Reflexes.LinkString());
        DexterityLinkLabel.setText(current.Dexterity.LinkString());
        IntelligenceLinkLabel.setText(current.Intelligence.LinkString());
        WillpowerLinkLabel.setText(current.Willpower.LinkString());
        CharismaLinkLabel.setText(current.Charisma.LinkString());
        EdgeLinkLabel.setText(current.Edge.LinkString());
    
        StrengthXPLabel.setText(Integer.toString(current.Strength.Experience()));
        BodyXPLabel.setText(Integer.toString(current.Body.Experience()));
        ReflexesXPLabel.setText(Integer.toString(current.Reflexes.Experience()));
        DexterityXPLabel.setText(Integer.toString(current.Dexterity.Experience()));
        IntelligenceXPLabel.setText(Integer.toString(current.Intelligence.Experience()));
        WillpowerXPLabel.setText(Integer.toString(current.Willpower.Experience()));
        CharismaXPLabel.setText(Integer.toString(current.Charisma.Experience()));
        EdgeXPLabel.setText(Integer.toString(current.Edge.Experience()));  
        
        StrengthExceptionalAttributeLabel.setText(current.Strength.ExceptionalAttributeString());
        BodyExceptionalAttributeLabel.setText(current.Body.ExceptionalAttributeString());
        ReflexesExceptionalAttributeLabel.setText(current.Reflexes.ExceptionalAttributeString());
        DexterityExceptionalAttributeLabel.setText(current.Dexterity.ExceptionalAttributeString());
        IntelligenceExceptionalAttributeLabel.setText(current.Intelligence.ExceptionalAttributeString());
        WillpowerExceptionalAttributeLabel.setText(current.Willpower.ExceptionalAttributeString());
        CharismaExceptionalAttributeLabel.setText(current.Charisma.ExceptionalAttributeString());
        EdgeExceptionalAttributeLabel.setText(current.Edge.ExceptionalAttributeString()); 
        
        if ("Exceptional Attribute +2 TP".equals(StrengthExceptionalAttributeLabel.getText()))
            StrengthExceptionalAttributeLabel.setForeground(new Color(0,100,0));
        if ("Exceptional Attribute +2 TP".equals(BodyExceptionalAttributeLabel.getText()))
            BodyExceptionalAttributeLabel.setForeground(new Color(0,100,0));
        if ("Exceptional Attribute +2 TP".equals(ReflexesExceptionalAttributeLabel.getText()))
            ReflexesExceptionalAttributeLabel.setForeground(new Color(0,100,0));
        if ("Exceptional Attribute +2 TP".equals(DexterityExceptionalAttributeLabel.getText()))
            DexterityExceptionalAttributeLabel.setForeground(new Color(0,100,0));
        if ("Exceptional Attribute +2 TP".equals(IntelligenceExceptionalAttributeLabel.getText()))
            IntelligenceExceptionalAttributeLabel.setForeground(new Color(0,100,0));
        if ("Exceptional Attribute +2 TP".equals(WillpowerExceptionalAttributeLabel.getText()))
            WillpowerExceptionalAttributeLabel.setForeground(new Color(0,100,0));
        if ("Exceptional Attribute +2 TP".equals(CharismaExceptionalAttributeLabel.getText()))
            CharismaExceptionalAttributeLabel.setForeground(new Color(0,100,0));
        if ("Exceptional Attribute +2 TP".equals(EdgeExceptionalAttributeLabel.getText()))
            EdgeExceptionalAttributeLabel.setForeground(new Color(0,100,0));
        
        if (current.Strength.Experience() == current.Strength.MaxXP())
            StrengthAddXPButton.setEnabled(false);
        if (current.Body.Experience() == current.Body.MaxXP())
            BodyAddXPButton.setEnabled(false);
        if (current.Reflexes.Experience() == current.Reflexes.MaxXP())
            ReflexesAddXPButton.setEnabled(false);
        if (current.Dexterity.Experience() == current.Dexterity.MaxXP())
            DexterityAddXPButton.setEnabled(false);
        if (current.Intelligence.Experience() == current.Intelligence.MaxXP())
            IntelligenceAddXPButton.setEnabled(false);
        if (current.Willpower.Experience() == current.Willpower.MaxXP())
            WillpowerAddXPButton.setEnabled(false);
        if (current.Charisma.Experience() == current.Charisma.MaxXP())
            CharismaAddXPButton.setEnabled(false);
        if (current.Edge.Experience() == current.Edge.MaxXP())
            EdgeAddXPButton.setEnabled(false);            
    }
    private void displaySkills(){         
        if (ProgramDataSingleton.CurrentCharacter != null){   
            ProgramDataSingleton.CurrentCharacter.Skills.stream().filter((SkillsNew) -> (SkillsNew.Experience() >= SkillsNew.MinimumXP())).map((SkillsNew) -> new SkillPanel(SkillsNew)).forEachOrdered((panel) -> {
                SkillDisplayPanel.add(panel);
            });
            ProgramDataSingleton.CurrentCharacter.Skills.stream().filter((SkillsNew) -> (SkillsNew.Experience() < SkillsNew.MinimumXP())).map((SkillsNew) -> new SkillPanel(SkillsNew)).forEachOrdered((panel) -> {
                SkillDisplayPanel.add(panel);
            });
        }
        NewSkillButtonPanel button = new NewSkillButtonPanel(ProgramDataSingleton.CurrentCharacter != null);       
        
        SkillDisplayPanel.add(button);
        validate();
    }    
    private void displayTraits(){
        if (ProgramDataSingleton.CurrentCharacter != null){
            ProgramDataSingleton.CurrentCharacter.Traits.stream().map((currentTrait) -> new TraitPanel(currentTrait)).forEachOrdered((panel) -> {
                TraitDisplayPanel.add(panel);
            });
            ProgramDataSingleton.TraitsMaster().forEach((trait) -> {
                Boolean newTrait = true;
                if (trait.MinimumTP() <= 0 &&
                        trait.MaximumTP() >= 0 &&
                        !trait.Vehicle() &&
                        !"ZZZZZ".equals(trait.Name.split("/")[trait.Name.split("/").length - 1])){
                    for (Trait currentTrait : ProgramDataSingleton.CurrentCharacter.Traits){
                        if (currentTrait.Name.equals(trait.Name))
                            newTrait = false;
                    }
                }
                else newTrait = false;
                if (newTrait) {
                    TraitPanel panel = new TraitPanel(trait);
                    TraitDisplayPanel.add(panel);
                }
            });
            NewTraitButtonPanel button = new NewTraitButtonPanel(true);
            TraitDisplayPanel.add(button);
         }
    }
    public void RefreshSkills(){        
        SkillDisplayPanel.removeAll();
        UpdateXP();
        displaySkills();
    }
    
    public void DisplayScreen(){
        AttributePanel.setVisible(true);
        SkillsPanel.setVisible(true);
        TraitsPanel.setVisible(true);
        EquipmentPanel.setVisible(true);
        VehiclePanel.setVisible(true);
        CombatDataPanel.setVisible(true);
        PersDataPanel.setVisible(true);
        XPPanel.setVisible(true);
        ModulesPanel.setVisible(true);
        displayAttributes();
        displaySkills();
        displayTraits();
    }
    public void UpdateXP(){
        XPLabel.setText(Integer.toString(ProgramDataSingleton.CurrentCharacter.Experience()));
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        SkillsPanel = new javax.swing.JPanel();
        scrollPane1 = new java.awt.ScrollPane();
        SkillDisplayPanel = new javax.swing.JPanel();
        TraitsPanel = new javax.swing.JPanel();
        scrollPane2 = new java.awt.ScrollPane();
        TraitDisplayPanel = new javax.swing.JPanel();
        EquipmentPanel = new javax.swing.JPanel();
        PersDataPanel = new javax.swing.JPanel();
        NameLabel = new javax.swing.JLabel();
        AffiliationLabel = new javax.swing.JLabel();
        HeightWeightLabel = new javax.swing.JLabel();
        DescriptionLabel = new javax.swing.JLabel();
        XPPanel = new javax.swing.JPanel();
        XPLabel = new javax.swing.JLabel();
        AttributePanel = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        BodyPanel = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        BodyScoreLabel = new javax.swing.JLabel();
        BodyLinkLabel = new javax.swing.JLabel();
        BodyXPLabel = new javax.swing.JLabel();
        BodyExceptionalAttributeLabel = new javax.swing.JLabel();
        BodyAddXPButton = new javax.swing.JButton();
        BodyExAttribButton = new javax.swing.JButton();
        BodyXPField = new javax.swing.JTextField();
        ReflexesPanel = new javax.swing.JPanel();
        jLabel7 = new javax.swing.JLabel();
        ReflexesScoreLabel = new javax.swing.JLabel();
        ReflexesLinkLabel = new javax.swing.JLabel();
        ReflexesXPLabel = new javax.swing.JLabel();
        ReflexesExceptionalAttributeLabel = new javax.swing.JLabel();
        ReflexesAddXPButton = new javax.swing.JButton();
        ReflexesExAttribButton = new javax.swing.JButton();
        ReflexesXPField = new javax.swing.JTextField();
        DexterityPanel = new javax.swing.JPanel();
        jLabel8 = new javax.swing.JLabel();
        DexterityScoreLabel = new javax.swing.JLabel();
        DexterityLinkLabel = new javax.swing.JLabel();
        DexterityXPLabel = new javax.swing.JLabel();
        DexterityExceptionalAttributeLabel = new javax.swing.JLabel();
        DexterityAddXPButton = new javax.swing.JButton();
        DexterityExAttribButton = new javax.swing.JButton();
        DexterityXPField = new javax.swing.JTextField();
        IntelligencePanel = new javax.swing.JPanel();
        jLabel9 = new javax.swing.JLabel();
        IntelligenceScoreLabel = new javax.swing.JLabel();
        IntelligenceLinkLabel = new javax.swing.JLabel();
        IntelligenceXPLabel = new javax.swing.JLabel();
        IntelligenceExceptionalAttributeLabel = new javax.swing.JLabel();
        IntelligenceAddXPButton = new javax.swing.JButton();
        IntelligenceExAttribButton = new javax.swing.JButton();
        IntelligenceXPField = new javax.swing.JTextField();
        WillpowerPanel = new javax.swing.JPanel();
        jLabel10 = new javax.swing.JLabel();
        WillpowerScoreLabel = new javax.swing.JLabel();
        WillpowerLinkLabel = new javax.swing.JLabel();
        WillpowerXPLabel = new javax.swing.JLabel();
        WillpowerExceptionalAttributeLabel = new javax.swing.JLabel();
        WillpowerAddXPButton = new javax.swing.JButton();
        WillpowerExAttribButton = new javax.swing.JButton();
        WillpowerXPField = new javax.swing.JTextField();
        CharismaPanel = new javax.swing.JPanel();
        jLabel11 = new javax.swing.JLabel();
        CharismaScoreLabel = new javax.swing.JLabel();
        CharismaLinkLabel = new javax.swing.JLabel();
        CharismaXPLabel = new javax.swing.JLabel();
        CharismaExceptionalAttributeLabel = new javax.swing.JLabel();
        CharismaAddXPButton = new javax.swing.JButton();
        CharismaExAttribButton = new javax.swing.JButton();
        CharismaXPField = new javax.swing.JTextField();
        StrengthPanel = new javax.swing.JPanel();
        jLabel13 = new javax.swing.JLabel();
        StrengthScoreLabel = new javax.swing.JLabel();
        StrengthLinkLabel = new javax.swing.JLabel();
        StrengthXPLabel = new javax.swing.JLabel();
        StrengthExceptionalAttributeLabel = new javax.swing.JLabel();
        StrengthAddXPButton = new javax.swing.JButton();
        StrengthExAttribButton = new javax.swing.JButton();
        StrengthXPField = new javax.swing.JTextField();
        CharismaPanel1 = new javax.swing.JPanel();
        jLabel14 = new javax.swing.JLabel();
        EdgeScoreLabel = new javax.swing.JLabel();
        EdgeLinkLabel = new javax.swing.JLabel();
        EdgeXPLabel = new javax.swing.JLabel();
        EdgeExceptionalAttributeLabel = new javax.swing.JLabel();
        EdgeAddXPButton = new javax.swing.JButton();
        EdgeExAttribButton = new javax.swing.JButton();
        EdgeXPField = new javax.swing.JTextField();
        VehiclePanel = new javax.swing.JPanel();
        CombatDataPanel = new javax.swing.JPanel();
        ModulesPanel = new javax.swing.JPanel();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        NewCharMenuItem = new javax.swing.JMenuItem();
        OpenCharMenuItem = new javax.swing.JMenuItem();
        ExitMenuItem = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);

        SkillsPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Skills", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 16))); // NOI18N

        javax.swing.GroupLayout SkillDisplayPanelLayout = new javax.swing.GroupLayout(SkillDisplayPanel);
        SkillDisplayPanel.setLayout(SkillDisplayPanelLayout);
        SkillDisplayPanelLayout.setHorizontalGroup(
            SkillDisplayPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 445, Short.MAX_VALUE)
        );
        SkillDisplayPanelLayout.setVerticalGroup(
            SkillDisplayPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 1246, Short.MAX_VALUE)
        );

        scrollPane1.add(SkillDisplayPanel);

        javax.swing.GroupLayout SkillsPanelLayout = new javax.swing.GroupLayout(SkillsPanel);
        SkillsPanel.setLayout(SkillsPanelLayout);
        SkillsPanelLayout.setHorizontalGroup(
            SkillsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(scrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 430, Short.MAX_VALUE)
        );
        SkillsPanelLayout.setVerticalGroup(
            SkillsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(scrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        TraitsPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Traits", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 16))); // NOI18N

        TraitDisplayPanel.setLayout(new java.awt.GridLayout(0, 2));
        scrollPane2.add(TraitDisplayPanel);

        javax.swing.GroupLayout TraitsPanelLayout = new javax.swing.GroupLayout(TraitsPanel);
        TraitsPanel.setLayout(TraitsPanelLayout);
        TraitsPanelLayout.setHorizontalGroup(
            TraitsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(TraitsPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(scrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 688, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        TraitsPanelLayout.setVerticalGroup(
            TraitsPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(TraitsPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(scrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 466, Short.MAX_VALUE)
                .addContainerGap())
        );

        EquipmentPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Equipment", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 16))); // NOI18N

        javax.swing.GroupLayout EquipmentPanelLayout = new javax.swing.GroupLayout(EquipmentPanel);
        EquipmentPanel.setLayout(EquipmentPanelLayout);
        EquipmentPanelLayout.setHorizontalGroup(
            EquipmentPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        EquipmentPanelLayout.setVerticalGroup(
            EquipmentPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        PersDataPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Personal Data", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 16))); // NOI18N

        NameLabel.setFont(new java.awt.Font("Perpetua", 1, 36)); // NOI18N
        NameLabel.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        NameLabel.setText("Name");

        AffiliationLabel.setFont(new java.awt.Font("Perpetua", 0, 24)); // NOI18N
        AffiliationLabel.setText("Affiliation");

        HeightWeightLabel.setFont(new java.awt.Font("Perpetua", 0, 18)); // NOI18N
        HeightWeightLabel.setText("Height/Weight:");

        DescriptionLabel.setFont(new java.awt.Font("Perpetua", 0, 18)); // NOI18N
        DescriptionLabel.setText("Hair Color/Eye Color:");

        javax.swing.GroupLayout PersDataPanelLayout = new javax.swing.GroupLayout(PersDataPanel);
        PersDataPanel.setLayout(PersDataPanelLayout);
        PersDataPanelLayout.setHorizontalGroup(
            PersDataPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PersDataPanelLayout.createSequentialGroup()
                .addGroup(PersDataPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(AffiliationLabel)
                    .addGroup(PersDataPanelLayout.createSequentialGroup()
                        .addComponent(HeightWeightLabel)
                        .addGap(18, 18, 18)
                        .addComponent(DescriptionLabel))
                    .addComponent(NameLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 138, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        PersDataPanelLayout.setVerticalGroup(
            PersDataPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PersDataPanelLayout.createSequentialGroup()
                .addComponent(NameLabel)
                .addGap(0, 0, 0)
                .addComponent(AffiliationLabel)
                .addGap(0, 0, 0)
                .addGroup(PersDataPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(HeightWeightLabel)
                    .addComponent(DescriptionLabel))
                .addGap(4, 4, 4))
        );

        XPPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Unallocated XP", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 16))); // NOI18N

        XPLabel.setFont(new java.awt.Font("Perpetua Titling MT", 1, 24)); // NOI18N
        XPLabel.setForeground(new java.awt.Color(255, 51, 51));
        XPLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        XPLabel.setText("9999");

        javax.swing.GroupLayout XPPanelLayout = new javax.swing.GroupLayout(XPPanel);
        XPPanel.setLayout(XPPanelLayout);
        XPPanelLayout.setHorizontalGroup(
            XPPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 138, Short.MAX_VALUE)
            .addGroup(XPPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(XPPanelLayout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addComponent(XPLabel)
                    .addGap(0, 0, Short.MAX_VALUE)))
        );
        XPPanelLayout.setVerticalGroup(
            XPPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
            .addGroup(XPPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(XPPanelLayout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addComponent(XPLabel)
                    .addGap(0, 0, Short.MAX_VALUE)))
        );

        AttributePanel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Attributes", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 16))); // NOI18N

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel1.setText("  Attribute");

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel2.setText("Score (Max)");

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel3.setText("Link");

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel4.setText("Experience");

        BodyPanel.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        jLabel6.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        jLabel6.setText("   Body");

        BodyScoreLabel.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        BodyScoreLabel.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        BodyScoreLabel.setText("+2 (9)");

        BodyLinkLabel.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        BodyLinkLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        BodyLinkLabel.setText("+2");

        BodyXPLabel.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        BodyXPLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        BodyXPLabel.setText("9999 XP");

        BodyExceptionalAttributeLabel.setFont(new java.awt.Font("Times New Roman", 0, 12)); // NOI18N
        BodyExceptionalAttributeLabel.setText("Exceptional Attribute: 100/999 XP");

        BodyAddXPButton.setText("XP");
        BodyAddXPButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BodyAddXPButtonActionPerformed(evt);
            }
        });

        BodyExAttribButton.setText("Ex. Attrib.");
        BodyExAttribButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BodyExAttribButtonActionPerformed(evt);
            }
        });

        BodyXPField.setMinimumSize(new java.awt.Dimension(20, 26));
        BodyXPField.setPreferredSize(new java.awt.Dimension(52, 26));

        javax.swing.GroupLayout BodyPanelLayout = new javax.swing.GroupLayout(BodyPanel);
        BodyPanel.setLayout(BodyPanelLayout);
        BodyPanelLayout.setHorizontalGroup(
            BodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(BodyPanelLayout.createSequentialGroup()
                .addGroup(BodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(BodyPanelLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(BodyExceptionalAttributeLabel))
                    .addGroup(BodyPanelLayout.createSequentialGroup()
                        .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(BodyScoreLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 215, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(BodyLinkLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(BodyXPLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(BodyXPField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(BodyAddXPButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(BodyExAttribButton)
                .addContainerGap())
        );
        BodyPanelLayout.setVerticalGroup(
            BodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(BodyPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(BodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(BodyPanelLayout.createSequentialGroup()
                        .addGroup(BodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel6)
                            .addComponent(BodyLinkLabel)
                            .addComponent(BodyXPLabel)
                            .addComponent(BodyScoreLabel))
                        .addGap(0, 0, 0)
                        .addComponent(BodyExceptionalAttributeLabel)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(BodyPanelLayout.createSequentialGroup()
                        .addGroup(BodyPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(BodyAddXPButton)
                            .addComponent(BodyXPField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(BodyExAttribButton))
                        .addGap(0, 0, Short.MAX_VALUE))))
        );

        ReflexesPanel.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        jLabel7.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        jLabel7.setText("   Reflexes");

        ReflexesScoreLabel.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        ReflexesScoreLabel.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        ReflexesScoreLabel.setText("+2 (9)");

        ReflexesLinkLabel.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        ReflexesLinkLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        ReflexesLinkLabel.setText("+2");

        ReflexesXPLabel.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        ReflexesXPLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        ReflexesXPLabel.setText("9999 XP");

        ReflexesExceptionalAttributeLabel.setFont(new java.awt.Font("Times New Roman", 0, 12)); // NOI18N
        ReflexesExceptionalAttributeLabel.setText("Exceptional Attribute: 100/999 XP");

        ReflexesAddXPButton.setText("XP");
        ReflexesAddXPButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ReflexesAddXPButtonActionPerformed(evt);
            }
        });

        ReflexesExAttribButton.setText("Ex. Attrib.");
        ReflexesExAttribButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ReflexesExAttribButtonActionPerformed(evt);
            }
        });

        ReflexesXPField.setMinimumSize(new java.awt.Dimension(20, 26));
        ReflexesXPField.setPreferredSize(new java.awt.Dimension(52, 26));

        javax.swing.GroupLayout ReflexesPanelLayout = new javax.swing.GroupLayout(ReflexesPanel);
        ReflexesPanel.setLayout(ReflexesPanelLayout);
        ReflexesPanelLayout.setHorizontalGroup(
            ReflexesPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(ReflexesPanelLayout.createSequentialGroup()
                .addGroup(ReflexesPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(ReflexesPanelLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(ReflexesExceptionalAttributeLabel))
                    .addGroup(ReflexesPanelLayout.createSequentialGroup()
                        .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(ReflexesScoreLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 215, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(ReflexesLinkLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(ReflexesXPLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(ReflexesXPField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(ReflexesAddXPButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(ReflexesExAttribButton)
                .addContainerGap())
        );
        ReflexesPanelLayout.setVerticalGroup(
            ReflexesPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(ReflexesPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(ReflexesPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(ReflexesPanelLayout.createSequentialGroup()
                        .addGroup(ReflexesPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel7)
                            .addComponent(ReflexesLinkLabel)
                            .addComponent(ReflexesXPLabel)
                            .addComponent(ReflexesScoreLabel))
                        .addGap(0, 0, 0)
                        .addComponent(ReflexesExceptionalAttributeLabel)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(ReflexesPanelLayout.createSequentialGroup()
                        .addGroup(ReflexesPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(ReflexesAddXPButton)
                            .addComponent(ReflexesXPField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(ReflexesExAttribButton))
                        .addGap(0, 0, Short.MAX_VALUE))))
        );

        DexterityPanel.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        jLabel8.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        jLabel8.setText("   Dexterity");

        DexterityScoreLabel.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        DexterityScoreLabel.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        DexterityScoreLabel.setText("+2 (9)");

        DexterityLinkLabel.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        DexterityLinkLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        DexterityLinkLabel.setText("+2");

        DexterityXPLabel.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        DexterityXPLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        DexterityXPLabel.setText("9999 XP");

        DexterityExceptionalAttributeLabel.setFont(new java.awt.Font("Times New Roman", 0, 12)); // NOI18N
        DexterityExceptionalAttributeLabel.setText("Exceptional Attribute: 100/999 XP");

        DexterityAddXPButton.setText("XP");
        DexterityAddXPButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                DexterityAddXPButtonActionPerformed(evt);
            }
        });

        DexterityExAttribButton.setText("Ex. Attrib.");
        DexterityExAttribButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                DexterityExAttribButtonActionPerformed(evt);
            }
        });

        DexterityXPField.setPreferredSize(new java.awt.Dimension(52, 26));

        javax.swing.GroupLayout DexterityPanelLayout = new javax.swing.GroupLayout(DexterityPanel);
        DexterityPanel.setLayout(DexterityPanelLayout);
        DexterityPanelLayout.setHorizontalGroup(
            DexterityPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(DexterityPanelLayout.createSequentialGroup()
                .addGroup(DexterityPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(DexterityPanelLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(DexterityExceptionalAttributeLabel))
                    .addGroup(DexterityPanelLayout.createSequentialGroup()
                        .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(DexterityScoreLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 215, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(DexterityLinkLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(DexterityXPLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(DexterityXPField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(DexterityAddXPButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(DexterityExAttribButton)
                .addContainerGap())
        );
        DexterityPanelLayout.setVerticalGroup(
            DexterityPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(DexterityPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(DexterityPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(DexterityPanelLayout.createSequentialGroup()
                        .addGroup(DexterityPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel8)
                            .addComponent(DexterityScoreLabel)
                            .addComponent(DexterityLinkLabel)
                            .addComponent(DexterityXPLabel))
                        .addGap(0, 0, 0)
                        .addComponent(DexterityExceptionalAttributeLabel)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(DexterityPanelLayout.createSequentialGroup()
                        .addGroup(DexterityPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(DexterityAddXPButton)
                            .addComponent(DexterityXPField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(DexterityExAttribButton))
                        .addGap(0, 0, Short.MAX_VALUE))))
        );

        IntelligencePanel.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        jLabel9.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        jLabel9.setText("   Intelligence");

        IntelligenceScoreLabel.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        IntelligenceScoreLabel.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        IntelligenceScoreLabel.setText("+2 (9)");

        IntelligenceLinkLabel.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        IntelligenceLinkLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        IntelligenceLinkLabel.setText("+2");

        IntelligenceXPLabel.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        IntelligenceXPLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        IntelligenceXPLabel.setText("9999 XP");

        IntelligenceExceptionalAttributeLabel.setFont(new java.awt.Font("Times New Roman", 0, 12)); // NOI18N
        IntelligenceExceptionalAttributeLabel.setText("Exceptional Attribute: 100/999 XP");

        IntelligenceAddXPButton.setText("XP");
        IntelligenceAddXPButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                IntelligenceAddXPButtonActionPerformed(evt);
            }
        });

        IntelligenceExAttribButton.setText("Ex. Attrib.");
        IntelligenceExAttribButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                IntelligenceExAttribButtonActionPerformed(evt);
            }
        });

        IntelligenceXPField.setPreferredSize(new java.awt.Dimension(52, 26));

        javax.swing.GroupLayout IntelligencePanelLayout = new javax.swing.GroupLayout(IntelligencePanel);
        IntelligencePanel.setLayout(IntelligencePanelLayout);
        IntelligencePanelLayout.setHorizontalGroup(
            IntelligencePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(IntelligencePanelLayout.createSequentialGroup()
                .addGroup(IntelligencePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(IntelligencePanelLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(IntelligenceExceptionalAttributeLabel))
                    .addGroup(IntelligencePanelLayout.createSequentialGroup()
                        .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(IntelligenceScoreLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 215, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(IntelligenceLinkLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(IntelligenceXPLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(IntelligenceXPField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(IntelligenceAddXPButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(IntelligenceExAttribButton)
                .addContainerGap())
        );
        IntelligencePanelLayout.setVerticalGroup(
            IntelligencePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(IntelligencePanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(IntelligencePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(IntelligencePanelLayout.createSequentialGroup()
                        .addGroup(IntelligencePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel9)
                            .addComponent(IntelligenceLinkLabel)
                            .addComponent(IntelligenceXPLabel)
                            .addComponent(IntelligenceScoreLabel))
                        .addGap(0, 0, 0)
                        .addComponent(IntelligenceExceptionalAttributeLabel)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(IntelligencePanelLayout.createSequentialGroup()
                        .addGroup(IntelligencePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(IntelligenceAddXPButton)
                            .addComponent(IntelligenceXPField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(IntelligenceExAttribButton))
                        .addGap(0, 0, Short.MAX_VALUE))))
        );

        WillpowerPanel.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        jLabel10.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        jLabel10.setText("   Willpower");

        WillpowerScoreLabel.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        WillpowerScoreLabel.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        WillpowerScoreLabel.setText("+2 (9)");

        WillpowerLinkLabel.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        WillpowerLinkLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        WillpowerLinkLabel.setText("+2");

        WillpowerXPLabel.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        WillpowerXPLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        WillpowerXPLabel.setText("9999 XP");

        WillpowerExceptionalAttributeLabel.setFont(new java.awt.Font("Times New Roman", 0, 12)); // NOI18N
        WillpowerExceptionalAttributeLabel.setText("Exceptional Attribute: 100/999 XP");

        WillpowerAddXPButton.setText("XP");
        WillpowerAddXPButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                WillpowerAddXPButtonActionPerformed(evt);
            }
        });

        WillpowerExAttribButton.setText("Ex. Attrib.");
        WillpowerExAttribButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                WillpowerExAttribButtonActionPerformed(evt);
            }
        });

        WillpowerXPField.setPreferredSize(new java.awt.Dimension(52, 26));

        javax.swing.GroupLayout WillpowerPanelLayout = new javax.swing.GroupLayout(WillpowerPanel);
        WillpowerPanel.setLayout(WillpowerPanelLayout);
        WillpowerPanelLayout.setHorizontalGroup(
            WillpowerPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(WillpowerPanelLayout.createSequentialGroup()
                .addGroup(WillpowerPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(WillpowerPanelLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(WillpowerExceptionalAttributeLabel))
                    .addGroup(WillpowerPanelLayout.createSequentialGroup()
                        .addComponent(jLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(WillpowerScoreLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 215, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(WillpowerLinkLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(WillpowerXPLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(WillpowerXPField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(WillpowerAddXPButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(WillpowerExAttribButton)
                .addContainerGap())
        );
        WillpowerPanelLayout.setVerticalGroup(
            WillpowerPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(WillpowerPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(WillpowerPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(WillpowerPanelLayout.createSequentialGroup()
                        .addGroup(WillpowerPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel10)
                            .addComponent(WillpowerLinkLabel)
                            .addComponent(WillpowerXPLabel)
                            .addComponent(WillpowerScoreLabel))
                        .addGap(0, 0, 0)
                        .addComponent(WillpowerExceptionalAttributeLabel)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(WillpowerPanelLayout.createSequentialGroup()
                        .addGroup(WillpowerPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(WillpowerAddXPButton)
                            .addComponent(WillpowerXPField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(WillpowerExAttribButton))
                        .addGap(0, 0, Short.MAX_VALUE))))
        );

        CharismaPanel.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        jLabel11.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        jLabel11.setText("   Charisma");

        CharismaScoreLabel.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        CharismaScoreLabel.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        CharismaScoreLabel.setText("+2 (9)");

        CharismaLinkLabel.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        CharismaLinkLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        CharismaLinkLabel.setText("+2");

        CharismaXPLabel.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        CharismaXPLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        CharismaXPLabel.setText("9999 XP");

        CharismaExceptionalAttributeLabel.setFont(new java.awt.Font("Times New Roman", 0, 12)); // NOI18N
        CharismaExceptionalAttributeLabel.setText("Exceptional Attribute: 100/999 XP");

        CharismaAddXPButton.setText("XP");
        CharismaAddXPButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CharismaAddXPButtonActionPerformed(evt);
            }
        });

        CharismaExAttribButton.setText("Ex. Attrib.");
        CharismaExAttribButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CharismaExAttribButtonActionPerformed(evt);
            }
        });

        CharismaXPField.setPreferredSize(new java.awt.Dimension(52, 26));

        javax.swing.GroupLayout CharismaPanelLayout = new javax.swing.GroupLayout(CharismaPanel);
        CharismaPanel.setLayout(CharismaPanelLayout);
        CharismaPanelLayout.setHorizontalGroup(
            CharismaPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(CharismaPanelLayout.createSequentialGroup()
                .addGroup(CharismaPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(CharismaPanelLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(CharismaExceptionalAttributeLabel))
                    .addGroup(CharismaPanelLayout.createSequentialGroup()
                        .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(CharismaScoreLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 215, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(CharismaLinkLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(CharismaXPLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(CharismaXPField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(CharismaAddXPButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(CharismaExAttribButton)
                .addContainerGap())
        );
        CharismaPanelLayout.setVerticalGroup(
            CharismaPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(CharismaPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(CharismaPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(CharismaPanelLayout.createSequentialGroup()
                        .addGroup(CharismaPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel11)
                            .addComponent(CharismaLinkLabel)
                            .addComponent(CharismaXPLabel)
                            .addComponent(CharismaScoreLabel))
                        .addGap(0, 0, 0)
                        .addComponent(CharismaExceptionalAttributeLabel)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(CharismaPanelLayout.createSequentialGroup()
                        .addGroup(CharismaPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(CharismaAddXPButton)
                            .addComponent(CharismaXPField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(CharismaExAttribButton))
                        .addGap(0, 0, Short.MAX_VALUE))))
        );

        StrengthPanel.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        jLabel13.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        jLabel13.setText("   Strength");

        StrengthScoreLabel.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        StrengthScoreLabel.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        StrengthScoreLabel.setText("+2 (9) Above Average");

        StrengthLinkLabel.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        StrengthLinkLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        StrengthLinkLabel.setText("+2");

        StrengthXPLabel.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        StrengthXPLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        StrengthXPLabel.setText("9999 XP");

        StrengthExceptionalAttributeLabel.setFont(new java.awt.Font("Times New Roman", 0, 12)); // NOI18N
        StrengthExceptionalAttributeLabel.setText("Exceptional Attribute: 100/999 XP");

        StrengthAddXPButton.setText("XP");
        StrengthAddXPButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                StrengthAddXPButtonActionPerformed(evt);
            }
        });

        StrengthExAttribButton.setText("Ex. Attrib.");
        StrengthExAttribButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                StrengthExAttribButtonActionPerformed(evt);
            }
        });

        StrengthXPField.setMinimumSize(new java.awt.Dimension(20, 26));
        StrengthXPField.setPreferredSize(new java.awt.Dimension(52, 26));

        javax.swing.GroupLayout StrengthPanelLayout = new javax.swing.GroupLayout(StrengthPanel);
        StrengthPanel.setLayout(StrengthPanelLayout);
        StrengthPanelLayout.setHorizontalGroup(
            StrengthPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(StrengthPanelLayout.createSequentialGroup()
                .addGroup(StrengthPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(StrengthPanelLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(StrengthExceptionalAttributeLabel))
                    .addGroup(StrengthPanelLayout.createSequentialGroup()
                        .addComponent(jLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(StrengthScoreLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 215, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(StrengthLinkLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(StrengthXPLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(StrengthXPField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(StrengthAddXPButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(StrengthExAttribButton)
                .addContainerGap())
        );
        StrengthPanelLayout.setVerticalGroup(
            StrengthPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(StrengthPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(StrengthPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(StrengthPanelLayout.createSequentialGroup()
                        .addGroup(StrengthPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel13)
                            .addComponent(StrengthLinkLabel)
                            .addComponent(StrengthXPLabel)
                            .addComponent(StrengthScoreLabel))
                        .addGap(0, 0, 0)
                        .addComponent(StrengthExceptionalAttributeLabel)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(StrengthPanelLayout.createSequentialGroup()
                        .addGroup(StrengthPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(StrengthAddXPButton)
                            .addComponent(StrengthXPField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(StrengthExAttribButton))
                        .addGap(0, 0, Short.MAX_VALUE))))
        );

        CharismaPanel1.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        jLabel14.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        jLabel14.setText("   Edge");

        EdgeScoreLabel.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        EdgeScoreLabel.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        EdgeScoreLabel.setText("+2 (9)");

        EdgeLinkLabel.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        EdgeLinkLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        EdgeLinkLabel.setText("+2");

        EdgeXPLabel.setFont(new java.awt.Font("Times New Roman", 0, 18)); // NOI18N
        EdgeXPLabel.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        EdgeXPLabel.setText("9999 XP");

        EdgeExceptionalAttributeLabel.setFont(new java.awt.Font("Times New Roman", 0, 12)); // NOI18N
        EdgeExceptionalAttributeLabel.setText("Exceptional Attribute: 100/999 XP");

        EdgeAddXPButton.setText("XP");
        EdgeAddXPButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                EdgeAddXPButtonActionPerformed(evt);
            }
        });

        EdgeExAttribButton.setText("Ex. Attrib.");
        EdgeExAttribButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                EdgeExAttribButtonActionPerformed(evt);
            }
        });

        EdgeXPField.setPreferredSize(new java.awt.Dimension(52, 26));

        javax.swing.GroupLayout CharismaPanel1Layout = new javax.swing.GroupLayout(CharismaPanel1);
        CharismaPanel1.setLayout(CharismaPanel1Layout);
        CharismaPanel1Layout.setHorizontalGroup(
            CharismaPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(CharismaPanel1Layout.createSequentialGroup()
                .addGroup(CharismaPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(CharismaPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(EdgeExceptionalAttributeLabel))
                    .addGroup(CharismaPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel14, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(EdgeScoreLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 215, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(EdgeLinkLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(EdgeXPLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(EdgeXPField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(EdgeAddXPButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(EdgeExAttribButton)
                .addContainerGap())
        );
        CharismaPanel1Layout.setVerticalGroup(
            CharismaPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(CharismaPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(CharismaPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(CharismaPanel1Layout.createSequentialGroup()
                        .addGroup(CharismaPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel14)
                            .addComponent(EdgeLinkLabel)
                            .addComponent(EdgeXPLabel)
                            .addComponent(EdgeScoreLabel))
                        .addGap(0, 0, 0)
                        .addComponent(EdgeExceptionalAttributeLabel)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(CharismaPanel1Layout.createSequentialGroup()
                        .addGroup(CharismaPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(EdgeAddXPButton)
                            .addComponent(EdgeXPField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(EdgeExAttribButton))
                        .addGap(0, 0, Short.MAX_VALUE))))
        );

        javax.swing.GroupLayout AttributePanelLayout = new javax.swing.GroupLayout(AttributePanel);
        AttributePanel.setLayout(AttributePanelLayout);
        AttributePanelLayout.setHorizontalGroup(
            AttributePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(AttributePanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(AttributePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(AttributePanelLayout.createSequentialGroup()
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 215, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(BodyPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(ReflexesPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(DexterityPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(IntelligencePanel, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(WillpowerPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(CharismaPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(StrengthPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(CharismaPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        AttributePanelLayout.setVerticalGroup(
            AttributePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(AttributePanelLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(AttributePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jLabel2)
                    .addComponent(jLabel3)
                    .addComponent(jLabel4))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(StrengthPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(BodyPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(ReflexesPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(DexterityPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(IntelligencePanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(WillpowerPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(CharismaPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(CharismaPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(29, 29, 29))
        );

        VehiclePanel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Vehicles", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 16))); // NOI18N

        javax.swing.GroupLayout VehiclePanelLayout = new javax.swing.GroupLayout(VehiclePanel);
        VehiclePanel.setLayout(VehiclePanelLayout);
        VehiclePanelLayout.setHorizontalGroup(
            VehiclePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        VehiclePanelLayout.setVerticalGroup(
            VehiclePanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 240, Short.MAX_VALUE)
        );

        CombatDataPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Combat Data", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 16))); // NOI18N

        javax.swing.GroupLayout CombatDataPanelLayout = new javax.swing.GroupLayout(CombatDataPanel);
        CombatDataPanel.setLayout(CombatDataPanelLayout);
        CombatDataPanelLayout.setHorizontalGroup(
            CombatDataPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );
        CombatDataPanelLayout.setVerticalGroup(
            CombatDataPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 300, Short.MAX_VALUE)
        );

        ModulesPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Modules", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 16))); // NOI18N

        javax.swing.GroupLayout ModulesPanelLayout = new javax.swing.GroupLayout(ModulesPanel);
        ModulesPanel.setLayout(ModulesPanelLayout);
        ModulesPanelLayout.setHorizontalGroup(
            ModulesPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 226, Short.MAX_VALUE)
        );
        ModulesPanelLayout.setVerticalGroup(
            ModulesPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        jMenu1.setText("File");
        jMenu1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenu1ActionPerformed(evt);
            }
        });

        NewCharMenuItem.setText("New Character");
        NewCharMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                NewCharMenuItemActionPerformed(evt);
            }
        });
        jMenu1.add(NewCharMenuItem);

        OpenCharMenuItem.setText("Open Character");
        OpenCharMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                OpenCharMenuItemActionPerformed(evt);
            }
        });
        jMenu1.add(OpenCharMenuItem);

        ExitMenuItem.setText("Exit");
        ExitMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ExitMenuItemActionPerformed(evt);
            }
        });
        jMenu1.add(ExitMenuItem);

        jMenuBar1.add(jMenu1);

        jMenu2.setText("Edit");
        jMenuBar1.add(jMenu2);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(PersDataPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(ModulesPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(XPPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(CombatDataPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(AttributePanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(TraitsPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(VehiclePanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(EquipmentPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(SkillsPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(XPPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(PersDataPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(ModulesPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(AttributePanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(CombatDataPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(layout.createSequentialGroup()
                .addComponent(EquipmentPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(VehiclePanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(TraitsPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addComponent(SkillsPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void OpenCharMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_OpenCharMenuItemActionPerformed
        
    }//GEN-LAST:event_OpenCharMenuItemActionPerformed

    private void jMenu1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenu1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jMenu1ActionPerformed

    private void ExitMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ExitMenuItemActionPerformed
        System.exit(0);
    }//GEN-LAST:event_ExitMenuItemActionPerformed

    private void NewCharMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_NewCharMenuItemActionPerformed
        //ProgramDataSingleton.CreateNewCharacter();
        //NewCharacterPopup popup = new NewCharacterPopup();
        DateForm popup = new DateForm(this, true);
        popup.setEnabled(true);
        popup.setVisible(true);
        setVisible(false);
        
        
    }//GEN-LAST:event_NewCharMenuItemActionPerformed

    private void BodyAddXPButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BodyAddXPButtonActionPerformed
        int addedXP;
        AttributeClass attrib = ProgramDataSingleton.CurrentCharacter.Body;
        JButton button = BodyAddXPButton;
        try {
            addedXP = Integer.parseInt(BodyXPField.getText());
        }
        catch (NumberFormatException e){
            BodyXPField.setText("");
            JFrame frame = new JFrame();
            JOptionPane.showMessageDialog(frame,
            "That is not a number!",
            "ERROR",
            JOptionPane.ERROR_MESSAGE);
            return;
        }
        
        BodyXPField.setText("");
        
        if (addedXP <= 0){
            JFrame frame = new JFrame();
            JOptionPane.showMessageDialog(frame,
            "You must add XP!",
            "ERROR",
            JOptionPane.ERROR_MESSAGE);
        }
        else if (addedXP > ProgramDataSingleton.CurrentCharacter.Experience()){
            JFrame frame = new JFrame();
            JOptionPane.showMessageDialog(frame,
            "You don't have that much XP!",
            "ERROR",
            JOptionPane.ERROR_MESSAGE);
        }
        else {
            if ( attrib.MaxXP() - attrib.Experience() < addedXP ){
                int refund = addedXP + attrib.Experience() - attrib.MaxXP();
                String refundmessage = "You are at maximum XP. " + refund + " XP returned to your pool.";
                JFrame frame = new JFrame();
                JOptionPane.showMessageDialog(frame,
                        refundmessage,
                        "Too much XP",
                        JOptionPane.WARNING_MESSAGE);
                        addedXP = attrib.MaxXP() - attrib.Experience(); 
                        button.setEnabled(false);
            }
            attrib.ChangeExperience("Added XP", addedXP);
            ProgramDataSingleton.CurrentCharacter.EnterXPLog("Added XP to " + attrib.Name, -addedXP);
            displayAttributes();
            AttributePanel.repaint();
        }
    }//GEN-LAST:event_BodyAddXPButtonActionPerformed

    private void BodyExAttribButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BodyExAttribButtonActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_BodyExAttribButtonActionPerformed

    private void ReflexesAddXPButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ReflexesAddXPButtonActionPerformed
        int addedXP;
        AttributeClass attrib = ProgramDataSingleton.CurrentCharacter.Reflexes;
        JButton button = ReflexesAddXPButton;
        try {
            addedXP = Integer.parseInt(ReflexesXPField.getText());
        }
        catch (NumberFormatException e){
            ReflexesXPField.setText("");
            JFrame frame = new JFrame();
            JOptionPane.showMessageDialog(frame,
            "That is not a number!",
            "ERROR",
            JOptionPane.ERROR_MESSAGE);
            return;
        }
        
        ReflexesXPField.setText("");
        
        if (addedXP <= 0){
            JFrame frame = new JFrame();
            JOptionPane.showMessageDialog(frame,
            "You must add XP!",
            "ERROR",
            JOptionPane.ERROR_MESSAGE);
        }
        else if (addedXP > ProgramDataSingleton.CurrentCharacter.Experience()){
            JFrame frame = new JFrame();
            JOptionPane.showMessageDialog(frame,
            "You don't have that much XP!",
            "ERROR",
            JOptionPane.ERROR_MESSAGE);
        }
        else {
            if ( attrib.MaxXP() - attrib.Experience() < addedXP ){
                int refund = addedXP + attrib.Experience() - attrib.MaxXP();
                String refundmessage = "You are at maximum XP. " + refund + " XP returned to your pool.";
                JFrame frame = new JFrame();
                JOptionPane.showMessageDialog(frame,
                        refundmessage,
                        "Too much XP",
                        JOptionPane.WARNING_MESSAGE);
                        addedXP = attrib.MaxXP() - attrib.Experience(); 
                        button.setEnabled(false);
            }
            attrib.ChangeExperience("Added XP", addedXP);
            ProgramDataSingleton.CurrentCharacter.EnterXPLog("Added XP to " + attrib.Name, -addedXP);
            displayAttributes();
            AttributePanel.repaint();
        }        
    }//GEN-LAST:event_ReflexesAddXPButtonActionPerformed

    private void ReflexesExAttribButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ReflexesExAttribButtonActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_ReflexesExAttribButtonActionPerformed

    private void DexterityAddXPButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_DexterityAddXPButtonActionPerformed
        int addedXP;
        AttributeClass attrib = ProgramDataSingleton.CurrentCharacter.Dexterity;
        JButton button = DexterityAddXPButton;
        try {
            addedXP = Integer.parseInt(DexterityXPField.getText());
        }
        catch (NumberFormatException e){
            DexterityXPField.setText("");
            JFrame frame = new JFrame();
            JOptionPane.showMessageDialog(frame,
            "That is not a number!",
            "ERROR",
            JOptionPane.ERROR_MESSAGE);
            return;
        }
        
        DexterityXPField.setText("");
        
        if (addedXP <= 0){
            JFrame frame = new JFrame();
            JOptionPane.showMessageDialog(frame,
            "You must add XP!",
            "ERROR",
            JOptionPane.ERROR_MESSAGE);
        }
        else if (addedXP > ProgramDataSingleton.CurrentCharacter.Experience()){
            JFrame frame = new JFrame();
            JOptionPane.showMessageDialog(frame,
            "You don't have that much XP!",
            "ERROR",
            JOptionPane.ERROR_MESSAGE);
        }
        else {
            if ( attrib.MaxXP() - attrib.Experience() < addedXP ){
                int refund = addedXP + attrib.Experience() - attrib.MaxXP();
                String refundmessage = "You are at maximum XP. " + refund + " XP returned to your pool.";
                JFrame frame = new JFrame();
                JOptionPane.showMessageDialog(frame,
                        refundmessage,
                        "Too much XP",
                        JOptionPane.WARNING_MESSAGE);
                        addedXP = attrib.MaxXP() - attrib.Experience(); 
                        button.setEnabled(false);
            }
            attrib.ChangeExperience("Added XP", addedXP);
            ProgramDataSingleton.CurrentCharacter.EnterXPLog("Added XP to " + attrib.Name, -addedXP);
            displayAttributes();
            AttributePanel.repaint();
        }
    }//GEN-LAST:event_DexterityAddXPButtonActionPerformed

    private void DexterityExAttribButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_DexterityExAttribButtonActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_DexterityExAttribButtonActionPerformed

    private void WillpowerAddXPButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_WillpowerAddXPButtonActionPerformed
        int addedXP;
        AttributeClass attrib = ProgramDataSingleton.CurrentCharacter.Willpower;
        JButton button = WillpowerAddXPButton;
        try {
            addedXP = Integer.parseInt(WillpowerXPField.getText());
        }
        catch (NumberFormatException e){
            WillpowerXPField.setText("");
            JFrame frame = new JFrame();
            JOptionPane.showMessageDialog(frame,
            "That is not a number!",
            "ERROR",
            JOptionPane.ERROR_MESSAGE);
            return;
        }
        
        WillpowerXPField.setText("");
        
        if (addedXP <= 0){
            JFrame frame = new JFrame();
            JOptionPane.showMessageDialog(frame,
            "You must add XP!",
            "ERROR",
            JOptionPane.ERROR_MESSAGE);
        }
        else if (addedXP > ProgramDataSingleton.CurrentCharacter.Experience()){
            JFrame frame = new JFrame();
            JOptionPane.showMessageDialog(frame,
            "You don't have that much XP!",
            "ERROR",
            JOptionPane.ERROR_MESSAGE);
        }
        else {
            if ( attrib.MaxXP() - attrib.Experience() < addedXP ){
                int refund = addedXP + attrib.Experience() - attrib.MaxXP();
                String refundmessage = "You are at maximum XP. " + refund + " XP returned to your pool.";
                JFrame frame = new JFrame();
                JOptionPane.showMessageDialog(frame,
                        refundmessage,
                        "Too much XP",
                        JOptionPane.WARNING_MESSAGE);
                        addedXP = attrib.MaxXP() - attrib.Experience(); 
                        button.setEnabled(false);
            }
            attrib.ChangeExperience("Added XP", addedXP);
            ProgramDataSingleton.CurrentCharacter.EnterXPLog("Added XP to " + attrib.Name, -addedXP);
            displayAttributes();
            AttributePanel.repaint();
        }
    }//GEN-LAST:event_WillpowerAddXPButtonActionPerformed

    private void WillpowerExAttribButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_WillpowerExAttribButtonActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_WillpowerExAttribButtonActionPerformed

    private void CharismaAddXPButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CharismaAddXPButtonActionPerformed
        int addedXP;
        AttributeClass attrib = ProgramDataSingleton.CurrentCharacter.Charisma;
        JButton button = CharismaAddXPButton;
        try {
            addedXP = Integer.parseInt(CharismaXPField.getText());
        }
        catch (NumberFormatException e){
            CharismaXPField.setText("");
            JFrame frame = new JFrame();
            JOptionPane.showMessageDialog(frame,
            "That is not a number!",
            "ERROR",
            JOptionPane.ERROR_MESSAGE);
            return;
        }
        
        CharismaXPField.setText("");
        
        if (addedXP <= 0){
            JFrame frame = new JFrame();
            JOptionPane.showMessageDialog(frame,
            "You must add XP!",
            "ERROR",
            JOptionPane.ERROR_MESSAGE);
        }
        else if (addedXP > ProgramDataSingleton.CurrentCharacter.Experience()){
            JFrame frame = new JFrame();
            JOptionPane.showMessageDialog(frame,
            "You don't have that much XP!",
            "ERROR",
            JOptionPane.ERROR_MESSAGE);
        }
        else {
            if ( attrib.MaxXP() - attrib.Experience() < addedXP ){
                int refund = addedXP + attrib.Experience() - attrib.MaxXP();
                String refundmessage = "You are at maximum XP. " + refund + " XP returned to your pool.";
                JFrame frame = new JFrame();
                JOptionPane.showMessageDialog(frame,
                        refundmessage,
                        "Too much XP",
                        JOptionPane.WARNING_MESSAGE);
                        addedXP = attrib.MaxXP() - attrib.Experience(); 
                        button.setEnabled(false);
            }
            attrib.ChangeExperience("Added XP", addedXP);
            ProgramDataSingleton.CurrentCharacter.EnterXPLog("Added XP to " + attrib.Name, -addedXP);
            displayAttributes();
            AttributePanel.repaint();
        }
    }//GEN-LAST:event_CharismaAddXPButtonActionPerformed

    private void CharismaExAttribButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CharismaExAttribButtonActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_CharismaExAttribButtonActionPerformed

    private void StrengthAddXPButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_StrengthAddXPButtonActionPerformed
        int addedXP;
        AttributeClass attrib = ProgramDataSingleton.CurrentCharacter.Strength;
        JButton button = StrengthAddXPButton;
        try {
            addedXP = Integer.parseInt(StrengthXPField.getText());
        }
        catch (NumberFormatException e){
            StrengthXPField.setText("");
            JFrame frame = new JFrame();
            JOptionPane.showMessageDialog(frame,
            "That is not a number!",
            "ERROR",
            JOptionPane.ERROR_MESSAGE);
            return;
        }
        
        StrengthXPField.setText("");
        
        if (addedXP <= 0){
            JFrame frame = new JFrame();
            JOptionPane.showMessageDialog(frame,
            "You must add XP!",
            "ERROR",
            JOptionPane.ERROR_MESSAGE);
        }
        else if (addedXP > ProgramDataSingleton.CurrentCharacter.Experience()){
            JFrame frame = new JFrame();
            JOptionPane.showMessageDialog(frame,
            "You don't have that much XP!",
            "ERROR",
            JOptionPane.ERROR_MESSAGE);
        }
        else {
            if ( attrib.MaxXP() - attrib.Experience() < addedXP ){
                int refund = addedXP + attrib.Experience() - attrib.MaxXP();
                String refundmessage = "You are at maximum XP. " + refund + " XP returned to your pool.";
                JFrame frame = new JFrame();
                JOptionPane.showMessageDialog(frame,
                        refundmessage,
                        "Too much XP",
                        JOptionPane.WARNING_MESSAGE);
                        addedXP = attrib.MaxXP() - attrib.Experience(); 
                        button.setEnabled(false);
            }
            attrib.ChangeExperience("Added XP", addedXP);
            ProgramDataSingleton.CurrentCharacter.EnterXPLog("Added XP to " + attrib.Name, -addedXP);
            displayAttributes();
            AttributePanel.repaint();
        }
    }//GEN-LAST:event_StrengthAddXPButtonActionPerformed

    private void StrengthExAttribButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_StrengthExAttribButtonActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_StrengthExAttribButtonActionPerformed

    private void IntelligenceExAttribButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_IntelligenceExAttribButtonActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_IntelligenceExAttribButtonActionPerformed

    private void IntelligenceAddXPButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_IntelligenceAddXPButtonActionPerformed
        int addedXP;
        AttributeClass attrib = ProgramDataSingleton.CurrentCharacter.Intelligence;
        JButton button = IntelligenceAddXPButton;
        try {
            addedXP = Integer.parseInt(IntelligenceXPField.getText());
        }
        catch (NumberFormatException e){
            IntelligenceXPField.setText("");
            JFrame frame = new JFrame();
            JOptionPane.showMessageDialog(frame,
            "That is not a number!",
            "ERROR",
            JOptionPane.ERROR_MESSAGE);
            return;
        }
        
        IntelligenceXPField.setText("");
        
        if (addedXP <= 0){
            JFrame frame = new JFrame();
            JOptionPane.showMessageDialog(frame,
            "You must add XP!",
            "ERROR",
            JOptionPane.ERROR_MESSAGE);
        }
        else if (addedXP > ProgramDataSingleton.CurrentCharacter.Experience()){
            JFrame frame = new JFrame();
            JOptionPane.showMessageDialog(frame,
            "You don't have that much XP!",
            "ERROR",
            JOptionPane.ERROR_MESSAGE);
        }
        else {
            if ( attrib.MaxXP() - attrib.Experience() < addedXP ){
                int refund = addedXP + attrib.Experience() - attrib.MaxXP();
                String refundmessage = "You are at maximum XP. " + refund + " XP returned to your pool.";
                JFrame frame = new JFrame();
                JOptionPane.showMessageDialog(frame,
                        refundmessage,
                        "Too much XP",
                        JOptionPane.WARNING_MESSAGE);
                        addedXP = attrib.MaxXP() - attrib.Experience(); 
                        button.setEnabled(false);
            }
            attrib.ChangeExperience("Added XP", addedXP);
            ProgramDataSingleton.CurrentCharacter.EnterXPLog("Added XP to " + attrib.Name, -addedXP);
            displayAttributes();
            AttributePanel.repaint();
        }
    }//GEN-LAST:event_IntelligenceAddXPButtonActionPerformed

    private void EdgeAddXPButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_EdgeAddXPButtonActionPerformed
        int addedXP;
        AttributeClass attrib = ProgramDataSingleton.CurrentCharacter.Edge;
        JButton button = EdgeAddXPButton;
        try {
            addedXP = Integer.parseInt(EdgeXPField.getText());
        }
        catch (NumberFormatException e){
            EdgeXPField.setText("");
            JFrame frame = new JFrame();
            JOptionPane.showMessageDialog(frame,
            "That is not a number!",
            "ERROR",
            JOptionPane.ERROR_MESSAGE);
            return;
        }
        
        EdgeXPField.setText("");
        
        if (addedXP <= 0){
            JFrame frame = new JFrame();
            JOptionPane.showMessageDialog(frame,
            "You must add XP!",
            "ERROR",
            JOptionPane.ERROR_MESSAGE);
        }
        else if (addedXP > ProgramDataSingleton.CurrentCharacter.Experience()){
            JFrame frame = new JFrame();
            JOptionPane.showMessageDialog(frame,
            "You don't have that much XP!",
            "ERROR",
            JOptionPane.ERROR_MESSAGE);
        }
        else {
            if ( attrib.MaxXP() - attrib.Experience() < addedXP ){
                int refund = addedXP + attrib.Experience() - attrib.MaxXP();
                String refundmessage = "You are at maximum XP. " + refund + " XP returned to your pool.";
                JFrame frame = new JFrame();
                JOptionPane.showMessageDialog(frame,
                        refundmessage,
                        "Too much XP",
                        JOptionPane.WARNING_MESSAGE);
                        addedXP = attrib.MaxXP() - attrib.Experience(); 
                        button.setEnabled(false);
            }
            attrib.ChangeExperience("Added XP", addedXP);
            ProgramDataSingleton.CurrentCharacter.EnterXPLog("Added XP to " + attrib.Name, -addedXP);
            displayAttributes();
            AttributePanel.repaint();
        }
    }//GEN-LAST:event_EdgeAddXPButtonActionPerformed

    private void EdgeExAttribButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_EdgeExAttribButtonActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_EdgeExAttribButtonActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MainJFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(() -> {
            new MainJFrame().setVisible(true);
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel AffiliationLabel;
    private javax.swing.JPanel AttributePanel;
    private javax.swing.JButton BodyAddXPButton;
    private javax.swing.JButton BodyExAttribButton;
    private javax.swing.JLabel BodyExceptionalAttributeLabel;
    private javax.swing.JLabel BodyLinkLabel;
    private javax.swing.JPanel BodyPanel;
    private javax.swing.JLabel BodyScoreLabel;
    private javax.swing.JTextField BodyXPField;
    private javax.swing.JLabel BodyXPLabel;
    private javax.swing.JButton CharismaAddXPButton;
    private javax.swing.JButton CharismaExAttribButton;
    private javax.swing.JLabel CharismaExceptionalAttributeLabel;
    private javax.swing.JLabel CharismaLinkLabel;
    private javax.swing.JPanel CharismaPanel;
    private javax.swing.JPanel CharismaPanel1;
    private javax.swing.JLabel CharismaScoreLabel;
    private javax.swing.JTextField CharismaXPField;
    private javax.swing.JLabel CharismaXPLabel;
    private javax.swing.JPanel CombatDataPanel;
    private javax.swing.JLabel DescriptionLabel;
    private javax.swing.JButton DexterityAddXPButton;
    private javax.swing.JButton DexterityExAttribButton;
    private javax.swing.JLabel DexterityExceptionalAttributeLabel;
    private javax.swing.JLabel DexterityLinkLabel;
    private javax.swing.JPanel DexterityPanel;
    private javax.swing.JLabel DexterityScoreLabel;
    private javax.swing.JTextField DexterityXPField;
    private javax.swing.JLabel DexterityXPLabel;
    private javax.swing.JButton EdgeAddXPButton;
    private javax.swing.JButton EdgeExAttribButton;
    private javax.swing.JLabel EdgeExceptionalAttributeLabel;
    private javax.swing.JLabel EdgeLinkLabel;
    private javax.swing.JLabel EdgeScoreLabel;
    private javax.swing.JTextField EdgeXPField;
    private javax.swing.JLabel EdgeXPLabel;
    private javax.swing.JPanel EquipmentPanel;
    private javax.swing.JMenuItem ExitMenuItem;
    private javax.swing.JLabel HeightWeightLabel;
    private javax.swing.JButton IntelligenceAddXPButton;
    private javax.swing.JButton IntelligenceExAttribButton;
    private javax.swing.JLabel IntelligenceExceptionalAttributeLabel;
    private javax.swing.JLabel IntelligenceLinkLabel;
    private javax.swing.JPanel IntelligencePanel;
    private javax.swing.JLabel IntelligenceScoreLabel;
    private javax.swing.JTextField IntelligenceXPField;
    private javax.swing.JLabel IntelligenceXPLabel;
    private javax.swing.JPanel ModulesPanel;
    private javax.swing.JLabel NameLabel;
    private javax.swing.JMenuItem NewCharMenuItem;
    private javax.swing.JMenuItem OpenCharMenuItem;
    private javax.swing.JPanel PersDataPanel;
    private javax.swing.JButton ReflexesAddXPButton;
    private javax.swing.JButton ReflexesExAttribButton;
    private javax.swing.JLabel ReflexesExceptionalAttributeLabel;
    private javax.swing.JLabel ReflexesLinkLabel;
    private javax.swing.JPanel ReflexesPanel;
    private javax.swing.JLabel ReflexesScoreLabel;
    private javax.swing.JTextField ReflexesXPField;
    private javax.swing.JLabel ReflexesXPLabel;
    private javax.swing.JPanel SkillDisplayPanel;
    private javax.swing.JPanel SkillsPanel;
    private javax.swing.JButton StrengthAddXPButton;
    private javax.swing.JButton StrengthExAttribButton;
    private javax.swing.JLabel StrengthExceptionalAttributeLabel;
    private javax.swing.JLabel StrengthLinkLabel;
    private javax.swing.JPanel StrengthPanel;
    private javax.swing.JLabel StrengthScoreLabel;
    private javax.swing.JTextField StrengthXPField;
    private javax.swing.JLabel StrengthXPLabel;
    private javax.swing.JPanel TraitDisplayPanel;
    private javax.swing.JPanel TraitsPanel;
    private javax.swing.JPanel VehiclePanel;
    private javax.swing.JButton WillpowerAddXPButton;
    private javax.swing.JButton WillpowerExAttribButton;
    private javax.swing.JLabel WillpowerExceptionalAttributeLabel;
    private javax.swing.JLabel WillpowerLinkLabel;
    private javax.swing.JPanel WillpowerPanel;
    private javax.swing.JLabel WillpowerScoreLabel;
    private javax.swing.JTextField WillpowerXPField;
    private javax.swing.JLabel WillpowerXPLabel;
    private javax.swing.JLabel XPLabel;
    private javax.swing.JPanel XPPanel;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenuBar jMenuBar1;
    private java.awt.ScrollPane scrollPane1;
    private java.awt.ScrollPane scrollPane2;
    // End of variables declaration//GEN-END:variables
}
