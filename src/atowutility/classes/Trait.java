/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package atowutility.classes;

/**
 *
 * @author Chris
 */
public class Trait extends XPObjectClass {

    public String Name() {
        return Name;
    }
    private final int MinimumTP;

    public int MinimumTP() {
        return MinimumTP;
    }
    private final int MaximumTP;

    public int MaximumTP() {
        return MaximumTP;
    }
    private final Boolean Identity;

    public Boolean Identity() {
        return Identity;
    }
    private final Boolean Multiple;
    private final Boolean Vehicle;

    public Boolean Vehicle() {
        return Vehicle;
    }
    private final String Opposed;
    private final String Rules;
    public Boolean NoBuy;

    public int MaximumAbsoluteXP() {
        if (MaximumTP > 0) {
            return MaximumTP * 100;
        } else {
            return MinimumTP * 100;
        }
    }

    public Trait(String name, int min, int max, Boolean vehicle, Boolean identity, Boolean multiple, String rules, String opposed) {
        super(name);

        MinimumTP = min;
        MaximumTP = max;
        Identity = identity;
        Multiple = multiple;
        Vehicle = vehicle;
        Opposed = opposed;
        Rules = rules;
        NoBuy = false;
        for (String s : Rules.split(",")) {
            if (s.equals("NoBuy")) {
                NoBuy = true;
            }
        }
    }

    public Boolean Active() {
        return Score() >= MinimumTP && Score() <= MaximumTP;
    }
}
