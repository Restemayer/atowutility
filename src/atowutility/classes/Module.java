/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package atowutility.classes;

import atowutility.ProgramDataSingleton;
import atowutility.enums.Stage3Type;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Chris
 */
public class Module {

    public int Stage;
    private int Cost;

    public int Cost() {
        int cost = Cost;
        for (SkillField s : TakenSkillFields) {
            cost += s.Cost();
        }
        return cost;
    }
    public double Years;
    public String Name;
    public String Rules;
    public String MiscText;
    public String AffiliationString;
    public String ChangeCaste;
    public Boolean RemoveMe;
    public Boolean Repeatable;
    public Stage3Type Stage3Type;
    public double[] Stage3Aging; //{Basic, Advanced, Special}
    public LogBook Attributes;
    public LogBook Skills;
    public LogBook Traits;
    public LogBook Prerequisites;
    public LogBook Restrictions;
    public List<SkillField> AvailableSkillFields;
    public List<SkillField> TakenSkillFields;

    public Module(int stage, int cost, double years, String name, String attributes, String traits, String skills, String prerequisites, String restrictions, String rules, String fields) {
        Stage = stage;
        Cost = cost;
        Years = years;
        Name = name;
        RemoveMe = false;
        ChangeCaste = "";
        Attributes = new LogBook(Name + " Attributes", false);
        Skills = new LogBook(Name + " Skills", false);
        Traits = new LogBook(Name + " Traits", false);
        Prerequisites = new LogBook(Name + " Prerequisites", false);
        Restrictions = new LogBook(Name + " Restrictions", false);
        AvailableSkillFields = new ArrayList<>();
        TakenSkillFields = new ArrayList<>();
        Stage3Aging = new double[]{0, 0, 0};
        String[] ruleslist = rules.split(",");
        
        if (!"".equals(ruleslist[0])){
            String text = "";
            for (String rule : ruleslist){
                if (rule.split("%")[0].equals("ChangeCaste"))
                    ChangeCaste = rule.split("%")[1];
                else{
                    if (!text.equals(""))
                        text += ",";
                    text += rule;
                }
            }
            Rules = text;
        }
        
        String[] attributeslist = attributes.split(",");
        if (!"".equals(attributeslist[0])) {
            for (String attrib : attributeslist) {
                String[] line = attrib.split(":");
                Attributes.Add(line[0], line[1]);
            }
        }

        String[] traitslist = traits.split(",");
        if (!"".equals(traitslist[0])) {
            for (String trait : traitslist) {
                String[] line = trait.split(":");
                Traits.Add(line[0], line[1]);
            }
        }

        String[] skillslist = skills.split(",");
        if (!"".equals(skillslist[0])) {
            for (String skill : skillslist) {
                String[] line = skill.split(":");
                Skills.Add(line[0], line[1]);
            }
        }

        String[] prerequisiteslist = prerequisites.split(",");
        if (!"".equals(prerequisiteslist[0])) {
            for (String prereq : prerequisiteslist) {
                String[] line = prereq.split(":");
                if (line.length == 2) {
                    Prerequisites.Add(line[0], line[1]);
                } else {
                    Prerequisites.Add(prereq, "NA");
                }
            }
        }
        String[] restrictionslist = restrictions.split(",");
        if (!"".equals(restrictionslist[0])) {
            for (String restrict : restrictionslist) {
                String[] line = restrict.split(":");
                Restrictions.Add(line[0], line[1]);
            }
        }
        if (!"".equals(fields)) {
            for (String field : fields.split(",")) {
                String fName = field.split(":")[0].split("#")[0];
                Boolean nonstandard = false;
                Boolean automatic = false;
                Boolean flex = false;
                String fprerequisites = "";
                String frestrictions = "";
                String frules = "";
                String fskills = "";
                String ftype = "";
                int percost = 0;
                int perxp = 0;
                int perrebate = 0;
                if (field.split(":")[0].split("#").length == 2) {
                    nonstandard = true;
                    percost = Integer.parseInt(field.split(":")[0].split("#")[1].split("/")[0]);
                    perxp = Integer.parseInt(field.split(":")[0].split("#")[1].split("/")[1]);
                    perrebate = Integer.parseInt(field.split(":")[0].split("#")[1].split("/")[2]);
                }
                Boolean failure = true;
                for (SkillField sfield : ProgramDataSingleton.SkillFieldMaster) {
                    if (sfield.Name.equals(fName)) {
                        failure = false;
                        if (field.split(":").length == 2) {
                            String[] fieldrules = field.split(":")[1].split("#");
                            for (String rule : fieldrules) {
                                if (rule.equals("Basic") || rule.equals("Advanced") || rule.equals("Special")) {
                                    ftype = rule;
                                } else {
                                    if (!frules.equals("")) {
                                        frules += ",";
                                    }
                                    frules += rule;
                                }
                            }

                            for (String s : sfield.Prerequisites.split(",")) {
                                Boolean addprereq = true;
                                for (String r : fieldrules) {
                                    if ("IgnorePrerequisite".equals(r.split("\\%")[0]) && (r.split("\\%")[1].split("$")[0] + ":" + r.split("\\%")[1].split("\\$")[1]).equals(s)) {
                                        addprereq = false;
                                    }
                                    if (r.split("%")[0].equals("Automatic")) {
                                        automatic = true;
                                    }
                                    if (r.split("%")[0].equals("Flex")) {
                                        flex = true;
                                    }
                                }
                                if (addprereq) {
                                    if (!"".equals(fprerequisites)) {
                                        fprerequisites += ",";
                                    }
                                    fprerequisites += s;
                                }
                            }
                            for (String s : sfield.Restrictions.split(",")) {
                                Boolean addrestriction = true;
                                for (String r : fieldrules) {
                                    if ("IgnoreRestriction".equals(r.split("\\%")[0]) && (r.split("\\%")[1].split("\\$")[0] + ":" + r.split("\\%")[1].split("\\$")[1]).equals(s)) {
                                        addrestriction = false;
                                    }
                                }
                                if (addrestriction) {
                                    if (!"".equals(frestrictions)) {
                                        frestrictions += ",";
                                    }
                                    frestrictions += s;
                                }
                            }
                        }
                        for (String s : sfield.Skills) {
                            if (!fskills.equals("")) {
                                fskills += ",";
                            }
                            fskills += s;
                        }
                        SkillField newskill;

                        if (nonstandard) {
                            newskill = new SkillField(sfield.Catagory.toString(), sfield.Name, fskills, fprerequisites, frestrictions, frules, percost, perxp, perrebate);
                        } else {
                            newskill = new SkillField(sfield.Catagory.toString(), sfield.Name, fskills, fprerequisites, frestrictions, frules);
                        }
                        newskill.Type = ftype;
                        newskill.Flex = flex;

                        if (automatic) {
                            newskill.Automatic = true;
                            TakenSkillFields.add(newskill);
                        } else {
                            AvailableSkillFields.add(newskill);
                        }
                        break;
                    }
                }
                if (failure) {
                    System.out.println("Load failure: Module " + Name + " can't find SkillField " + fName);
                }
            }
        }
    }

    public void AddTakenSkillField(SkillField sf) {
        TakenSkillFields.add(sf);
    }

    public void SystemCheck() {
        for (LogBookEntry log : Skills.GetLogs()) {
            for (String t : log.Description().split("#")[0].split("\\|")) {
                Boolean nonstandard = false;
                String test = "";
                if (t.split("/")[t.split("/").length - 1].equals("Any") || t.split("/")[t.split("/").length - 1].equals("Nearest State") || t.split("/")[t.split("/").length - 1].equals("Affiliation") || t.split("/")[t.split("/").length - 1].equals("Any Primary") || t.split("/")[t.split("/").length - 1].equals("Any Secondary")
                        || t.split("/")[t.split("/").length - 1].equals("Any Clan") || t.split("/")[t.split("/").length - 1].equals("Any Homeworld Clan") || t.split("/")[t.split("/").length - 1].equals("Any Invading Clan") || t.split("/")[t.split("/").length - 1].equals("Any Inner Sphere")) {
                    nonstandard = true;
                    for (int x = 0; x < t.split("/").length - 1; x++) {
                        if (!"".equals(test)) {
                            test += "/";
                        }
                        test += t.split("/")[x];
                    }
                } else {
                    test = t;
                }

                if (!test.equals("Any")) {
                    Boolean fail = true;
                    for (Skill skill : ProgramDataSingleton.SkillsMaster()) {
                        String comparison = "";
                        if (nonstandard) {
                            for (int x = 0; x < skill.Name.split("/").length - 1; x++) {
                                if (!"".equals(comparison)) {
                                    comparison += "/";
                                }
                                comparison += skill.Name.split("/")[x];
                            }
                        } else {
                            comparison = skill.Name;
                        }
                        if (comparison.equals(test)) {
                            fail = false;
                        }
                    }
                    if (fail) {
                        System.out.println("Module " + Name + " has a failing skill: " + test);
                    }
                }
            }
        }
        for (LogBookEntry log : Traits.GetLogs()) {
            for (String t : log.Description().split("#")[0].split("\\|")) {
                Boolean nonstandard = false;
                String test = "";
                if (t.split("/")[t.split("/").length - 1].equals("Any") || t.split("/")[t.split("/").length - 1].equals("Nearest State") || t.split("/")[t.split("/").length - 1].equals("Affiliation") || t.split("/")[t.split("/").length - 1].equals("Any Primary") || t.split("/")[t.split("/").length - 1].equals("Any Secondary")
                        || t.split("/")[t.split("/").length - 1].equals("Any Clan") || t.split("/")[t.split("/").length - 1].equals("Any Homeworld Clan") || t.split("/")[t.split("/").length - 1].equals("Any Invading Clan") || t.split("/")[t.split("/").length - 1].equals("Any Inner Sphere")) {
                    nonstandard = true;
                    for (int x = 0; x < t.split("/").length - 1; x++) {
                        if (!"".equals(test)) {
                            test += "/";
                        }
                        test += t.split("/")[x];
                    }
                } else {
                    test = t;
                }

                if (!test.equals("Any")) {
                    Boolean fail = true;
                    for (Trait trait : ProgramDataSingleton.TraitsMaster()) {
                        String comparison = "";
                        if (nonstandard) {
                            for (int x = 0; x < trait.Name.split("/").length - 1; x++) {
                                if (!"".equals(comparison)) {
                                    comparison += "/";
                                }
                                comparison += trait.Name.split("/")[x];
                            }
                        } else {
                            comparison = trait.Name;
                        }
                        if (comparison.equals(test)) {
                            fail = false;
                        }
                    }
                    if (fail) {
                        System.out.println("Module " + Name + " has a failing trait: " + test);
                    }
                }
            }
        }
        for (SkillField sf : AvailableSkillFields) {
            sf.SystemCheck();
        }
        for (SkillField sf : TakenSkillFields) {
            sf.SystemCheck();
        }
    }

}
