/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package atowutility.classes;

import atowutility.ProgramDataSingleton;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Chris
 */
public class Skill extends XPObjectClass {    
   
    private final String ActionRating;    
    private final String TrainingRating;
    private final String LinkOne;
    private final String LinkTwo;
    private final Boolean TieredSkill;  
    public Boolean TieredSkill(){
        return TieredSkill;
    }
    private final Boolean Expandible;
    public String LinkString(){
        return LinkOne+"+"+LinkTwo;
    }
    public String ComplexityString(){
        return ActionRating+TrainingRating;
    }
    public Boolean Expandible(){
        return Expandible;
    }
    private List<LogBookEntry> NaturalAptitudeXPLog;
    public int NaturalAptitudeXP(){
        int xp = 0;
        xp = NaturalAptitudeXPLog.stream().map((log) -> Integer.parseInt(log.Value())).reduce(xp, Integer::sum); 
        return xp;
    }    
    public int NaturalAptitudeTP(){
        if ("A".equals(TrainingRating))
            return 500;
        else return 300;
    }
    public Boolean NaturalAptitude(){
        return (NaturalAptitudeTP() == NaturalAptitudeXP());
    }    
    public int MaximumXP (){
        int scoretable[] = GetScoreTable();
        return scoretable[10];
    }
    public int MinimumXP (){
        int scoretable[] = GetScoreTable();
        return scoretable[0];
    }
    
    public Skill(String name, String links, String complexity, Boolean tiered, Boolean expandible){
        super(name);
        NaturalAptitudeXPLog = new ArrayList<>();
        String[] ComplexityArray = complexity.split("");
        ActionRating = ComplexityArray[0];
        TrainingRating = ComplexityArray[1];
        TieredSkill = tiered;
        Expandible = expandible;
        
        String[] values = links.split("\\+");
        LinkOne = values[0];
        
        if (values.length == 2){
            LinkTwo = values[1];
        }
        else LinkTwo = "";
    }
    
    public int Score(){        
        int scoretable[] = GetScoreTable();
    
        for (int x=0; x < scoretable.length; x++){
            if (Experience() == scoretable[x] || (Experience() > scoretable[x] && Experience() < scoretable[x+1]))
                return x;
    }
    
    return -1;
    }
    public String ComplexityRating(){
    if (TieredSkill && Score() > 3){
        return ActionRating + "A";
    }
    else return ActionRating + TrainingRating;
    }
    public int TargetNumber(){
        int tn = 7;        
        if ("C".equals(ActionRating))
            tn+=1;        
        if ("A".equals(TrainingRating) || (TieredSkill && Score() > 3)){
            tn+=1;
        }
        return tn;
    }
    public String Name(){
        String addon = "";        
        if (TieredSkill){
        if (Score() > 3){
            addon = " [Advanced Tier]";
            }
        else addon = " [Basic Tier]";
        }
        return Name + addon;
    }
    public void ChangeAptitudeXP(String description, int xp){
        LogBookEntry log = new LogBookEntry(description, Integer.toString(xp));
        NaturalAptitudeXPLog.add(log);
    }   
    
    private int[] GetScoreTable(){
        int scoretable[] = {20,30,50,80,120,170,230,300,380,470,570};
        double multiplier = 1;
        for (Trait trait : ProgramDataSingleton.CurrentCharacter.Traits){
        if ("Fast Learner".equals(trait.Name) && trait.Experience() == trait.MinimumTP() * 100)
            multiplier -= 0.2;
        if ("Slow Learner".equals(trait.Name) && trait.Experience() == trait.MinimumTP() * 100)
            multiplier += 0.2;
        if ("Tech Empathy".equals(trait.Name) && "Technician".equals(Name.split("/")[0]) && trait.Experience() == trait.MinimumTP() * 100)
            multiplier -= 0.1;
        if ("Gremlins".equals(trait.Name) && "Technician".equals(Name.split("/")[0]) && trait.Experience() == trait.MinimumTP() * 100)
            multiplier += 0.1;
    }
    
    for (int a : scoretable){
        a *= multiplier;
    }
    
    return scoretable;
    }
    
    
    
}
