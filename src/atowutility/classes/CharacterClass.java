/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package atowutility.classes;
import atowutility.enums.Phenotype;
import atowutility.MainJFrame;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

/**
 *
 * @author Chris
 */
public class CharacterClass {
   private final String Name;
   
   private Phenotype Phenotype;
   public String Name(){
       return Name;
   }
    
   public AttributeClass Strength;
   public AttributeClass Body;
   public AttributeClass Reflexes;
   public AttributeClass Dexterity;
   public AttributeClass Intelligence;
   public AttributeClass Willpower;
   public AttributeClass Charisma;
   public AttributeClass Edge;
   
   public LogBook Aging;
   public List<Skill> Skills;
   public List<Trait> Traits;
   public List<CharacterClass> Identities;
   private LogBook ExperienceLog;
   
   private MainJFrame mainScreen;
   
   public int Experience(){
        return ExperienceLog.ReturnValueSum();
   }
   public int Age(){
        return Aging.ReturnValueSum();
   }
   
   public CharacterClass(String name, Phenotype phenotype){
       Name = name;
       Phenotype = phenotype;
       
       Strength = new AttributeClass("Strength", Phenotype);
       Body = new AttributeClass("Body", Phenotype);
       Reflexes = new AttributeClass("Reflexes", Phenotype);
       Dexterity = new AttributeClass("Dexterity", Phenotype);
       Intelligence = new AttributeClass("Intelligence", Phenotype);
       Willpower = new AttributeClass("Willpower", Phenotype);
       Charisma = new AttributeClass("Charisma", Phenotype);
       Edge = new AttributeClass("Edge", Phenotype);
       
       Skills = new ArrayList<>();
       Traits = new ArrayList<>();
       Identities = new ArrayList<>();
       Aging = new LogBook("Character Aging", true);
       ExperienceLog = new LogBook("Character Experience", true);       
   }
   
   public void CreateIdentity(String name){
       CharacterClass newIdentity = new CharacterClass(name, Phenotype);
       
       /* Need to implement this after I have a functioning skill, trait, and equipment lists*/
       
       Identities.add(newIdentity);
       }   
   public void UpdateMyIdentities(){
       AttributeClass[] attributes = new AttributeClass[8];
       attributes[0] = Strength;
       attributes[1] = Body;
       attributes[2] = Reflexes;
       attributes[3] = Dexterity;
       attributes[4] = Intelligence;
       attributes[5] = Willpower;
       attributes[6] = Charisma;
       attributes[7] = Edge;
       
       for (ListIterator<CharacterClass> iter = Identities.listIterator(); iter.hasNext();){
                CharacterClass element = iter.next();
                element.UpdateIdentity(attributes, Skills, Traits);
                }
   }
   public void UpdateIdentity(AttributeClass[] attributes, List<Skill> skillist, List<Trait> traitlist){
       Strength = attributes[0];
       Body = attributes[1];
       Reflexes = attributes[2];
       Dexterity = attributes[3];
       Intelligence = attributes[4];
       Willpower = attributes[5];
       Charisma = attributes[6];
       Edge = attributes[7];
       
       Skills.clear();
       Skills = skillist;
       
       for (ListIterator<Trait> iter = Traits.listIterator(); iter.hasNext();){
            Trait element = iter.next();
            if (!element.Identity()){
                for (ListIterator<Trait> iterupdate = traitlist.listIterator(); iterupdate.hasNext();){
                    Trait elementupdate = iterupdate.next();
                    if (element.Name().equals(elementupdate.Name())){
                        element.ChangeLog.clear();
                        element.ChangeLog = elementupdate.ChangeLog;
                    }
                }
            }
        }
   }
   public void EnterXPLog(String description, int xp){
   ExperienceLog.Add(description, Integer.toString(xp));
   mainScreen.UpdateXP();
   }
   public void SetMainScreen(MainJFrame frame){
       mainScreen = frame;
   }
   public Boolean AvaliableAptitude(){       
       int aptitudes = 0;
       if (aptitudes == Intelligence.Score())
               return false;
       for (Skill Skill : Skills) {
           if (Skill.NaturalAptitude()){
           aptitudes++;
           if (aptitudes == Intelligence.Score())
               return false;
           }
       }
       return true;
   }   
}
