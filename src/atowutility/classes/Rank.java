/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package atowutility.classes;

/**
 *
 * @author Chris
 */
public class Rank {

    public final String Name;
    public final String Affiliation;
    public final String[][] EnlistedRanks;
    public final String[][] OfficerRanks;
    public final int BeginningYear;
    public final int EndingYear;
    public final Boolean YearsExcluded;

    public Rank(String name, String years, String affiliation, String enlisted, String officer) {
        Name = name;
        Affiliation = affiliation;
        EnlistedRanks = new String[15][2];
        OfficerRanks = new String[15][2];
        int length = enlisted.split(",").length;
        String lastentry = enlisted.split(",")[enlisted.split(",").length - 1];
        for (int x = 0; x < 15 - length; x++) {
            enlisted += "," + lastentry;
        }

        length = officer.split(",").length;
        lastentry = officer.split(",")[officer.split(",").length - 1];
        for (int x = 0; x < 15 - length; x++) {
            officer += "," + lastentry;
        }

        for (int x = 0; x < 15; x++) {
            for (int y = 0; y < 2; y++) {
                String n = enlisted.split(",")[x].split("/")[0];
                String a;
                if (enlisted.split(",")[x].split("/").length == 1) {
                    a = n;
                } else {
                    a = enlisted.split(",")[x].split("/")[1];
                }
                if (y == 0) {
                    EnlistedRanks[x][y] = n;
                } else {
                    EnlistedRanks[x][y] = a;
                }
            }
        }

        if (years.split("!").length == 2) {
            YearsExcluded = true;
            BeginningYear = Integer.parseInt(years.split("!")[1].split("/")[0]);
            EndingYear = Integer.parseInt(years.split("!")[1].split("/")[1]);
        } else {
            YearsExcluded = false;
            if ("".equals(years) || years == null) {
                BeginningYear = 0;
                EndingYear = 9999;
            } else {
                BeginningYear = Integer.parseInt(years.split("/")[0]);
                EndingYear = Integer.parseInt(years.split("/")[1]);
            }
        }

    }

}
