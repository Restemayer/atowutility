/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package atowutility.classes;
import atowutility.ProgramDataSingleton;
import atowutility.enums.SkillFieldCatagory;

/**
 *
 * @author Chris
 */
public class SkillField {
    public SkillFieldCatagory Catagory;
    public String Name;
    public String Type;
    public String[] Skills;
    public String Prerequisites;
    public String Restrictions;
    public String Rules;
    public Boolean Flex;
    public Boolean Automatic = false;
    private final int Cost;
    public int Cost(){
        return SkillCount() * Cost;
    }
    public int CostPer(){
        return Cost;
    }
    public int XP;
    private final int Refund;
    public int Refund() {
        return SkillCount() * Refund;
    }
    public int RefundPer(){
        return Refund;
    }
    public int SkillCount(){
        int x = 0;
        for (String skill : Skills){
            if (skill.split("#").length == 1)
                x++;
            else {
                switch (skill.split("#")[1]){
                    case "2": case "2R": {x+=2; break;}
                    case "3": case "3R": {x+=3; break;}
                    case "4": case "4R": {x+=4; break;}
                    case "5": case "5R": {x+=5; break;}
                }
            }           
        }
         return x;
    }
    
    public SkillField(String catagory, String name, String skills, String prerequisites, String restrictions, String rules){
        Catagory = SkillFieldCatagory.valueOf(catagory);
        Name = name;
        Flex = false;
        Skills = skills.split(",");
        Prerequisites = prerequisites;
        Restrictions = restrictions;
        Rules = rules;
        Cost = 30;
        XP = 30;
        Refund = 6;
    }
    public SkillField(String catagory, String name, String skills, String prerequisites, String restrictions, String rules, int cost, int xp, int refund){
        Catagory = SkillFieldCatagory.valueOf(catagory);
        Name = name;
        Skills = skills.split(",");
        Prerequisites = prerequisites;
        Restrictions = restrictions;
        Rules = rules;
        Cost = cost;
        XP = xp;
        Refund = refund;
    }
    public void SystemCheck() {
        
        for (String log : Skills) {            
            for (String t : log.split("#")[0].split("\\|")) {
                String test = "";
                Boolean nonstandard = false;
                if (t.split("/")[t.split("/").length-1].equals("Any") || t.split("/")[t.split("/").length-1].equals("Nearest State") || t.split("/")[t.split("/").length-1].equals("Affiliation")){
                    nonstandard = true;
                    for (int x = 0; x < t.split("/").length - 1; x++){
                        if (!"".equals(test))
                            test += "/";
                        test += t.split("/")[x];
                    }
                }
                else test = t;
                if (!test.equals("Any")){
                    Boolean fail = true;
                    for (Skill skill : ProgramDataSingleton.SkillsMaster()) {                        
                        String comparison = "";
                        if (nonstandard){
                            for (int x = 0; x < skill.Name.split("/").length - 1; x++){
                                if (!"".equals(comparison))
                                    comparison += "/";
                                comparison += skill.Name.split("/")[x];
                            }
                        }
                        else comparison = skill.Name;
                        if (comparison.equals(test)) {
                            fail = false;
                        }
                    }
                    if (fail) System.out.println("SkillField " + Name + " has a failing skill: " + test);
                }
            }
        }
    }
    
}
