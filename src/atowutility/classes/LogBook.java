/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package atowutility.classes;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.ListIterator;

/**
 *
 * @author Chris
 */
public class LogBook {

    List<LogBookEntry> logs;
    String Name;
    final Boolean DuplicatesAllowed;

    public LogBook(String name, Boolean duplicates) {
        Name = name;
        DuplicatesAllowed = duplicates;
        logs = new ArrayList<>();
    }

    public void Add(String description, String value) {
        Boolean newTrait = true;
        if (!DuplicatesAllowed) {
            for (LogBookEntry log : logs) {

                if (log.Description().equals(description)) {
                    try {
                    log.ChangeValue(Integer.parseInt(log.Value()) + Integer.parseInt(value));
                    }
                    finally {
                    newTrait = false;
                    break;
                    }
                }
            }
        }
        if (newTrait) {
            logs.add(new LogBookEntry(description, value));
        }

        Collections.sort(logs, (final LogBookEntry object1, final LogBookEntry object2) -> object1.Description().compareTo(object2.Description()));
    }
    public void AddHigher(String description, String value){
        Boolean newTrait = true;
        if (!DuplicatesAllowed) {
            for (LogBookEntry log : logs) {
                if (log.Description().equals(description)) {
                    try {
                        int LogBookValue = Integer.parseInt(log.Value());
                        int newValue = Integer.parseInt(value);
                        if (newValue > LogBookValue)
                            log.ChangeValue(newValue);
                    }
                    finally {
                    newTrait = false;
                    break;
                    }
                }
            }
        }
        if (newTrait) {
            logs.add(new LogBookEntry(description, value));
        }

        Collections.sort(logs, (final LogBookEntry object1, final LogBookEntry object2) -> object1.Description().compareTo(object2.Description()));
    }
    public Boolean AddReturnNew(String description, String value){
        Boolean newTrait = true;
        if (!DuplicatesAllowed) {
            for (LogBookEntry log : logs) {

                if (log.Description().equals(description)) {
                    try {
                    log.ChangeValue(Integer.parseInt(log.Value()) + Integer.parseInt(value));
                    }
                    finally {
                    newTrait = false;
                    break;
                    }
                }
            }
        }
        if (newTrait) {
            logs.add(new LogBookEntry(description, value));
        }

        Collections.sort(logs, (final LogBookEntry object1, final LogBookEntry object2) -> object1.Description().compareTo(object2.Description()));
        return newTrait;
    }
    public Boolean SetValueReturnNew(String description, String value){
        Boolean newTrait = true;
        if (!DuplicatesAllowed) {
            for (LogBookEntry log : logs) {

                if (log.Description().equals(description)) {
                    try {
                    log.ChangeValue(Integer.parseInt(value));
                    }
                    finally {
                    newTrait = false;
                    break;
                    }
                }
            }
        }
        if (newTrait) {
            logs.add(new LogBookEntry(description, value));
        }

        Collections.sort(logs, (final LogBookEntry object1, final LogBookEntry object2) -> object1.Description().compareTo(object2.Description()));
        return newTrait;
    }

    public List<LogBookEntry> GetLogs() {
        return logs;
    }

    public void EraseLogs() {
        logs.clear();
    }
    public void Cleanup(){
        logs.removeIf((LogBookEntry log) -> log.RemoveMe == true);
    }

    public int ReturnValueSum() {
        int v = 0;
        for (ListIterator<LogBookEntry> iter = logs.listIterator(); iter.hasNext();) {
            LogBookEntry element = iter.next();
            v += Integer.parseInt(element.Value());
        }
        return v;
    }
    public void HalfValues(){
        logs.forEach((log) -> {
            if (!log.Value().equals("-9999"))
                log.ChangeValue((int) Math.floor(Integer.parseInt(log.Value()) / 2));
        });
    }
}
