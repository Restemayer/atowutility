/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package atowutility.classes;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

/**
 *
 * @author Chris
 */
public class XPObjectClass {
    public final String Name;
    public List<LogBookEntry> ChangeLog;
    
    public XPObjectClass(String name){
    ChangeLog = new ArrayList<>();
    Name = name;
    }
    
    public int Score(){
        double score = Experience()/100;
        if (score < 0){
        return (int)Math.ceil(score);
        }
        else {
        return (int)Math.floor(score);   
        }
    }
    public int Experience(){
        if (ChangeLog.isEmpty()){
            return 0;
        }
        else {
            int xp = 0;
            for (ListIterator<LogBookEntry> iter = ChangeLog.listIterator(); iter.hasNext();){
                LogBookEntry element = iter.next();
                xp += Integer.parseInt(element.Value());
                }
            return xp;
            }
    }  
    public void ChangeExperience (String description, int xp){
        LogBookEntry newEntry;
        newEntry = new LogBookEntry(description, Integer.toString(xp));
        ChangeLog.add(newEntry);
    }
}
