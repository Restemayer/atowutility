/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package atowutility.classes;

import atowutility.enums.Phenotype;
import java.util.ArrayList;
import java.util.List;


/**
 *
 * @author Chris
 */
public class AttributeClass extends XPObjectClass {          
    
    private List<LogBookEntry> ExceptionalAttributeXPLog;
    private Phenotype Phenotype;
    public int ExceptionalAttributeXP(){
        int xp = 0;
        xp = ExceptionalAttributeXPLog.stream().map((log) -> Integer.parseInt(log.Value())).reduce(xp, Integer::sum); 
        return xp;
    }
    public Boolean ExceptionalAttribute(){
        if (ExceptionalAttributeXP() == 200)
            return true;
        else return false;
    }
    public String ExceptionalAttributeString(){
        if (ExceptionalAttribute())
            return "Exceptional Attribute +2 TP";
        else {
        String returnString = "Exceptional Attribute: " + ExceptionalAttributeXP() + "/200 XP";
        return returnString;
        }
    }
    
    public AttributeClass(String name, Phenotype phenotype){
    super(name);
    Phenotype = phenotype;
    ExceptionalAttributeXPLog = new ArrayList<>();
    }        
    
    public String ScoreString(){
        String descriptor;        
        switch (Score()){
            case 0: descriptor = "Crippled";
            break;
            case 1: descriptor = "Feeble";
            break;
            case 2: descriptor = "Weak";
            break;
            case 3: descriptor = "Below Average";
            break;
            case 4: descriptor = "Average";
            break;
            case 5: descriptor = "Above Average";
            break;
            case 6: descriptor = "Good";
            break;
            case 7: descriptor = "Superb";
            break;
            case 8: descriptor = "Excellent";
            break;
            case 9: descriptor = "Exceptional";
            break;
            case 10: descriptor = "Super Human";
            break;
            default: descriptor = "God-like";
            break;
        }
        return Integer.toString(Score()) + " (" + MaxScore() + ") " + descriptor;
    }
    public int LinkAttribute(){        
        int linkAttribute;
        switch (Score()){
            case 0: linkAttribute = -4;
            break;
            case 1: linkAttribute = -2;
            break;
            case 2: linkAttribute = -1;
            break;
            case 3: linkAttribute = -1;
            break;
            case 4: linkAttribute = 0;
            break;
            case 5: linkAttribute = 0;
            break;
            case 6: linkAttribute = 0;
            break;
            case 7: linkAttribute = 1;
            break;
            case 8: linkAttribute = 1;
            break;
            case 9: linkAttribute = 1;
            break;
            case 10: linkAttribute = 2;
            break;
            default: if (Score()/3 < 5){
            linkAttribute = (int)Math.floor(Score()/3);
            } 
            else { linkAttribute = 5; }
            break;
        }
        return linkAttribute;
    }   
    public String LinkString(){
        if (LinkAttribute() <= 0)
            return Integer.toString(LinkAttribute());
        else return "+" + LinkAttribute();
    }
    public int MaxScore(){
        int maxscore = 8;        
        switch (Phenotype){
            case Normal:{
                switch (Name){
                    case "Charisma": case "Edge": { maxscore++; break;} 
                }
                break;
            }
            case Aerospace:{
                switch (Name){
                    case "Strength": case "Body": { maxscore--; break; }
                    case "Dexterity": case "Reflexes": case "Intelligence": { maxscore++; break; }
                }
                break;
            }
            case Elemental:{
                switch (Name) {
                    case "Strength": case "Body": case "Willpower": { maxscore++; break; }
                    case "Dexterity": { maxscore--; break; }
                }
                break;
            }
            case MechWarrior:{
                switch (Name){
                    case "Dexterity": case "Reflexes": case "Charisma": { maxscore++; break; }
                }
            }
            break;
        }
        if (ExceptionalAttribute())
            maxscore++;
        
        return maxscore;
    }
    public int MaxXP(){
    return MaxScore() * 100;
    }
}
