/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package atowutility.classes;

import atowutility.enums.AffiliationType;
import atowutility.ProgramDataSingleton;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Chris
 */
public class Stage0Module extends Module {

    //Only used in stage zero
    private final String PrimaryLanguage;
    private final String SecondaryLanguage;
    public String HouseName;
    public List<Stage0Module> Subaffiliations;
    public AffiliationType AffType;

    public Stage0Module(int stage, int cost, int years, String name, String attributes, String traits, String skills, String prerequisites, String restrictions, String rules, String house, String primarylang, String secondarylang, String afftype) {
        super(stage, cost, years, name, attributes, traits, skills, prerequisites, restrictions, rules, "");

        HouseName = house;
        PrimaryLanguage = primarylang;
        SecondaryLanguage = secondarylang;
        AffType = AffiliationType.valueOf(afftype);

        Subaffiliations = new ArrayList<>();
    }

    public String PrimaryLanguage(String subaffiliation) {
        if (subaffiliation == null || "None".equals(subaffiliation) || "".equals(subaffiliation)) {
            if (PrimaryLanguage != null && !PrimaryLanguage.equals("None") && !PrimaryLanguage.equals("")) {
                if ("Any".equals(PrimaryLanguage)) {
                    String l = "";
                    for (Skill skill : ProgramDataSingleton.SkillsMaster()) {
                        if ("Language".equals(skill.Name.split("/")[0])) {
                            if (!"".equals(l)) {
                                l += ",";
                            }
                            l += skill.Name.split("/")[1];
                        }
                    }
                    return l;
                } else {
                    return PrimaryLanguage;
                }
            } else {
                return "None";
            }
        } else {
            for (Stage0Module mod : Subaffiliations) {
                if (mod.Name.equals(subaffiliation)) {
                    if (mod.PrimaryLanguage("None") != null && !mod.PrimaryLanguage("None").equals("None")) {
                        if (PrimaryLanguage.equals("Sub")) {
                            return mod.PrimaryLanguage("");
                        } else {
                            if ("Any".equals(PrimaryLanguage)) {
                                String l = "";
                                for (Skill skill : ProgramDataSingleton.SkillsMaster()) {
                                    if ("Language".equals(skill.Name.split("/")[0])) {
                                        if (!"".equals(l)) {
                                            l += ",";
                                        }
                                        l += skill.Name.split("/")[1];
                                    }
                                }
                                return l;
                            } else {
                                return PrimaryLanguage + "," + mod.PrimaryLanguage("");
                            }
                        }
                    }
                    if (!PrimaryLanguage.equals("None") && !PrimaryLanguage.equals("") && PrimaryLanguage != null) {
                        if ("Any".equals(PrimaryLanguage)) {
                            String l = "";
                            for (Skill skill : ProgramDataSingleton.SkillsMaster()) {
                                if ("Language".equals(skill.Name.split("/")[0])) {
                                    if (!"".equals(l)) {
                                        l += ",";
                                    }
                                    l += skill.Name.split("/")[1];
                                }
                            }
                            return l;
                        } else {
                            return PrimaryLanguage;
                        }
                    }
                }
            }
            return "None";
        }
    }

    public String SecondaryLanguage(String subaffiliation) {
        if (subaffiliation == null || "None".equals(subaffiliation) || "".equals(subaffiliation)) {
            if (SecondaryLanguage != null && !SecondaryLanguage.equals("None") && !SecondaryLanguage.equals("")) {
                if ("Any".equals(SecondaryLanguage)) {
                    String l = "";
                    for (Skill skill : ProgramDataSingleton.SkillsMaster()) {
                        if ("Language".equals(skill.Name.split("/")[0])) {
                            if (!"".equals(l)) {
                                l += ",";
                            }
                            l += skill.Name.split("/")[1];
                        }
                    }
                    return l;
                } else {
                    return SecondaryLanguage;
                }
            } else {
                return "None";
            }
        } else {
            for (Stage0Module mod : Subaffiliations) {
                if (mod.Name.equals(subaffiliation)) {
                    if (mod.SecondaryLanguage("None") != null && !mod.SecondaryLanguage("None").equals("None")) {
                        if (SecondaryLanguage.equals("Sub")) {
                            return mod.SecondaryLanguage("");
                        } else {
                            if ("Any".equals(SecondaryLanguage)) {
                                String l = "";
                                for (Skill skill : ProgramDataSingleton.SkillsMaster()) {
                                    if ("Language".equals(skill.Name.split("/")[0])) {
                                        if (!"".equals(l)) {
                                            l += ",";
                                        }
                                        l += skill.Name.split("/")[1];
                                    }
                                }
                                return l;
                            } else {
                                return SecondaryLanguage + "," + mod.SecondaryLanguage("");
                            }
                        }
                    }
                    if (!SecondaryLanguage.equals("None") && !SecondaryLanguage.equals("") && SecondaryLanguage != null) {
                        if ("Any".equals(SecondaryLanguage)) {
                            String l = "";
                            for (Skill skill : ProgramDataSingleton.SkillsMaster()) {
                                if ("Language".equals(skill.Name.split("/")[0])) {
                                    if (!"".equals(l)) {
                                        l += ",";
                                    }
                                    l += skill.Name.split("/")[1];
                                }
                            }
                            return l;
                        } else {
                            return SecondaryLanguage;
                        }
                    }
                }
            }
            return "None";
        }
    }
}
